/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.injection.forge.mixins.client;

import net.ccbluex.liquidbounce.LiquidBounce;
import net.ccbluex.liquidbounce.features.module.modules.movement.NoSlow;
import net.minecraft.util.MovementInputFromOptions;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(MovementInputFromOptions.class)
public class MixinMovementInputFromOptions {

	@ModifyConstant(method = "updatePlayerMoveState", constant = @Constant(doubleValue = 0.3D, ordinal = 0))
	public double noSlowSneakStrafe(double constant) {
		NoSlow noSlow = LiquidBounce.moduleManager.getModule(NoSlow.class);
		if (noSlow.getState() && noSlow.getSneak().get())
			return noSlow.getSneakStrafeMultiplier().get();
		else
			return 0.3D;
	}

	@ModifyConstant(method = "updatePlayerMoveState", constant = @Constant(doubleValue = 0.3D, ordinal = 1))
	public double noSlowSneakForward(double constant) {
		NoSlow noSlow = LiquidBounce.moduleManager.getModule(NoSlow.class);
		if (noSlow.getState() && noSlow.getSneak().get())
			return noSlow.getSneakForwardMultiplier().get();
		else
			return 0.3D;
		//return LiquidBounce.moduleManager.getModule(NoSlow.class).getState() ? LiquidBounce.moduleManager.getModule(NoSlow.class).getSneakForwardMultiplier().get() : 0.3D;
	}

}
