/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.ui.client.clickgui

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.modules.client.ClickGuiModule
import net.ccbluex.liquidbounce.features.module.modules.client.ClickGuiModule.generateColor
import net.ccbluex.liquidbounce.ui.client.clickgui.elements.ButtonElement
import net.ccbluex.liquidbounce.ui.client.clickgui.elements.ModuleElement
import net.ccbluex.liquidbounce.ui.client.clickgui.style.Style
import net.ccbluex.liquidbounce.ui.client.clickgui.style.styles.LiquidBounceStyle
import net.ccbluex.liquidbounce.ui.client.hud.designer.GuiHudDesigner
import net.ccbluex.liquidbounce.ui.font.AWTFontRenderer.Companion.assumeNonVolatile
import net.ccbluex.liquidbounce.utils.EntityUtils
import net.ccbluex.liquidbounce.utils.render.EaseUtils.easeInOutQuint
import net.ccbluex.liquidbounce.utils.render.EaseUtils.easeOutBack
import net.ccbluex.liquidbounce.utils.render.EaseUtils.easeOutQuart
import net.ccbluex.liquidbounce.utils.render.EaseUtils.easeOutQuint
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.ccbluex.liquidbounce.utils.render.RenderUtils.drawImage
import net.minecraft.client.audio.PositionedSoundRecord
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.RenderHelper
import net.minecraft.util.ResourceLocation
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import java.io.IOException
import java.util.*

class ClickGui : GuiScreen() {
  @JvmField var panels: MutableList<Panel> = ArrayList()
  private val hudIcon = ResourceLocation("liquidbounce+/custom_hud_icon.png")

  @JvmField var style: Style = LiquidBounceStyle()
  var slide = 0.0
  var progress = 0.0
  private var clickedPanel: Panel? = null
  private var mouseX = 0
  private var mouseY = 0
  private var scrollAmount = 0
  private var lastScrollMS: Long = 0
  private var scrolling = false

  init {
    val height = 14
    var yPos = 5
    for (category in CATEGORIES) {
      panels.add(object : Panel(category.displayName, ClickGui.width, yPos, ClickGui.width, height, false) {
        override fun setupItems() {
          for (module in LiquidBounce.moduleManager.modules) {
            if (module.category === category) {
              elements.add(ModuleElement(module))
            }
          }
        }
      })
      yPos += height + 2
    }
    yPos += height + 2

    //region custom categories
    panels.add(object : Panel("Targets", ClickGui.width, yPos, ClickGui.width, height, false) {
      override fun setupItems() {
        elements.add(object : ButtonElement("Players") {
          override fun createButton(displayName: String) {
            color = if (EntityUtils.targetPlayer) generateColor().rgb else Int.MAX_VALUE
            super.createButton(displayName)
          }

          override fun getDisplayName(): String {
            displayName = "Players"
            color = if (EntityUtils.targetPlayer) generateColor().rgb else Int.MAX_VALUE
            return super.getDisplayName()
          }

          override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
            if (mouseButton == 0 && isHovering(mouseX, mouseY) && isVisible) {
              EntityUtils.targetPlayer = !EntityUtils.targetPlayer
              displayName = "Players"
              color = if (EntityUtils.targetPlayer) generateColor().rgb else Int.MAX_VALUE
              mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
              return true
            }
            return false
          }
        })
        elements.add(object : ButtonElement("Mobs") {
          override fun createButton(displayName: String) {
            color = if (EntityUtils.targetMobs) generateColor().rgb else Int.MAX_VALUE
            super.createButton(displayName)
          }

          override fun getDisplayName(): String {
            displayName = "Mobs"
            color = if (EntityUtils.targetMobs) generateColor().rgb else Int.MAX_VALUE
            return super.getDisplayName()
          }

          override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
            if (mouseButton == 0 && isHovering(mouseX, mouseY) && isVisible) {
              EntityUtils.targetMobs = !EntityUtils.targetMobs
              displayName = "Mobs"
              color = if (EntityUtils.targetMobs) generateColor().rgb else Int.MAX_VALUE
              mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
              return true
            }
            return false
          }
        })
        elements.add(object : ButtonElement("Animals") {
          override fun createButton(displayName: String) {
            color = if (EntityUtils.targetAnimals) generateColor().rgb else Int.MAX_VALUE
            super.createButton(displayName)
          }

          override fun getDisplayName(): String {
            displayName = "Animals"
            color = if (EntityUtils.targetAnimals) generateColor().rgb else Int.MAX_VALUE
            return super.getDisplayName()
          }

          override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
            if (mouseButton == 0 && isHovering(mouseX, mouseY) && isVisible) {
              EntityUtils.targetAnimals = !EntityUtils.targetAnimals
              displayName = "Animals"
              color = if (EntityUtils.targetAnimals) generateColor().rgb else Int.MAX_VALUE
              mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
              return true
            }
            return false
          }
        })
        elements.add(object : ButtonElement("Invisible") {
          override fun createButton(displayName: String) {
            color = if (EntityUtils.targetInvisible) generateColor().rgb else Int.MAX_VALUE
            super.createButton(displayName)
          }

          override fun getDisplayName(): String {
            displayName = "Invisible"
            color = if (EntityUtils.targetInvisible) generateColor().rgb else Int.MAX_VALUE
            return super.getDisplayName()
          }

          override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
            if (mouseButton == 0 && isHovering(mouseX, mouseY) && isVisible) {
              EntityUtils.targetInvisible = !EntityUtils.targetInvisible
              displayName = "Invisible"
              color = if (EntityUtils.targetInvisible) generateColor().rgb else Int.MAX_VALUE
              mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
              return true
            }
            return false
          }
        })
        elements.add(object : ButtonElement("Dead") {
          override fun createButton(displayName: String) {
            color = if (EntityUtils.targetDead) generateColor().rgb else Int.MAX_VALUE
            super.createButton(displayName)
          }

          override fun getDisplayName(): String {
            displayName = "Dead"
            color = if (EntityUtils.targetDead) generateColor().rgb else Int.MAX_VALUE
            return super.getDisplayName()
          }

          override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
            if (mouseButton == 0 && isHovering(mouseX, mouseY) && isVisible) {
              EntityUtils.targetDead = !EntityUtils.targetDead
              displayName = "Dead"
              color = if (EntityUtils.targetDead) generateColor().rgb else Int.MAX_VALUE
              mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
              return true
            }
            return false
          }
        })
      }
    })    //endregion
  }

  override fun initGui() {
    progress = 0.0
    slide = progress
    lastMS = System.currentTimeMillis()
    super.initGui()
  }

  override fun drawScreen(mouseXIn: Int, mouseYIn: Int, partialTicks: Float) {
    var mouseX = mouseXIn
    var mouseY = mouseYIn
    progress = if (progress < 1) {
      ((System.currentTimeMillis() - lastMS).toFloat() / (500f / Objects.requireNonNull(LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java)).animSpeedValue.get())).toDouble() // fully fps async
    } else {
      1.0
    }
    when (ClickGuiModule.animationValue.get().lowercase()) {
      "slidebounce", "zoombounce" -> slide = easeOutBack(progress)
      "slide", "zoom", "azura" -> slide = easeOutQuart(progress)
      "quint" -> slide = easeOutQuint(progress)
      "none" -> slide = 1.0
    }
    if (Mouse.isButtonDown(0) && mouseX >= 5 && mouseX <= 50 && mouseY <= height - 5 && mouseY >= height - 50) {
      mc.displayGuiScreen(GuiHudDesigner())
    }
    val scale = ClickGuiModule.scaleValue.get().toDouble()
    mouseX = (mouseX / scale).toInt()
    mouseY = (mouseY / scale).toInt()
    this.mouseX = mouseX
    this.mouseY = mouseY
    when (ClickGuiModule.backgroundValue.get().lowercase()) {
      "default" -> drawDefaultBackground() //			"gradient" -> drawGradientRect( 0, 0, this.width, this.height, reAlpha(generateColor(), ClickGuiModule.gradEndValue.get()).rgb, reAlpha(generateColor(), ClickGuiModule.gradStartValue.get()).rgb )
    }
    GlStateManager.disableAlpha()
    drawImage(hudIcon, 9, height - 41, 32, 32)
    GlStateManager.enableAlpha()
    when (Objects.requireNonNull(LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java)).animationValue.get().lowercase()) {
      "azura" -> {
        GlStateManager.translate(0.0, (1.0 - slide) * height * 2.0, 0.0)
        GlStateManager.scale(scale, scale + (1.0 - slide) * 2.0, scale)
      }

      "slide", "slidebounce", "quint" -> {
        GlStateManager.translate(0.0, (1.0 - slide) * height * 2.0, 0.0)
        GlStateManager.scale(scale, scale, scale)
      }

      "zoom" -> {
        GlStateManager.translate((1.0 - slide) * (width / 2.0), (1.0 - slide) * (height / 2.0), (1.0 - slide) * (width / 2.0))
        GlStateManager.scale(scale * slide, scale * slide, scale * slide)
      }

      "zoombounce" -> {
        GlStateManager.translate((1.0 - slide) * (width / 2.0), (1.0 - slide) * (height / 2.0), 0.0)
        GlStateManager.scale(scale * slide, scale * slide, scale * slide)
      }

      "none" -> GlStateManager.scale(scale, scale, scale)
    }
    for (panel in panels) {
      panel.updateFade(RenderUtils.deltaTime)
      panel.drawScreen(mouseX, mouseY, partialTicks)
    }
    for (panel in panels) {
      for (element in panel.elements) {
        if (element is ModuleElement) {
          if (mouseX != 0 && mouseY != 0 && element.isHovering(mouseX, mouseY) && element.isVisible && element.getY() <= panel.y + panel.fade) {
            style.drawDescription(mouseX, mouseY, element.module.description)
          }
        }
      }
    }
    if (Keyboard.isKeyDown(Keyboard.KEY_DELETE)) {
      for (panel in panels) {
        panel.y = 10
      }
    }
    if (Mouse.hasWheel() && Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
      val w = Mouse.getDWheel()
      if (w != 0) {
        scrolling = true
        scrollAmount = w
        lastScrollMS = System.currentTimeMillis()
        for (panel in panels) {
          panel.startY = panel.y
        }
      }
    }
    if (scrolling) {
      handleScroll()
    }
    GlStateManager.disableLighting()
    RenderHelper.disableStandardItemLighting()
    when (Objects.requireNonNull(LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java)).animationValue.get().lowercase()) {
      "azura", "slide", "slidebounce" -> GlStateManager.translate(0.0, (1.0 - slide) * height * -2.0, 0.0)
      "zoom" -> GlStateManager.translate(-1 * (1.0 - slide) * (width / 2.0), -1 * (1.0 - slide) * (height / 2.0), -1 * (1.0 - slide) * (width / 2.0))

      "zoombounce" -> GlStateManager.translate(-1 * (1.0 - slide) * (width / 2.0), -1 * (1.0 - slide) * (height / 2.0), 0.0)
    }
    GlStateManager.scale(1f, 1f, 1f)
    assumeNonVolatile = false
    super.drawScreen(mouseX, mouseY, partialTicks)
  }

  fun handleScroll() {
    var progress = (System.currentTimeMillis() - lastScrollMS).toDouble() / ClickGuiModule.panelScrollAnimMS.get().toDouble()
    progress = progress.coerceAtLeast(0.0).coerceAtMost(1.0)
    if (!ClickGuiModule.panelScrollAnim.get()) progress = 1.0

    if (progress == 1.0) {
      scrolling = false
      return
    }
    val anim = easeInOutQuint(progress)
    for (panel in panels) panel.y = (panel.startY + anim * scrollAmount).toInt()
  }

  @Throws(IOException::class)
  override fun handleMouseInput() {
    super.handleMouseInput()
    val wheel = Mouse.getEventDWheel()
    for (i in panels.indices.reversed()) {
      if (panels[i].handleScroll(mouseX, mouseY, wheel)) {
        break
      }
    }
  }

  @Throws(IOException::class)
  override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int) {
    var mouseX = mouseX
    var mouseY = mouseY
    val scale = Objects.requireNonNull(LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java)).scaleValue.get().toDouble()
    mouseX = (mouseX / scale).toInt()
    mouseY = (mouseY / scale).toInt()
    for (i in panels.indices.reversed()) {
      if (panels[i].mouseClicked(mouseX, mouseY, mouseButton)) {
        break
      }
    }
    for (panel in panels) {
      panel.drag = false
      if (mouseButton == 0 && panel.isHovering(mouseX, mouseY)) {
        clickedPanel = panel
        break
      }
    }
    if (clickedPanel != null) {
      clickedPanel!!.mouseCornerDiffX = clickedPanel!!.x - mouseX
      clickedPanel!!.mouseCornerDiffY = clickedPanel!!.y - mouseY
      clickedPanel!!.drag = true
      clickedPanel!!.dragStartTime = System.currentTimeMillis()
      panels.remove(clickedPanel)
      panels.add(clickedPanel!!)
      clickedPanel = null
    }
    super.mouseClicked(mouseX, mouseY, mouseButton)
  }

  override fun mouseReleased(mouseX: Int, mouseY: Int, state: Int) {
    var mouseX = mouseX
    var mouseY = mouseY
    val scale = Objects.requireNonNull(LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java)).scaleValue.get().toDouble()
    mouseX = (mouseX / scale).toInt()
    mouseY = (mouseY / scale).toInt()
    for (panel in panels) {
      panel.mouseReleased(mouseX, mouseY, state)
    }
    super.mouseReleased(mouseX, mouseY, state)
  }

  override fun updateScreen() {
    for (panel in panels) {
      for (element in panel.elements) {
        if (element is ButtonElement) {
          val buttonElement = element
          if (buttonElement.isHovering(mouseX, mouseY)) {
            if (buttonElement.hoverTime < 7) {
              buttonElement.hoverTime++
            }
          } else if (buttonElement.hoverTime > 0) {
            buttonElement.hoverTime--
          }
        }
        if (element is ModuleElement) {
          val e = element
          if (e.module.state) {
            if (e.slowlyFade < 255) {
              e.slowlyFade += 50
            }
          } else if (e.slowlyFade > 0) {
            e.slowlyFade -= 50
          }
          if (e.slowlyFade > 255) {
            e.slowlyFade = 255
          }
          if (e.slowlyFade < 0) {
            e.slowlyFade = 0
          }
        }
      }
    }
    super.updateScreen()
  }

  override fun onGuiClosed() {
    LiquidBounce.fileManager.saveConfig(LiquidBounce.fileManager.clickGuiConfig)
  }

  override fun doesGuiPauseGame(): Boolean {
    return false
  }

  companion object {
    val CATEGORIES = ModuleCategory.values()
    var lastMS = System.currentTimeMillis()
    const val width = 80
  }
}
