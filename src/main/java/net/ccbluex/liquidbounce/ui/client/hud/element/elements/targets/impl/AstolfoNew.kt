/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.impl

import net.ccbluex.liquidbounce.ui.client.hud.element.Border
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.Target
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.TargetStyle
import net.ccbluex.liquidbounce.ui.font.Fonts
import net.ccbluex.liquidbounce.utils.extensions.setAlpha
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.ccbluex.liquidbounce.utils.render.RenderUtils.drawEntityOnScreen
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.opengl.GL11
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

// implemented from a screenshot of astolfo on yt
class AstolfoNew(inst: Target) : TargetStyle("AstolfoNew", inst, true) {

  override fun drawTarget(entity: EntityPlayer) {
    val font = Fonts.minecraftFont

    updateAnim(entity.health)

    // new
    RenderUtils.drawRect(0F, 0F, 225F, 76F, targetInstance.bgColor.rgb)

    GL11.glPushMatrix()
    GL11.glScalef(2F, 2F, 2F)
    font.drawStringWithShadow(entity.name, 22.5F, 1.2F, getColor(-1).rgb)
    GL11.glPopMatrix()

    GL11.glPushMatrix()
    GL11.glScalef(4F, 4F, 4F)
    font.drawStringWithShadow("${getHealth1Places(entity)} ❤", 11.25F, 5F, targetInstance.barColor.rgb)
    GL11.glPopMatrix()

    GlStateManager.resetColor()
    drawEntityOnScreen(22, 72, 30, entity)

    val bgColor = targetInstance.barColor
    bgColor.setAlpha(bgColor.alpha - 40)
    RenderUtils.drawRect(45F, 60F, 220F, 71F, bgColor)

    val fgColor = targetInstance.barColor
    RenderUtils.drawRect(45F, 60F, 45F + (easingHealth / entity.maxHealth).coerceIn(0F, entity.maxHealth) * (175F), 71F, fgColor)

  }

  //region no idea how to handle these
  override fun handleBlur(entity: EntityPlayer) {
  }

  override fun handleShadowCut(entity: EntityPlayer) = handleBlur(entity)

  override fun handleShadow(entity: EntityPlayer) {
  } //endregion

  override fun getBorder(entity: EntityPlayer?): Border {
    return Border(0F, 0F, 225F, 76F)
  }

  private fun getHealth1Places(entity: EntityLivingBase?): Float {
    return if (entity == null || entity.isDead) {
      0f
    } else {
      entity.health.roundToOneDecimalPlace()
    }
  }

}

fun Float.roundToOneDecimalPlace(): Float {
  val df = DecimalFormat("#.#", DecimalFormatSymbols(Locale.ENGLISH)).apply {
    roundingMode = RoundingMode.HALF_UP
  }
  return df.format(this).toFloat()
}