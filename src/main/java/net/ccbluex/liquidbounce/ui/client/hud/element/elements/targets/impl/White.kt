/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.impl

import net.ccbluex.liquidbounce.ui.client.hud.element.Border
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.Target
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.TargetStyle
import net.ccbluex.liquidbounce.ui.font.Fonts
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.minecraft.client.gui.Gui
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11
import java.awt.Color
import kotlin.math.abs

/**
 * @author larissa
 *
 * @author <a href="https://forums.ccbluex.net/topic/7342/a-long-shape-of-the-template-targethud">Forum post</a>
 */
class White(inst: Target) : TargetStyle("White", inst, true) {

  private val fadeSpeed = inst.globalAnimSpeed
  private var lastTarget: Entity? = null

  override fun drawTarget(target: EntityPlayer) {
    val font = Fonts.minecraftFont
    updateAnim(target.health)

    if (target != lastTarget || easingHealth < 0 || easingHealth > target.maxHealth || abs(easingHealth - target.health) < 0.01) {
      easingHealth = target.health
    }

    val width = 120F

    RenderUtils.drawBorderedRect(0F, 0F, width, 36F, 0F, Color(0, 0, 0, 0).rgb, Color(0, 0, 0, 220).rgb)

    if (easingHealth > target.health) RenderUtils.drawRect(36F, 2F, (target.health / target.maxHealth) * width - 2F, 34F, Color(138, 60, 65).rgb)

    if (target.hurtTime > 9) {
      RenderUtils.drawRect(36F, 2F, (target.health / target.maxHealth) * width - 2F, 34F, Color(149, 9, 17).rgb)
    } else {
      RenderUtils.drawRect(36F, 2F, (target.health / target.maxHealth) * width - 2F, 34F, Color(255, 255, 255, 240).rgb)
    }
    val text = "${getHealth1Places(target)}"
    val textWidth = Fonts.fontBiko40.getStringWidth(text)
    Fonts.fontBiko40.drawStringWithShadow(text, 36 + (width - textWidth - 36 - 2) / 2F, (36 - Fonts.fontBiko40.FONT_HEIGHT) / 2F, Integer.MAX_VALUE)

    val playerInfo = mc.netHandler.getPlayerInfo(target.uniqueID)
    if (playerInfo != null) {
      val locationSkin = playerInfo.locationSkin
      drawHead(locationSkin)
    }
    lastTarget = target

  }

  private fun drawHead(skin: ResourceLocation) {
    GL11.glColor4f(1F, 1F, 1F, 1F)
    mc.textureManager.bindTexture(skin)
    Gui.drawScaledCustomSizeModalRect(2, 2, 8F, 8F, 8, 8, 32, 32, 64F, 64F)
  }

  override fun getBorder(entity: EntityPlayer?): Border {
    return Border(0F, 0F, 120F, 36F)
  }

  private fun getHealth1Places(entity: EntityLivingBase?): Float {
    return if (entity == null || entity.isDead) {
      0f
    } else {
      entity.health.roundToOneDecimalPlace()
    }
  }
}