/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.impl

import net.ccbluex.liquidbounce.ui.client.hud.element.Border
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.Target
import net.ccbluex.liquidbounce.ui.client.hud.element.elements.targets.TargetStyle
import net.ccbluex.liquidbounce.ui.font.Fonts
import net.ccbluex.liquidbounce.utils.extensions.setAlpha
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.ccbluex.liquidbounce.utils.render.RenderUtils.drawEntityOnScreen
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.RenderHelper
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import org.lwjgl.opengl.GL11

// implemented from another screenshot of astolfo on yt
class Astolfo(inst: Target) : TargetStyle("Astolfo", inst, true) {

  override fun drawTarget(entity: EntityPlayer) {
    val font = Fonts.minecraftFont

    updateAnim(entity.health)

    //    val bgColor = targetInstance.barColor
    //    bgColor.setAlpha(bgColor.alpha - 40)
    //    RenderUtils.drawRect( 45F, 60F, 220F, 71F, bgColor)
    //    val fgColor = targetInstance.barColor
    //    RenderUtils.drawRect(45F, 60F, 45F + (easingHealth / entity.maxHealth).coerceIn(0F, entity.maxHealth) * (175F), 71F, fgColor)

    // new
    RenderUtils.drawRect(0F, 0F, 190F, 68F, targetInstance.bgColor.rgb)
    GL11.glPushMatrix()
    GL11.glScalef(1.4F, 1.4F, 1.4F)
    font.drawStringWithShadow(entity.name, 32.14F, 1.2F, getColor(-1).rgb)
    GL11.glPopMatrix()

    GL11.glPushMatrix()
    GL11.glScalef(2.5F, 2.5F, 2.5F)
    font.drawStringWithShadow("${getHealth1Places(entity)} ❤", 17.8F, 5F, targetInstance.barColor.rgb)
    GL11.glPopMatrix()
    GlStateManager.resetColor()
    drawEntityOnScreen(23, 60, 25, entity)

    val bgColor = targetInstance.barColor
    bgColor.setAlpha(bgColor.alpha - 40)
    RenderUtils.drawRect(2F, 60F, 187F, 65F, bgColor)
    val fgColor = targetInstance.barColor
    RenderUtils.drawRect(2F, 60F, 2F + (easingHealth / entity.maxHealth).coerceIn(0F, entity.maxHealth) * (185F), 65F, fgColor)

    drawArmor(entity)

  }

  private fun drawArmor(entity: EntityPlayer) {
    val renderItem = mc.renderItem
    val x = 172F
    var y = 1F
    val step = 14F

    RenderHelper.enableGUIStandardItemLighting() // From mcp source:
    //  conf/methods.csv
    //  4704:func_71124_b,getEquipmentInSlot,2,0: Tool in Hand; 1-4: Armor
    //
    for (index in 4 downTo 1) {
      val stack = entity.getEquipmentInSlot(index) ?: return

      GL11.glPushMatrix() // normal texture size is 16x16 but the armor collumn is 14*56 (56 = 14*4)
      val scale = 14F / 16F
      GL11.glScalef(scale, scale, scale)
      renderItem.renderItemAndEffectIntoGUI(stack, (x / scale).toInt(), (y / scale).toInt())
      renderItem.renderItemOverlays(mc.fontRendererObj, stack, (x / scale).toInt(), (y / scale).toInt())
      GL11.glPopMatrix()
      y += step
    }

  }

  //region no idea how to handle these
  override fun handleBlur(entity: EntityPlayer) {
  }

  override fun handleShadowCut(entity: EntityPlayer) = handleBlur(entity)

  override fun handleShadow(entity: EntityPlayer) {
  } //endregion

  override fun getBorder(entity: EntityPlayer?): Border {
    return Border(0F, 0F, 190F, 68F)
  }

  private fun getHealth1Places(entity: EntityLivingBase?): Float {
    return if (entity == null || entity.isDead) {
      0f
    } else {
      entity.health.roundToOneDecimalPlace()
    }
  }

}