/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.ui.client.clickgui

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.modules.client.ClickGuiModule
import net.ccbluex.liquidbounce.ui.client.clickgui.elements.Element
import net.ccbluex.liquidbounce.utils.MinecraftInstance
import net.ccbluex.liquidbounce.utils.render.EaseUtils
import net.minecraft.util.StringUtils

abstract class Panel(val name: String, var x: Int, var y: Int, val width: Int, val height: Int, var open: Boolean) : MinecraftInstance() {
  val elements: MutableList<Element>

  var mouseCornerDiffX = 0
  var mouseCornerDiffY = 0

  @JvmField var startY = 0

  var drag = false
  var dragStartTime: Long = 0
  private var openStartTime: Long = 0
  private var scroll = 0
  var dragged = 0
    private set
  var scrollbar = false
    private set
  var isVisible = true
  private var elementsHeight = 0f
  var fade = 0f
  var openAnimProgress = 0.0

  init {
    elements = ArrayList()
    setupItems()
  }

  abstract fun setupItems()

  fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
    if (!isVisible) {
      return
    }
    val maxElements = LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java).maxElementsValue.get()

    // Drag
    if (drag) {
      val nx: Int = mouseCornerDiffX + mouseX
      val ny: Int = mouseCornerDiffY + mouseY

      x = nx.coerceAtLeast(0)
      y = ny.coerceAtLeast(0)
    }
    elementsHeight = (getElementsHeight() - 1).toFloat()
    val scrollbar = elements.size >= maxElements
    if (this.scrollbar != scrollbar) {
      this.scrollbar = scrollbar
    }
    LiquidBounce.clickGui.style.drawPanel(mouseX, mouseY, this)
    var y = y + height - 2
    var count = 0
    for (element in elements) {
      if (++count > scroll && count < scroll + (maxElements + 1) && scroll < elements.size) {
        element.setLocation(x, y)
        element.width = width
        if (y <= this.y + fade) {
          element.drawScreen(mouseX, mouseY, partialTicks)
        }
        y += element.height + 1
        element.isVisible = true
      } else {
        element.isVisible = false
      }
    }
  }

  fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int): Boolean {
    if (!isVisible) {
      return false
    }
    if (mouseButton == 1 && isHovering(mouseX, mouseY)) {
      open = !open
      openStartTime = System.currentTimeMillis()
      return true
    }
    for (element in elements) {
      if (element.y <= y + fade && element.mouseClicked(mouseX, mouseY, mouseButton)) {
        return true
      }
    }
    return false
  }

  fun mouseReleased(mouseX: Int, mouseY: Int, state: Int): Boolean {
    if (!isVisible) {
      return false
    }
    drag = false
    if (!open) {
      return false
    }
    for (element in elements) {
      if (element.y <= y + fade && element.mouseReleased(mouseX, mouseY, state)) {
        return true
      }
    }
    return false
  }

  fun handleScroll(mouseX: Int, mouseY: Int, wheel: Int): Boolean {
    val maxElements = LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java).maxElementsValue.get()
    if (mouseX >= x && mouseX <= x + 100 && mouseY >= y && mouseY <= y + 19 + elementsHeight) {
      if (wheel < 0 && scroll < elements.size - maxElements) {
        ++scroll
        if (scroll < 0) {
          scroll = 0
        }
      } else if (wheel > 0) {
        --scroll
        if (scroll < 0) {
          scroll = 0
        }
      }
      if (wheel < 0) {
        if (dragged < elements.size - maxElements) {
          ++dragged
        }
      } else if (wheel > 0 && dragged >= 1) {
        --dragged
      }
      return true
    }
    return false
  }

  fun updateFade(delta: Int) {
    var progress = 1.0
    if (ClickGuiModule.panelOpenAnim.get()) {
      try {
        progress = (System.currentTimeMillis() - openStartTime) / ClickGuiModule.panelOpenAnimMS.get().toDouble()
      } catch (_: ArithmeticException) {
      }
    }

    openAnimProgress = EaseUtils.easeOutQuint(progress).coerceAtMost(1.0).coerceAtLeast(0.0)

    if (open) fade = (elementsHeight * openAnimProgress).toFloat()
    else fade = ((1.0 - openAnimProgress) * elementsHeight).toFloat()

  }

  private fun getElementsHeight(): Int {
    var height = 0
    var count = 0
    for (element in elements) {
      if (count >= LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java).maxElementsValue.get()) {
        continue
      }
      height += element.height + 1
      ++count
    }
    return height
  }

  fun isHovering(mouseX: Int, mouseY: Int): Boolean {
    val textWidth = mc.fontRendererObj.getStringWidth(StringUtils.stripControlCodes(name)) - 100f
    return mouseX >= x - textWidth / 2f - 19f && mouseX <= x - textWidth / 2f + mc.fontRendererObj.getStringWidth(StringUtils.stripControlCodes(name)) + 19f && mouseY >= y && mouseY <= y + height - if (open) 2 else 0
  }
}
