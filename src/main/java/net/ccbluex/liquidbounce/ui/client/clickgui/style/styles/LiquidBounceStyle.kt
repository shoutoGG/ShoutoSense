/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */

package net.ccbluex.liquidbounce.ui.client.clickgui.style.styles

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.modules.client.ClickGuiModule
import net.ccbluex.liquidbounce.ui.client.clickgui.ClickGui
import net.ccbluex.liquidbounce.ui.client.clickgui.Panel
import net.ccbluex.liquidbounce.ui.client.clickgui.elements.ButtonElement
import net.ccbluex.liquidbounce.ui.client.clickgui.elements.ModuleElement
import net.ccbluex.liquidbounce.ui.client.clickgui.style.Style
import net.ccbluex.liquidbounce.ui.font.AWTFontRenderer.Companion.assumeNonVolatile
import net.ccbluex.liquidbounce.ui.font.Fonts
import net.ccbluex.liquidbounce.utils.Constants
import net.ccbluex.liquidbounce.utils.FontUtils
import net.ccbluex.liquidbounce.utils.block.BlockUtils.getBlockName
import net.ccbluex.liquidbounce.utils.render.EaseUtils
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.ccbluex.liquidbounce.value.*
import net.minecraft.client.audio.PositionedSoundRecord
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.util.MathHelper
import net.minecraft.util.ResourceLocation
import net.minecraft.util.StringUtils
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import org.lwjgl.opengl.GL11
import java.awt.Color
import java.awt.Point
import java.awt.Rectangle
import java.math.BigDecimal
import java.math.RoundingMode

class LiquidBounceStyle : Style() {
  private var mouseDown = false
  private var rightMouseDown = false

  private val bg = Color(Int.MIN_VALUE).rgb // black
  private val hoverBg: Int
    get() = ClickGuiModule.hoverColor().rgb
  private val cg: ClickGuiModule
    get() = LiquidBounce.moduleManager[ClickGuiModule::class.java]

  override fun drawPanel(mouseX: Int, mouseY: Int, panel: Panel) {
    if (Keyboard.isKeyDown(Keyboard.KEY_F10)) panel.y = 50
    val font = Fonts.font35    // background for category
    RenderUtils.drawBorderedRect(panel.x.toFloat() - if (panel.scrollbar) 4 else 0, panel.y.toFloat(), panel.x.toFloat() + panel.width, panel.y.toFloat() + 19 + panel.fade, 2f, Color(84, 168, 255, 100).rgb, Int.MIN_VALUE)
    val textWidth = font.getStringWidth("§f" + StringUtils.stripControlCodes(panel.name)).toFloat()    // category name
    font.drawString("§f" + panel.name, (panel.x - (textWidth - ClickGui.width) / 2f).toInt(), panel.y + 7, Color(255, 255, 255, 255).rgb)
    if (panel.scrollbar && panel.fade > 0) {
      val mul = if (ClickGuiModule.panelScrollAnim.get()) 1f else panel.openAnimProgress.toFloat()      // slider background
      RenderUtils.drawRect(panel.x - 1.5f, (panel.y + 21f), panel.x - 0.5f, (panel.y + 16 + panel.fade), Color(61, 59, 60).rgb)      // slider
      RenderUtils.drawRect((panel.x - 2).toFloat(), (panel.y + 30 + ((panel.fade - 24f) / (panel.elements.size - LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java).maxElementsValue.get()) * panel.dragged) - 10.0f), panel.x.toFloat(), (panel.y + 40 + ((panel.fade - 24.0f) / (panel.elements.size - LiquidBounce.moduleManager.getModule(ClickGuiModule::class.java).maxElementsValue.get()) * panel.dragged)), Color(226, 218, 198))
    }
  }

  override fun drawDescription(mouseX: Int, mouseY: Int, text: String) {
    val font = Fonts.font35
    val textWidth = font.getStringWidth(text)
    RenderUtils.drawBorderedRect((mouseX + 9).toFloat(), mouseY.toFloat(), (mouseX + textWidth + 14).toFloat(), (mouseY + font.FONT_HEIGHT + 3).toFloat(), 1f, Color(255, 255, 255, 90).rgb, Int.MIN_VALUE)
    GlStateManager.resetColor()
    font.drawString(text, mouseX + 12, mouseY + font.FONT_HEIGHT / 2, Int.MAX_VALUE)
  }

  override fun drawButtonElement(mouseX: Int, mouseY: Int, buttonElement: ButtonElement) {
    val font = Fonts.font35
    GlStateManager.resetColor()
    font.drawString(buttonElement.displayName, buttonElement.x + 5, buttonElement.y + 7, buttonElement.color)
  }

  private fun drawBoolValue(value: BoolValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    val text = value.name
    val textWidth = font.getStringWidth(text).toFloat()
    val mouse = Point(mouseX, mouseY)
    val box = Rectangle()
    box.x = moduleElement.x + moduleElement.width + 4
    box.width = moduleElement.settingsWidth.toInt() - 4
    box.y = yPosIn + 2
    box.height = 12
    val background: Int = if (box.contains(mouse)) hoverBg else bg

    if (moduleElement.settingsWidth < textWidth + 8) {
      moduleElement.settingsWidth = textWidth + 8
    }

    RenderUtils.drawRect(box, background)
    if (box.contains(mouse) && Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
      value.set(!value.get())
      mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
    }
    GlStateManager.resetColor()
    font.drawString(text, moduleElement.x + moduleElement.width + 6, yPosIn + 4, if (value.get()) guiColor else Int.MAX_VALUE)
    return box.height
  }

  private fun drawIntegerValue(value: IntegerValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    val text = value.name + "§f: §c" + if (value is BlockValue) getBlockName(value.get()) + " (" + value.get() + ")" else value.get().toString() + value.suffix
    val textWidth = font.getStringWidth(text).toFloat()
    val mouse = Point(mouseX, mouseY)
    val box = Rectangle(moduleElement.x + moduleElement.width + 4, yPosIn + 2, moduleElement.settingsWidth.toInt() - 4, 22)

    if (moduleElement.settingsWidth < textWidth + 8) {
      moduleElement.settingsWidth = textWidth + 8
    }

    val background: Int = if (box.contains(mouse)) hoverBg else bg
    RenderUtils.drawRect(box, background)

    // the line of the slider
    RenderUtils.drawRect((moduleElement.x + moduleElement.width + 8).toFloat(), (yPosIn + 18).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth - 4, (yPosIn + 19).toFloat(), Int.MAX_VALUE)

    // the slider
    val sliderValue = moduleElement.x + moduleElement.width + (moduleElement.settingsWidth - 12) * (value.get() - value.minimum) / (value.maximum - value.minimum)
    RenderUtils.drawRect(8 + sliderValue, (yPosIn + 15).toFloat(), sliderValue + 11, (yPosIn + 21).toFloat(), guiColor)
    if (box.contains(mouse)) {
      val dWheel = Mouse.getDWheel()
      if (Mouse.hasWheel() && dWheel != 0) {
        var amount = 1
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) amount = 10
        when {
          dWheel > 0 -> value.set((value.get() + amount).coerceAtMost(value.maximum))
          else -> value.set((value.get() - amount).coerceAtLeast(value.minimum))
        }
      }
      if (Mouse.isButtonDown(Constants.LEFTMOUSE)) {
        val i = MathHelper.clamp_double(((mouseX - moduleElement.x - moduleElement.width - 8) / (moduleElement.settingsWidth - 12)).toDouble(), 0.0, 1.0)
        value.set((value.minimum + (value.maximum - value.minimum) * i).toInt())
      }
    }
    GlStateManager.resetColor()
    font.drawString(text, moduleElement.x + moduleElement.width + 6, yPosIn + 4, 0xffffff)

    return box.height
  }

  private fun drawFloatValue(value: FloatValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    val text = value.name + "§f: §c" + round(value.get()) + value.suffix
    val textWidth = font.getStringWidth(text).toFloat()
    val box = Rectangle(moduleElement.x + moduleElement.width + 4, yPosIn + 2, moduleElement.settingsWidth.toInt() - 4, 22)
    val mouse = Point(mouseX, mouseY)
    if (moduleElement.settingsWidth < textWidth + 8) {
      moduleElement.settingsWidth = textWidth + 8
    }
    val background: Int = if (box.contains(mouse)) hoverBg
    else bg

    RenderUtils.drawRect(box, background)

    // slider line
    RenderUtils.drawRect((moduleElement.x + moduleElement.width + 8).toFloat(), (yPosIn + 18).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth - 4, (yPosIn + 19).toFloat(), Int.MAX_VALUE)

    // slider
    val sliderValue = moduleElement.x + moduleElement.width + (moduleElement.settingsWidth - 12) * (value.get() - value.minimum) / (value.maximum - value.minimum)
    RenderUtils.drawRect(8 + sliderValue, (yPosIn + 15).toFloat(), sliderValue + 11, (yPosIn + 21).toFloat(), guiColor)

    if (box.contains(mouse)) {
      val dWheel = Mouse.getDWheel()
      if (Mouse.hasWheel() && dWheel != 0) {
        var amount = 0.01f
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) amount = 0.1f
        if (Keyboard.isKeyDown(Keyboard.KEY_1)) amount = 1f

        when {
          dWheel > 0 -> value.set((value.get() + amount).coerceAtMost(value.maximum))
          else -> value.set((value.get() - amount).coerceAtLeast(value.minimum))
        }
      }
      if (Mouse.isButtonDown(Constants.LEFTMOUSE)) {
        val i = MathHelper.clamp_double(((mouseX - moduleElement.x - moduleElement.width - 8) / (moduleElement.settingsWidth - 12)).toDouble(), 0.0, 1.0)
        value.set(round((value.minimum + (value.maximum - value.minimum) * i).toFloat()).toFloat())
      }
    }
    GlStateManager.resetColor()
    font.drawString(text, moduleElement.x + moduleElement.width + 6, yPosIn + 4, 0xffffff)

    return box.height
  }

  private fun drawListValue(value: ListValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    var yPos = yPosIn
    var entryCount = 1
    val text = value.name
    val textWidth = font.getStringWidth(text).toFloat()
    val mouse = Point(mouseX, mouseY)
    val parentBox = Rectangle(moduleElement.x + moduleElement.width + 4, yPos + 2, moduleElement.settingsWidth.toInt() - 4, 12)
    val background: Int = if (parentBox.contains(mouse)) hoverBg else bg

    if (moduleElement.settingsWidth < textWidth + 16) {
      moduleElement.settingsWidth = textWidth + 16
    }

    RenderUtils.drawRect(parentBox, background)
    GlStateManager.resetColor()
    font.drawString("§c$text", moduleElement.x + moduleElement.width + 6, yPos + 4, 0xffffff)
    font.drawString(if (value.openList) "-" else "+", (moduleElement.x + moduleElement.width + moduleElement.settingsWidth - if (value.openList) 5 else 6).toInt(), yPos + 4, 0xffffff)
    if (parentBox.contains(mouse) && Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
      value.openList = !value.openList
      mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
    }
    yPos += parentBox.height
    for (entry in value.values) {
      val entryWidth = font.getStringWidth(">$entry").toFloat()
      val entryBox = Rectangle(moduleElement.x + moduleElement.width + 4, yPos + 2, moduleElement.settingsWidth.toInt() - 4, 12)
      if (moduleElement.settingsWidth < entryWidth + 8) {
        moduleElement.settingsWidth = entryWidth + 8
      }
      if (value.openList) {
        val entryBackground: Int = if (entryBox.contains(mouse)) hoverBg else bg

        RenderUtils.drawRect(entryBox, entryBackground)
        if (entryBox.contains(mouse) && Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
          value.set(entry)
          mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
        }
        GlStateManager.resetColor()
        font.drawString(">", moduleElement.x + moduleElement.width + 6, yPos + 4, Int.MAX_VALUE)
        font.drawString(entry, moduleElement.x + moduleElement.width + 14, yPos + 4, if (value.get().equals(entry, ignoreCase = true)) guiColor else Int.MAX_VALUE)
        yPos += entryBox.height
        entryCount++
      }
    }
    return entryCount * parentBox.height
  }

  private fun drawFontValue(value: FontValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    var yPos = yPosIn
    var amount = 0
    val text = value.name
    val textWidth = font.getStringWidth(text).toFloat()
    if (moduleElement.settingsWidth < textWidth + 16) {
      moduleElement.settingsWidth = textWidth + 16
    }
    val background: Int = if (mouseX > (moduleElement.x + moduleElement.width + 4) && mouseX < (moduleElement.x + moduleElement.width + moduleElement.settingsWidth) && mouseY > (yPos + 2) && mouseY < (yPos + 14)) hoverBg
    else bg
    RenderUtils.drawRect((moduleElement.x + moduleElement.width + 4).toFloat(), (yPos + 2).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth, (yPos + 14).toFloat(), background)
    GlStateManager.resetColor()
    font.drawString("§c$text", moduleElement.x + moduleElement.width + 6, yPos + 4, 0xffffff)
    font.drawString(if (value.openList) "-" else "+", (moduleElement.x + moduleElement.width + moduleElement.settingsWidth - if (value.openList) 5 else 6).toInt(), yPos + 4, 0xffffff)
    if (mouseX > moduleElement.x + moduleElement.width + 4 && (mouseX < (moduleElement.x + moduleElement.width + moduleElement.settingsWidth)) && mouseY > yPos + 2 && mouseY < yPos + 14) {
      if (Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
        value.openList = !value.openList
        mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
      }
    }
    yPos += 12
    amount += 12
    for (fontPair in FontUtils.getAllFontDetails()) {
      val format = "> ${fontPair.first}"

      val textWidth2 = font.getStringWidth(format).toFloat()
      if (moduleElement.settingsWidth < textWidth2 + 8) {
        moduleElement.settingsWidth = textWidth2 + 8
      }
      if (value.openList) {
        val listItemBackground: Int = if (mouseX > (moduleElement.x + moduleElement.width + 4) && mouseX < (moduleElement.x + moduleElement.width + moduleElement.settingsWidth) && mouseY > (yPos + 2) && mouseY < (yPos + 14)) hoverBg
        else bg

        RenderUtils.drawRect((moduleElement.x + moduleElement.width + 4).toFloat(), (yPos + 2).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth, (yPos + 14).toFloat(), listItemBackground)
        if (mouseX > moduleElement.x + moduleElement.width + 4 && (mouseX < (moduleElement.x + moduleElement.width + moduleElement.settingsWidth)) && mouseY > yPos + 2 && mouseY < yPos + 14) {
          if (Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
            value.set(fontPair.second)
            mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
          }
        }
        GlStateManager.resetColor()
        font.drawString(">", moduleElement.x + moduleElement.width + 6, yPos + 4, Int.MAX_VALUE)
        font.drawString(fontPair.first, moduleElement.x + moduleElement.width + 14, yPos + 4, if (value.get() == fontPair.second) guiColor else Int.MAX_VALUE)
        yPos += 12
        amount += 12
      }
    }
    return amount
  }

  private fun drawTextValue(value: TextValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    val text = value.name + "§f: §c" + value.get()
    val textWidth = font.getStringWidth(text).toFloat()
    if (moduleElement.settingsWidth < textWidth + 8) {
      moduleElement.settingsWidth = textWidth + 8
    }
    val background: Int = if (mouseX > (moduleElement.x + moduleElement.width + 4) && mouseX < (moduleElement.x + moduleElement.width + moduleElement.settingsWidth) && mouseY > (yPosIn + 2) && mouseY < (yPosIn + 14)) hoverBg
    else bg

    RenderUtils.drawRect((moduleElement.x + moduleElement.width + 4).toFloat(), (yPosIn + 2).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth, (yPosIn + 14).toFloat(), background)
    GlStateManager.resetColor()
    font.drawString(text, moduleElement.x + moduleElement.width + 6, yPosIn + 4, 0xffffff)
    return 12
  }

  private fun drawNoteValue(value: NoteValue, mouseX: Int, mouseY: Int, me: ModuleElement, yPosIn: Int, guiColor: Int): Int {
    val font = Fonts.font40
    val text = "§8-- §${if (value.open) "a" else "c"}§l${value.name} §8--§r"
    val textWidth = font.getStringWidth(text).toFloat()
    if (me.settingsWidth < textWidth + 8) {
      me.settingsWidth = textWidth + 8
    }
    val background = bg

    val startY = 2
    val yOffset = 3
    val height = 16

    RenderUtils.drawRect((me.x + me.width + 4).toFloat(), (yPosIn + startY).toFloat(), me.x + me.width + me.settingsWidth, (yPosIn + startY + height).toFloat(), background)
    GlStateManager.resetColor()
    font.drawString(text, (me.x + me.width + (me.settingsWidth - textWidth) / 2).toInt(), yPosIn + yOffset * 2, 0xffffff)

    val startX = (me.x + me.width + 4)
    val startYY = (yPosIn + startY)
    val endX = (me.x + me.width + me.settingsWidth).toInt()
    val endY = (yPosIn + startY + height)

    if (mouseX in startX..endX && mouseY in startYY..endY && Mouse.isButtonDown(Constants.LEFTMOUSE) && me.isntPressed()) {
      value.open = !value.open
    }

    return height
  }

  private fun drawMultiBoolValue(value: MultiBoolValue, mouseX: Int, mouseY: Int, moduleElement: ModuleElement, yPosIn: Int, guiColor: Int, font: FontRenderer): Int {
    var yPos = yPosIn
    var entryCount = 1
    val text = value.name
    val textWidth = font.getStringWidth(text).toFloat()
    val mouse = Point(mouseX, mouseY)
    val parentBox = Rectangle(moduleElement.x + moduleElement.width + 4, yPos + 2, moduleElement.settingsWidth.toInt() - 4, 12)
    val background: Int = if (parentBox.contains(mouse)) hoverBg else bg

    if (moduleElement.settingsWidth < textWidth + 16) {
      moduleElement.settingsWidth = textWidth + 16
    }

    RenderUtils.drawRect(parentBox, background)
    GlStateManager.resetColor()
    font.drawString("§c$text", moduleElement.x + moduleElement.width + 6, yPos + 4, 0xffffff)
    font.drawString(if (value.openList) "-" else "+", (moduleElement.x + moduleElement.width + moduleElement.settingsWidth - if (value.openList) 5 else 6).toInt(), yPos + 4, 0xffffff)
    if (parentBox.contains(mouse) && Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
      value.openList = !value.openList
      mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
    }
    yPos += parentBox.height
    for (entry in value.keys()) {
      val entryWidth = font.getStringWidth(">$entry").toFloat()
      val entryBox = Rectangle(moduleElement.x + moduleElement.width + 4, yPos + 2, moduleElement.settingsWidth.toInt() - 4, 12)
      if (moduleElement.settingsWidth < entryWidth + 8) {
        moduleElement.settingsWidth = entryWidth + 8
      }
      if (value.openList) {
        val entryBackground: Int = if (entryBox.contains(mouse)) hoverBg else bg

        RenderUtils.drawRect(entryBox, entryBackground)
        if (entryBox.contains(mouse) && Mouse.isButtonDown(Constants.LEFTMOUSE) && moduleElement.isntPressed()) {
          value.set(entry, !value.get(entry))
          mc.soundHandler.playSound(PositionedSoundRecord.create(ResourceLocation("gui.button.press"), 1.0f))
        }
        GlStateManager.resetColor()
        font.drawString(">", moduleElement.x + moduleElement.width + 6, yPos + 4, Int.MAX_VALUE)
        font.drawString(entry, moduleElement.x + moduleElement.width + 14, yPos + 4, if (value.get(entry)) guiColor else Int.MAX_VALUE)
        yPos += entryBox.height
        entryCount++
      }
    }
    return entryCount * parentBox.height
  }

  override fun drawModuleElement(mouseX: Int, mouseY: Int, moduleElement: ModuleElement) {
    val font = Fonts.font35
    val guiColor = ClickGuiModule.generateColor().rgb
    GlStateManager.resetColor()
    GlStateManager.enableAlpha()
    font.drawString(moduleElement.displayName, moduleElement.x + 5, moduleElement.y + 7, if (moduleElement.module.state) guiColor else Int.MAX_VALUE)
    val moduleValues = moduleElement.module.values

    if (moduleValues.isEmpty()) return
    font.drawString(if (moduleElement.isShowSettings) ">" else "+", moduleElement.x + moduleElement.width - 8, moduleElement.y + moduleElement.height / 2, Color.WHITE.rgb)
    if (!moduleElement.isShowSettings) return

    var yPos = moduleElement.y + 4
    val progress = (System.currentTimeMillis() - moduleElement.lastOpenMS) / ClickGuiModule.valuesOpenAnimMS.get().toDouble()
    val anim = if (!ClickGuiModule.valuesOpenAnim.get()) 1.0 else EaseUtils.easeOutQuint(progress).coerceAtLeast(0.0).coerceAtMost(1.0)

    GL11.glPushMatrix()
    GL11.glScaled(1.0, anim, 1.0)

    var should = true

    for (value in moduleValues) {
      if (!value.canDisplay.invoke()) {
        continue
      }

      if (!should && value !is NoteValue) continue

      val isNumber = value.get() is Number
      if (isNumber) assumeNonVolatile = false
      yPos += when (value) {
        is BoolValue -> drawBoolValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is ListValue -> drawListValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is FloatValue -> drawFloatValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is IntegerValue -> drawIntegerValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is FontValue -> drawFontValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is TextValue -> drawTextValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is MultiBoolValue -> drawMultiBoolValue(value, mouseX, mouseY, moduleElement, yPos, guiColor, font)
        is NoteValue -> {
          should = value.open
          drawNoteValue(value, mouseX, mouseY, moduleElement, yPos, guiColor)
        }

        else -> 0
      }
      if (isNumber) assumeNonVolatile = true
    }
    moduleElement.updatePressed()
    mouseDown = Mouse.isButtonDown(Constants.LEFTMOUSE)
    rightMouseDown = Mouse.isButtonDown(Constants.RIGHTMOUSE)
    GlStateManager.disableAlpha()
    if (moduleElement.settingsWidth > 0f && yPos > moduleElement.y + 4) RenderUtils.drawBorderedRect((moduleElement.x + moduleElement.width + 4).toFloat(), (moduleElement.y + 6).toFloat(), moduleElement.x + moduleElement.width + moduleElement.settingsWidth, (yPos + 2).toFloat(), 2F, Color(84, 168, 255, 100).rgb, Color(0, 0, 0, 0).rgb)
    GL11.glPopMatrix()
  }

  private fun round(f: Float): BigDecimal {
    var bd = BigDecimal(f.toString())
    bd = bd.setScale(4, RoundingMode.HALF_UP)
    return bd
  }
}
