/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.file.configs

import com.google.gson.*
import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.modules.misc.AutoDisable.DisableEvent
import net.ccbluex.liquidbounce.file.FileConfig
import net.ccbluex.liquidbounce.file.FileManager
import java.io.*

class ModulesConfig
/**
 * Constructor of config
 *
 * @param file of config
 */
  (file: File?) : FileConfig(file) {
  /**
   * Load config from file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun loadConfig() {
    val jsonElement = JsonParser().parse(BufferedReader(FileReader(file)))
    if (jsonElement is JsonNull) return
    val entryIterator: Iterator<Map.Entry<String, JsonElement>> = jsonElement.asJsonObject.entrySet().iterator()
    while (entryIterator.hasNext()) {
      val (key, value) = entryIterator.next()
      val module = LiquidBounce.moduleManager.getModule(key)
      if (module != null) {
        val jsonModule = value as JsonObject
        module.state = jsonModule["State"].asBoolean
        module.keyBind = jsonModule["KeyBind"].asInt
        if (jsonModule.has("Array")) module.array = jsonModule["Array"].asBoolean
        if (jsonModule.has("AutoDisable")) {
          module.autoDisables.clear()
          try {
            val jsonAD = jsonModule.getAsJsonArray("AutoDisable")
            if (jsonAD.size() > 0) for (i in 0..jsonAD.size() - 1) {
              try {
                val disableEvent = DisableEvent.valueOf(jsonAD[i].asString)
                module.autoDisables.add(disableEvent)
              } catch (e: Exception) {                // nothing
              }
            }
          } catch (e: Exception) {            //nothing.
          }
        }
      }
    }
  }

  /**
   * Save config to file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun saveConfig() {
    val jsonObject = JsonObject()
    for (module in LiquidBounce.moduleManager.modules) {
      val jsonMod = JsonObject()
      jsonMod.addProperty("State", module.state)
      jsonMod.addProperty("KeyBind", module.keyBind)
      jsonMod.addProperty("Array", module.array)
      val jsonAD = JsonArray()
      for (e in module.autoDisables) {
        jsonAD.add(JsonPrimitive(e.toString()))
      }
      jsonMod.add("AutoDisable", jsonAD)
      jsonObject.add(module.name, jsonMod)
    }
    val printWriter = PrintWriter(FileWriter(file))
    printWriter.println(FileManager.PRETTY_GSON.toJson(jsonObject))
    printWriter.close()
  }
}
