/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.file.configs

import com.google.gson.JsonArray
import com.google.gson.JsonParser
import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.modules.render.XRay
import net.ccbluex.liquidbounce.file.FileConfig
import net.ccbluex.liquidbounce.file.FileManager
import net.ccbluex.liquidbounce.utils.ClientUtils
import net.minecraft.block.Block
import java.io.*

class XRayConfig
/**
 * Constructor of config
 *
 * @param file of config
 */
  (file: File?) : FileConfig(file) {
  /**
   * Load config from file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun loadConfig() {
    val xRay = LiquidBounce.moduleManager.getModule(XRay::class.java)
    if (xRay == null) {
      ClientUtils.getLogger().error("[FileManager] Failed to find xray module.")
      return
    }
    val jsonArray = JsonParser().parse(BufferedReader(FileReader(file))).asJsonArray
    xRay.xrayBlocks.clear()
    for (jsonElement in jsonArray) {
      try {
        val block = Block.getBlockFromName(jsonElement.asString)
        if (xRay.xrayBlocks.contains(block)) {
          ClientUtils.getLogger().error("[FileManager] Skipped xray block '" + block.registryName + "' because the block is already added.")
          continue
        }
        xRay.xrayBlocks.add(block)
      } catch (throwable: Throwable) {
        ClientUtils.getLogger().error("[FileManager] Failed to add block to xray.", throwable)
      }
    }
  }

  /**
   * Save config to file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun saveConfig() {
    val xRay = LiquidBounce.moduleManager.getModule(XRay::class.java)
    if (xRay == null) {
      ClientUtils.getLogger().error("[FileManager] Failed to find xray module.")
      return
    }
    val jsonArray = JsonArray()
    for (block in xRay.xrayBlocks) jsonArray.add(FileManager.PRETTY_GSON.toJsonTree(Block.getIdFromBlock(block)))
    val printWriter = PrintWriter(FileWriter(file))
    printWriter.println(FileManager.PRETTY_GSON.toJson(jsonArray))
    printWriter.close()
  }
}
