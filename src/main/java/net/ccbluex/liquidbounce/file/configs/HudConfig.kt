/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.file.configs

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.file.FileConfig
import net.ccbluex.liquidbounce.ui.client.hud.Config
import org.apache.commons.io.FileUtils
import java.io.*

class HudConfig
/**
 * Constructor of config
 *
 * @param file of config
 */
  (file: File?) : FileConfig(file) {
  /**
   * Load config from file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun loadConfig() {
    LiquidBounce.hud.clearElements()
    LiquidBounce.hud = Config(FileUtils.readFileToString(file)).toHUD()
  }

  /**
   * Save config to file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun saveConfig() {
    val printWriter = PrintWriter(FileWriter(file))
    printWriter.println(Config(LiquidBounce.hud).toJson())
    printWriter.close()
  }
}
