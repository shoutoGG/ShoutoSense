/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.file.configs

import com.google.gson.*
import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.LiquidBounce.darkMode
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.special.AntiForge
import net.ccbluex.liquidbounce.features.special.AutoReconnect.delay
import net.ccbluex.liquidbounce.features.special.BungeeCordSpoof
import net.ccbluex.liquidbounce.features.special.MacroManager.addMacro
import net.ccbluex.liquidbounce.features.special.MacroManager.macroMapping
import net.ccbluex.liquidbounce.file.FileConfig
import net.ccbluex.liquidbounce.file.FileManager
import net.ccbluex.liquidbounce.ui.client.GuiBackground.Companion.enabled
import net.ccbluex.liquidbounce.ui.client.GuiBackground.Companion.particles
import net.ccbluex.liquidbounce.ui.client.GuiMainMenu.Companion.useParallax
import net.ccbluex.liquidbounce.ui.client.altmanager.menus.altgenerator.GuiTheAltening.Companion.apiKey
import net.ccbluex.liquidbounce.utils.EntityUtils
import net.ccbluex.liquidbounce.value.Value
import java.io.*
import java.util.function.Consumer

class ValuesConfig
/**
 * Constructor of config
 *
 * @param file of config
 */
  (file: File?) : FileConfig(file) {
  /**
   * Load config from file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun loadConfig() {
    val jsonElement = JsonParser().parse(BufferedReader(FileReader(file)))
    if (jsonElement is JsonNull) return
    val jsonObject = jsonElement as JsonObject
    val iterator: Iterator<Map.Entry<String, JsonElement>> = jsonObject.entrySet().iterator()
    while (iterator.hasNext()) {
      val (key, value) = iterator.next()
      if (key.equals("CommandPrefix", ignoreCase = true)) {
        LiquidBounce.commandManager.prefix = value.asCharacter
      } else if (key.equals("ShowRichPresence", ignoreCase = true)) {
        LiquidBounce.clientRichPresence.showRichPresenceValue = value.asBoolean
      } else if (key.equals("targets", ignoreCase = true)) {
        val jsonValue = value as JsonObject
        if (jsonValue.has("TargetPlayer")) EntityUtils.targetPlayer = jsonValue["TargetPlayer"].asBoolean
        if (jsonValue.has("TargetMobs")) EntityUtils.targetMobs = jsonValue["TargetMobs"].asBoolean
        if (jsonValue.has("TargetAnimals")) EntityUtils.targetAnimals = jsonValue["TargetAnimals"].asBoolean
        if (jsonValue.has("TargetInvisible")) EntityUtils.targetInvisible = jsonValue["TargetInvisible"].asBoolean
        if (jsonValue.has("TargetDead")) EntityUtils.targetDead = jsonValue["TargetDead"].asBoolean
      } else if (key.equals("macros", ignoreCase = true)) {
        val jsonValue = value.asJsonArray
        for (macroElement in jsonValue) {
          val macroObject = macroElement.asJsonObject
          val keyValue = macroObject["key"]
          val commandValue = macroObject["command"]
          addMacro(keyValue.asInt, commandValue.asString)
        }
      } else if (key.equals("features", ignoreCase = true)) {
        val jsonValue = value as JsonObject
        if (jsonValue.has("DarkMode")) darkMode = jsonValue["DarkMode"].asBoolean
        if (jsonValue.has("AntiForge")) AntiForge.enabled = jsonValue["AntiForge"].asBoolean
        if (jsonValue.has("AntiForgeFML")) AntiForge.blockFML = jsonValue["AntiForgeFML"].asBoolean
        if (jsonValue.has("AntiForgeProxy")) AntiForge.blockProxyPacket = jsonValue["AntiForgeProxy"].asBoolean
        if (jsonValue.has("AntiForgePayloads")) AntiForge.blockPayloadPackets = jsonValue["AntiForgePayloads"].asBoolean
        if (jsonValue.has("BungeeSpoof")) BungeeCordSpoof.enabled = jsonValue["BungeeSpoof"].asBoolean
        if (jsonValue.has("AutoReconnectDelay")) delay = jsonValue["AutoReconnectDelay"].asInt
      } else if (key.equals("thealtening", ignoreCase = true)) {
        val jsonValue = value as JsonObject
        if (jsonValue.has("API-Key")) apiKey = jsonValue["API-Key"].asString
      } else if (key.equals("MainMenuParallax", ignoreCase = true)) {
        useParallax = value.asBoolean
      } else if (key.equals("Background", ignoreCase = true)) {
        val jsonValue = value as JsonObject
        if (jsonValue.has("Enabled")) enabled = jsonValue["Enabled"].asBoolean
        if (jsonValue.has("Particles")) particles = jsonValue["Particles"].asBoolean
      } else {
        val module = LiquidBounce.moduleManager.getModule(key)
        if (module != null) {
          val jsonModule = value as JsonObject
          for (moduleValue in module.values) {
            val element = jsonModule[moduleValue.name]
            if (element != null) moduleValue.fromJson(element)
          }
        }
      }
    }
  }

  /**
   * Save config to file
   *
   * @throws IOException
   */
  @Throws(IOException::class)
  override fun saveConfig() {
    val jsonObject = JsonObject()
    jsonObject.addProperty("CommandPrefix", LiquidBounce.commandManager.prefix)
    jsonObject.addProperty("ShowRichPresence", LiquidBounce.clientRichPresence.showRichPresenceValue)
    val jsonTargets = JsonObject()
    jsonTargets.addProperty("TargetPlayer", EntityUtils.targetPlayer)
    jsonTargets.addProperty("TargetMobs", EntityUtils.targetMobs)
    jsonTargets.addProperty("TargetAnimals", EntityUtils.targetAnimals)
    jsonTargets.addProperty("TargetInvisible", EntityUtils.targetInvisible)
    jsonTargets.addProperty("TargetDead", EntityUtils.targetDead)
    jsonObject.add("targets", jsonTargets)
    val jsonMacros = JsonArray()
    macroMapping.forEach { (k: Int?, v: String?) ->
      val jsonMacro = JsonObject()
      jsonMacro.addProperty("key", k)
      jsonMacro.addProperty("command", v)
      jsonMacros.add(jsonMacro)
    }
    jsonObject.add("macros", jsonMacros)
    val jsonFeatures = JsonObject()
    jsonFeatures.addProperty("DarkMode", darkMode)
    jsonFeatures.addProperty("AntiForge", AntiForge.enabled)
    jsonFeatures.addProperty("AntiForgeFML", AntiForge.blockFML)
    jsonFeatures.addProperty("AntiForgeProxy", AntiForge.blockProxyPacket)
    jsonFeatures.addProperty("AntiForgePayloads", AntiForge.blockPayloadPackets)
    jsonFeatures.addProperty("BungeeSpoof", BungeeCordSpoof.enabled)
    jsonFeatures.addProperty("AutoReconnectDelay", delay)
    jsonObject.add("features", jsonFeatures)
    val theAlteningObject = JsonObject()
    theAlteningObject.addProperty("API-Key", apiKey)
    jsonObject.add("thealtening", theAlteningObject)
    jsonObject.addProperty("MainMenuParallax", useParallax)
    val backgroundObject = JsonObject()
    backgroundObject.addProperty("Enabled", enabled)
    backgroundObject.addProperty("Particles", particles)
    jsonObject.add("Background", backgroundObject)
    LiquidBounce.moduleManager.modules.stream().filter { module: Module -> !module.values.isEmpty() }.forEach { module: Module ->
      val jsonModule = JsonObject()
      module.values.forEach(Consumer { value: Value<*> ->
        val jsonElement = value.toJson()
        if (jsonElement != null) jsonModule.add(value.name, jsonElement)
      })
      jsonObject.add(module.name, jsonModule)
    }
    val printWriter = PrintWriter(FileWriter(file))
    printWriter.println(FileManager.PRETTY_GSON.toJson(jsonObject))
    printWriter.close()
  }
}
