/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.utils.misc;

import net.ccbluex.liquidbounce.utils.MinecraftInstance;

import javax.swing.*;
import java.io.File;

public final class MiscUtils extends MinecraftInstance {

	public static void showErrorPopup(final String title, final String message) {
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
	}

	public static File openFileChooser() {
		if (mc.isFullScreen())
			mc.toggleFullscreen();

		final JFileChooser fileChooser = new JFileChooser();
		final JFrame frame = new JFrame();

		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		frame.setVisible(true);
		frame.toFront();
		frame.setVisible(false);

		final int action = fileChooser.showOpenDialog(frame);
		frame.dispose();

		return action == JFileChooser.APPROVE_OPTION ? fileChooser.getSelectedFile() : null;
	}

	public static File saveFileChooser() {
		if (mc.isFullScreen())
			mc.toggleFullscreen();

		final JFileChooser fileChooser = new JFileChooser();
		final JFrame frame = new JFrame();

		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		frame.setVisible(true);
		frame.toFront();
		frame.setVisible(false);

		final int action = fileChooser.showSaveDialog(frame);
		frame.dispose();

		return action == JFileChooser.APPROVE_OPTION ? fileChooser.getSelectedFile() : null;
	}
}
