package net.ccbluex.liquidbounce.utils

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.minecraft.entity.EntityLivingBase

class CombatManager : Listenable, MinecraftInstance() {
  private val lastAttackTimer = MSTimer()

  var inCombat = false
    private set
  var target: EntityLivingBase? = null
    private set
  val attackedEntityList = mutableListOf<EntityLivingBase>()
  val killaura: KillAura
    get() = LiquidBounce.moduleManager.getModule(KillAura::class.java)

  val blockingStatus: Boolean
    get() = killaura.blockingStatus

  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    if (mc.thePlayer == null) return

    // bypass java.util.ConcurrentModificationException
    attackedEntityList.map { it }.forEach {
      if (it.isDead) {
        LiquidBounce.eventManager.callEvent(EntityKilledEvent(it))
        attackedEntityList.remove(it)
      }
    }

    inCombat = false

    if (!lastAttackTimer.hasTimePassed(1000)) {
      inCombat = true
      return
    }

    if (target != null) {
      if (mc.thePlayer.getDistanceToEntity(target) > 7 || !inCombat || target!!.isDead) {
        target = null
      } else {
        inCombat = true
      }
    }
  }

  @EventTarget
  fun onAttack(event: AttackEvent) {
    val target = event.targetEntity

    if (target is EntityLivingBase && EntityUtils.isSelected(target, true)) {
      this.target = target
      if (!attackedEntityList.contains(target)) {
        attackedEntityList.add(target)
      }
    }
    lastAttackTimer.reset()
  }

  @EventTarget
  fun onWorld(event: WorldEvent) {
    inCombat = false
    target = null
    attackedEntityList.clear()
  }

  //    @EventTarget
  //    fun onPacket(event: PacketEvent) {
  //        val packet = event.packet
  //        if(packet is S02PacketChat) {
  //            val raw = packet.chatComponent.unformattedText
  //            val found = hackerWords.filter { raw.contains(it, true) }
  //            if(raw.contains(mc.session.username, true) && found.isNotEmpty()) {
  //                LiquidBounce.hud.addNotification(Notification("Someone call you a hacker!", found.joinToString(", "), NotifyType.ERROR))
  //            }
  //        }
  //    }

  fun getNearByEntity(radius: Float): EntityLivingBase? {
    return try {
      mc.theWorld.loadedEntityList.filter {
        mc.thePlayer.getDistanceToEntity(it) < radius && EntityUtils.isSelected(it, true)
      }.sortedBy { it.getDistanceToEntity(mc.thePlayer) }[0] as EntityLivingBase?
    } catch (e: Exception) {
      null
    }
  }


  override fun handleEvents() = true

}
