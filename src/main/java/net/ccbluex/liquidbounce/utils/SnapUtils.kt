package net.ccbluex.liquidbounce.utils

import org.lwjgl.input.Keyboard

object SnapUtils {
  fun snap(pos: Double): Double {
    return if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) pos
    else (pos / 4).toInt().toDouble() * 4
  }

  fun snap(pos: Int): Int {
    return snap(pos.toDouble()).toInt()
  }
}