/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.utils.timer

import java.security.SecureRandom

object TimeUtils {
  private val random = SecureRandom()
  fun randomDelay(minDelay: Int, maxDelay: Int): Long {
    return (minDelay + random.nextFloat() * (maxDelay - minDelay)).toLong()
  }

  fun randomClickDelay(minCPS: Int, maxCPS: Int): Long {
    return (random.nextFloat() * (1000 / minCPS - 1000 / maxCPS + 1) + 1000 / maxCPS).toLong()
  }
}