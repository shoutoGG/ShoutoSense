/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.utils.timer

class MSTimer {
  @JvmField var time = -1L
  fun hasTimePassed(ms: Long): Boolean {
    return System.currentTimeMillis() >= time + ms
  }

  fun hasTimePassed(ms: Int): Boolean {
    return hasTimePassed(ms.toLong())
  }

  fun hasTimeLeft(ms: Long): Long {
    return ms + time - System.currentTimeMillis()
  }

  fun reset() {
    time = System.currentTimeMillis()
  }
}
