package net.ccbluex.liquidbounce.utils.extensions

import java.awt.Rectangle

fun Rectangle.expand(amount: Int) {
  val a = amount.floorDiv(2)
  val b = amount - a

  this.x -= a
  this.width += b

  this.y -= a
  this.width += b
}