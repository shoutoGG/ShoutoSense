package net.ccbluex.liquidbounce.utils

import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.Listenable
import net.ccbluex.liquidbounce.event.PacketEvent
import net.minecraft.network.play.client.*

@Suppress("unused")
object BadPacketUtils : Listenable, MinecraftInstance() {

  private var slot = false
  private var attack = false
  private var swing = false
  private var block = false
  private var inventory = false

  fun bad(): Boolean {
    return bad(slot = true, attack = true, swing = true, block = true, inventory = true)
  }

  fun bad(type: String): Boolean {
    var slot = false
    var attack = false
    var swing = false
    var block = false
    var inventory = false
    for (s in type.split(Regex("[-+,.]"))) {
      when (s.lowercase()) {
        "slot" -> slot = true
        "attack" -> attack = true
        "swing" -> swing = true
        "block" -> block = true
        "inventory" -> inventory = true
      }
    }

    return bad(slot, attack, swing, block, inventory)
  }

  fun bad(slot: Boolean, attack: Boolean, swing: Boolean, block: Boolean, inventory: Boolean): Boolean {
    return this.slot && slot || this.attack && attack || this.swing && swing || this.block && block || this.inventory && inventory
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {

    when (event.packet) {
      is C09PacketHeldItemChange -> this.slot = true
      is C0APacketAnimation -> this.swing = true
      is C02PacketUseEntity -> this.attack = true

      is C07PacketPlayerDigging -> this.block = true
      is C08PacketPlayerBlockPlacement -> this.block = true

      is C0EPacketClickWindow -> this.inventory = true
      is C0DPacketCloseWindow -> this.inventory = true

      is C03PacketPlayer -> reset()
    }
  }

  fun reset() {
    this.slot = false
    this.swing = false
    this.attack = false
    this.block = false
    this.inventory = false
  }

  override fun handleEvents() = true
}
