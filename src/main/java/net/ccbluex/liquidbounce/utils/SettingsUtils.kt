/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.utils

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.modules.client.ConfigSettings
import net.ccbluex.liquidbounce.features.special.MacroManager
import net.ccbluex.liquidbounce.utils.misc.HttpUtils.get
import net.ccbluex.liquidbounce.utils.misc.StringUtils
import net.ccbluex.liquidbounce.utils.render.ColorUtils.translateAlternateColorCodes
import net.ccbluex.liquidbounce.value.*
import org.lwjgl.input.Keyboard
import java.io.File

object SettingsUtils {

  fun applySettings(scriptFile: File) {
    applySettings(scriptFile.readText())
  }

  /**
   * Execute settings [script]
   */
  fun applySettings(script: String) {
    LiquidBounce.isStarting = true
    script.lines().filter { it.isNotEmpty() && !it.startsWith('#') }.forEachIndexed { lineNumber, s ->
      val args = s.split(" ").toTypedArray()

      if (args.size <= 1) {
        ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §cSyntax error at line '$lineNumber' in setting script.\n§8§lLine: §7$s")
        return@forEachIndexed
      }

      when (args[0]) {
        "chat" -> ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §e${
          translateAlternateColorCodes(StringUtils.toCompleteString(args, 1))
        }")

        "unchat" -> ClientUtils.displayChatMessage(translateAlternateColorCodes(StringUtils.toCompleteString(args, 1)))

        "load" -> {
          val urlRaw = StringUtils.toCompleteString(args, 1)
          val url = urlRaw

          try {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §7Loading settings from §a§l$url§7...")
            applySettings(get(url))
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §7Loaded settings from §a§l$url§7.")
          } catch (e: Exception) {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §7Failed to load settings from §a§l$url§7.")
          }
        }

        "macro" -> {
          if (args[1] != "0") {
            val macroBind = args[1]
            val macroCommand = StringUtils.toCompleteString(args, 2)
            try {
              MacroManager.addMacro(macroBind.toInt(), macroCommand)
              ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] Macro §c§l$macroCommand§7 has been bound to §a§l$macroBind§7.")
            } catch (e: Exception) {
              ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${e.javaClass.name}§7(${e.message}) §cAn Exception occurred while importing macro with keybind §a§l$macroBind§c to §a§l$macroCommand§c.")
            }
          }
        }

        "targetPlayer", "targetPlayers" -> {
          EntityUtils.targetPlayer = args[1].equals("true", ignoreCase = true)
          ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${args[0]}§7 set to §c§l${EntityUtils.targetPlayer}§7.")
        }

        "targetMobs" -> {
          EntityUtils.targetMobs = args[1].equals("true", ignoreCase = true)
          ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${args[0]}§7 set to §c§l${EntityUtils.targetMobs}§7.")
        }

        "targetAnimals" -> {
          EntityUtils.targetAnimals = args[1].equals("true", ignoreCase = true)
          ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${args[0]}§7 set to §c§l${EntityUtils.targetAnimals}§7.")
        }

        "targetInvisible" -> {
          EntityUtils.targetInvisible = args[1].equals("true", ignoreCase = true)
          ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${args[0]}§7 set to §c§l${EntityUtils.targetInvisible}§7.")
        }

        "targetDead" -> {
          EntityUtils.targetDead = args[1].equals("true", ignoreCase = true)
          ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${args[0]}§7 set to §c§l${EntityUtils.targetDead}§7.")
        }

        else -> {
          if (args.size < 3) {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §cSyntax error at line '$lineNumber' in setting script.\n§8§lLine: §7$s")
            return@forEachIndexed
          }

          val moduleName = args[0]
          val valueName = args[1]
          val value = args.copyOfRange(2, 99.coerceAtMost(args.size)).joinToString(separator = " ")
          val module = LiquidBounce.moduleManager.getModule(moduleName)

          if (module == null) {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §cModule §a§l$moduleName§c was not found!")
            return@forEachIndexed
          }

          if (!shouldLoad(module)) return

          if (valueName.equals("toggle", ignoreCase = true)) {
            if (ConfigSettings.loadState.get()) {
              module.state = value.equals("true", ignoreCase = true)
              ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${module.name} §7was toggled §c§l${if (module.state) "on" else "off"}§7.")
            }
            return@forEachIndexed
          }

          if (valueName.equals("bind", ignoreCase = true)) {
            if (ConfigSettings.loadBind.get()) {
              module.keyBind = Keyboard.getKeyIndex(value)
              ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${module.name} §7was bound to §c§l${
                Keyboard.getKeyName(module.keyBind)
              }§7.")
            }
            return@forEachIndexed
          }

          val moduleValue = module.getValue(valueName)
          if (moduleValue == null) {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §cValue §a§l$valueName§c isn't in module §a§l$moduleName§c.")
            return@forEachIndexed
          }

          try {
            when (moduleValue) {
              is BoolValue -> moduleValue.set(value.toBoolean())
              is FloatValue -> moduleValue.set(value.toFloat())
              is IntegerValue -> moduleValue.set(value.toInt())
              is TextValue -> moduleValue.set(value)
              is ListValue -> moduleValue.set(value)
              is MultiBoolValue -> moduleValue.set(value) // why is this shit in plain text
            }
          } catch (e: Exception) {
            ClientUtils.displayChatMessage("§7[§3§lAutoSettings§7] §a§l${e.javaClass.name}§7(${e.message}) §cAn Exception occurred while setting §a§l$value§c to §a§l${moduleValue.name}§c in §a§l${module.name}§c.")
          }
        }
      }
    }

    LiquidBounce.isStarting = false
    LiquidBounce.fileManager.saveConfig(LiquidBounce.fileManager.valuesConfig)
  }

  /**
   * Generate settings script
   */
  fun dumpSettings(): String {
    val stringBuilder = StringBuilder()

    MacroManager.macroMapping.filter { it.key != 0 }.forEach { stringBuilder.append("macro ${it.key} ${it.value}").append("\n") }

    LiquidBounce.moduleManager.modules.filter { shouldSave(it) }.forEach {
      it.values.forEach { value ->
        stringBuilder.append("${it.name} ${value.name} ${value.get()}").append("\n")
      }

      if (ConfigSettings.saveState.get()) stringBuilder.append("${it.name} toggle ${it.state}").append("\n")

      if (ConfigSettings.saveBind.get()) stringBuilder.append("${it.name} bind ${Keyboard.getKeyName(it.keyBind)}").append("\n")
    }

    return stringBuilder.toString()
  }

  private fun shouldSave(module: Module): Boolean {
    return when (module.category) {
      ModuleCategory.COMBAT -> ConfigSettings.saveCombatModules.get()
      ModuleCategory.PLAYER -> ConfigSettings.savePlayerModules.get()
      ModuleCategory.MOVEMENT -> ConfigSettings.saveMovementModules.get()
      ModuleCategory.RENDER -> ConfigSettings.saveRenderModules.get()
      ModuleCategory.CLIENT -> ConfigSettings.saveClientModules.get()
      ModuleCategory.WORLD -> ConfigSettings.saveWorldModules.get()
      ModuleCategory.MISC -> ConfigSettings.saveMiscModules.get()
      ModuleCategory.EXPLOIT -> ConfigSettings.saveExploitModules.get()
      ModuleCategory.GHOST -> ConfigSettings.saveGhostModules.get()
      ModuleCategory.COLOR -> ConfigSettings.saveColorModules.get()
    }
  }

  private fun shouldLoad(module: Module): Boolean {
    return when (module.category) {
      ModuleCategory.COMBAT -> ConfigSettings.loadCombatModules.get()
      ModuleCategory.PLAYER -> ConfigSettings.loadPlayerModules.get()
      ModuleCategory.MOVEMENT -> ConfigSettings.loadMovementModules.get()
      ModuleCategory.RENDER -> ConfigSettings.loadRenderModules.get()
      ModuleCategory.CLIENT -> ConfigSettings.loadClientModules.get()
      ModuleCategory.WORLD -> ConfigSettings.loadWorldModules.get()
      ModuleCategory.MISC -> ConfigSettings.loadMiscModules.get()
      ModuleCategory.EXPLOIT -> ConfigSettings.loadExploitModules.get()
      ModuleCategory.GHOST -> ConfigSettings.loadGhostModules.get()
      ModuleCategory.COLOR -> ConfigSettings.loadColorModules.get()
    }
  }
}
