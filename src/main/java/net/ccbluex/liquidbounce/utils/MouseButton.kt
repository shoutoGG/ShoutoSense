package net.ccbluex.liquidbounce.utils

enum class MouseButton(private val index: Int) {
  LEFT(0), MIDDLE(1), RIGHT(2)
}
