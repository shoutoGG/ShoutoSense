/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module

import java.awt.Color

enum class ModuleCategory(val displayName: String, val astolfoColor: Color) {
  CLIENT("Client", Color(160, 55, 63)), COMBAT("Combat", Color(232, 76, 60, 255)), PLAYER("Player", Color(141, 68, 173, 255)), MOVEMENT("Movement", Color(46, 204, 113, 255)), RENDER("Render", Color(55, 0, 206, 255)), WORLD("World", Color(202, 223, 111)), MISC("Misc", Color(50, 137, 90)), EXPLOIT("Exploit", Color(51, 152, 217, 255)), COLOR("Color", Color(0, 0, 0, 255)), GHOST("Ghost", Color(223, 153, 90, 255))
}