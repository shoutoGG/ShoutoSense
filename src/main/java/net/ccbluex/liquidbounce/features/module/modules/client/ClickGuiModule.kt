/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.color.ColorMixer
import net.ccbluex.liquidbounce.ui.client.clickgui.ClickGui
import net.ccbluex.liquidbounce.ui.client.clickgui.style.styles.LiquidBounceStyle
import net.ccbluex.liquidbounce.utils.render.ColorUtils.LiquidSlowly
import net.ccbluex.liquidbounce.utils.render.ColorUtils.fade
import net.ccbluex.liquidbounce.utils.render.RenderUtils.getRainbowOpaque
import net.ccbluex.liquidbounce.utils.render.RenderUtils.skyRainbow
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.ListValue
import org.lwjgl.input.Keyboard
import java.awt.Color
import java.util.*

@ModuleInfo(name = "ClickGuiModule", description = "Opens the ClickGuiModule.", category = ModuleCategory.CLIENT, keyBind = Keyboard.KEY_RSHIFT, forceNoSound = true, onlyEnable = true)
object ClickGuiModule : Module() {
  val scaleValue = FloatValue("Scale", 1f, 0.4f, 2f)
  val maxElementsValue = IntegerValue("MaxElements", 15, 1, 20)
  val backgroundValue = ListValue("Background", arrayOf("Default", "Gradient", "None"), "Default")
  val gradStartValue = IntegerValue("GradientStartAlpha", 255, 0, 255) { backgroundValue.get().equals("gradient", ignoreCase = true) }
  val gradEndValue = IntegerValue("GradientEndAlpha", 0, 0, 255) { backgroundValue.get().equals("gradient", ignoreCase = true) }
  val animationValue = ListValue("Animation", arrayOf("Azura", "Slide", "Quint", "SlideBounce", "Zoom", "ZoomBounce", "None"), "Azura")
  val animSpeedValue = FloatValue("AnimSpeed", 1f, 0.01f, 5f, "x")
  val colorModeValue = ListValue("Color", arrayOf("Custom", "Sky", "Rainbow", "LiquidSlowly", "Fade", "Mixer"), "Custom")
  val colorRedValue = IntegerValue("Red", 0, 0, 255)
  val colorGreenValue = IntegerValue("Green", 160, 0, 255)
  val colorBlueValue = IntegerValue("Blue", 255, 0, 255)
  val colorHoverRedValue = IntegerValue("HoverRed", 0, 0, 255)
  val colorHoverGreenValue = IntegerValue("HoverGreen", 160, 0, 255)
  val colorHoverBlueValue = IntegerValue("HoverBlue", 255, 0, 255)
  val saturationValue = FloatValue("Saturation", 1f, 0f, 1f)
  val brightnessValue = FloatValue("Brightness", 1f, 0f, 1f)
  val mixerSecondsValue = IntegerValue("Seconds", 2, 1, 10)
  val panelOpenAnim = BoolValue("PanelOpenAnimation", true)
  val panelOpenAnimMS = IntegerValue("PanelOpenAnimationSpeed", 400, 0, 1000, "ms") { panelOpenAnim.get() }
  val panelScrollAnim = BoolValue("PanelScrollAnim", true)
  val panelScrollAnimMS = IntegerValue("PanelScrollAnimMS", 400, 0, 1000) { panelScrollAnim.get() }
  val valuesOpenAnim = BoolValue("ValuesOpenAnim", true)
  val valuesOpenAnimMS = IntegerValue("ValuesOpenAnimMS", 100, 0, 200) { valuesOpenAnim.get() }

  override fun onEnable() {
    updateStyle()
    LiquidBounce.clickGui.progress = 0.0
    LiquidBounce.clickGui.slide = 0.0
    ClickGui.lastMS = System.currentTimeMillis()
    mc.displayGuiScreen(LiquidBounce.clickGui)
  }

  private fun updateStyle() {
    LiquidBounce.clickGui.style = LiquidBounceStyle()
  }

  fun hoverColor(): Color {
    return Color(colorHoverRedValue.get(), colorHoverGreenValue.get(), colorHoverBlueValue.get(), 255)
  }

  fun generateColor(): Color {
    var c = Color(255, 255, 255, 255)
    when (colorModeValue.get().lowercase(Locale.getDefault())) {
      "custom" -> c = Color(colorRedValue.get(), colorGreenValue.get(), colorBlueValue.get())
      "rainbow" -> c = Color(getRainbowOpaque(mixerSecondsValue.get(), saturationValue.get(), brightnessValue.get(), 0))
      "sky" -> c = skyRainbow(0, saturationValue.get(), brightnessValue.get())
      "liquidslowly" -> c = LiquidSlowly(System.nanoTime(), 0, saturationValue.get(), brightnessValue.get())
      "fade" -> c = fade(Color(colorRedValue.get(), colorGreenValue.get(), colorBlueValue.get()), 0, 100)
      "mixer" -> c = ColorMixer.getMixedColor(0, mixerSecondsValue.get())
    }
    return c
  }
}
