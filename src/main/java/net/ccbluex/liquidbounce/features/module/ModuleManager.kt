/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.KeyEvent
import net.ccbluex.liquidbounce.event.Listenable
import net.ccbluex.liquidbounce.features.module.modules.client.*
import net.ccbluex.liquidbounce.features.module.modules.color.ColorMixer
import net.ccbluex.liquidbounce.features.module.modules.combat.*
import net.ccbluex.liquidbounce.features.module.modules.exploit.*
import net.ccbluex.liquidbounce.features.module.modules.ghost.*
import net.ccbluex.liquidbounce.features.module.modules.misc.*
import net.ccbluex.liquidbounce.features.module.modules.movement.*
import net.ccbluex.liquidbounce.features.module.modules.player.*
import net.ccbluex.liquidbounce.features.module.modules.render.*
import net.ccbluex.liquidbounce.features.module.modules.world.*
import net.ccbluex.liquidbounce.features.module.modules.world.Timer
import net.ccbluex.liquidbounce.utils.ClientUtils
import java.util.*


class ModuleManager : Listenable {

  val modules = TreeSet<Module> { module1, module2 -> module1.name.compareTo(module2.name) }
  private val moduleClassMap = hashMapOf<Class<*>, Module>()

  val shouldNotify: Boolean
    get() = ClientSettings.showToggleNotification.get()
  val toggleSoundMode: Int
    get() = ClientSettings.playToggleSound.index
  val toggleVolume: Float
    get() = ClientSettings.toggleVolume.get().toFloat()

  init {
    LiquidBounce.eventManager.registerListener(this)
  }

  /**
   * Register all modules
   */
  fun registerModules() {
    ClientUtils.getLogger().info("[ModuleManager] Loading modules...")

    registerModules(
      AbortBreaking::class.java,
      Aimbot::class.java,
      AirJump::class.java,
      AirLadder::class.java,
      Ambience::class.java,
      AntiAFK::class.java,
      AntiBan::class.java,
      AntiBlind::class.java,
      AntiBot::class.java,
      AntiCactus::class.java,
      AntiCheat::class.java,
      AntiDesync::class.java,
      AntiExploit::class.java,
      AntiFireBall::class.java,
      AntiHunger::class.java,
      AntiVanish::class.java,
      AntiVoid::class.java,
      AsianHat::class.java,
      NewClickGui::class.java,
      AtAll::class.java,
      AuraRange::class.java,
      AuthBypass::class.java,
      AutoBow::class.java,
      AutoBreak::class.java,
      AutoClicker::class.java,
      AutoDisable::class.java,
      AutoFish::class.java,
      AutoHypixel::class.java,
      AutoJump::class.java,
      AutoKit::class.java,
      AutoLeave::class.java,
      AutoLogin::class.java,
      AutoPlay::class.java,
      AutoPot::class.java,
      AutoRespawn::class.java,
      AutoSoup::class.java,
      AutoTool::class.java,
      AutoWalk::class.java,
      AutoWeapon::class.java,
      BedGodMode::class.java,
      BedInstantLeave::class.java,
      Blink::class.java,
      BlockESP::class.java,
      BlockOverlay::class.java,
      BlockWalk::class.java,
      BowAimbot::class.java,
      BowJump::class.java,
      Breadcrumbs::class.java,
      BufferSpeed::class.java,
      CameraClip::class.java,
      Cape::class.java,
      Chams::class.java,
      ChestStealer::class.java,
      CivBreak::class.java,
      Clip::class.java,
      ColorMixer::class.java,
      ComboOneHit::class.java,
      ConsoleSpammer::class.java,
      Criticals::class.java,
      Crosshair::class.java,
      CustomDisabler::class.java,
      CustomGapple::class.java,
      Damage::class.java,
      DamageParticle::class.java,
      Disabler::class.java,
      ESP2D::class.java,
      ESP::class.java,
      Eagle::class.java,
      EnchantEffect::class.java,
      FakeLag::class.java,
      FastBow::class.java,
      FastBreak::class.java,
      FastClimb::class.java,
      FastPlace::class.java,
      FastStairs::class.java,
      FastUse::class.java,
      Fly::class.java,
      ForceUnicodeChat::class.java,
      FreeCam::class.java,
      Freeze::class.java,
      Fullbright::class.java,
      Gapple::class.java,
      GhostHand::class.java,
      GodMode::class.java,
      HUD::class.java,
      HUDDesigner::class.java,
      Heal::class.java,
      HighJump::class.java,
      HitBox::class.java,
      HoverDetails::class.java,
      HpLog::class.java,
      IceSpeed::class.java,
      InvManager::class.java,
      InvMove::class.java,
      ItemESP::class.java,
      ItemPhysics::class.java,
      KeepAlive::class.java,
      KeepContainer::class.java,
      KeepSprint::class.java,
      Kick::class.java,
      KillAura::class.java,
      LadderJump::class.java,
      Lightning::class.java,
      LiquidInteract::class.java,
      LiquidWalk::class.java,
      LongJump::class.java,
      MidClick::class.java,
      MultiActions::class.java,
      NameProtect::class.java,
      NameTags::class.java,
      NewGUI::class.java,
      NoAchievements::class.java,
      NoClip::class.java,
      NoFOV::class.java,
      NoFall::class.java,
      NoFriends::class.java,
      NoHurtCam::class.java,
      NoInvClose::class.java,
      NoJumpDelay::class.java,
      NoRender::class.java,
      NoRotateSet::class.java,
      NoSlow::class.java,
      NoSlowBreak::class.java,
      NoWeb::class.java,
      Nuker::class.java,
      PackSpoofer::class.java,
      PacketFixer::class.java,
      PacketLogger::class.java,
      Parkour::class.java,
      PerfectHorseJump::class.java,
      Phase::class.java,
      PingSpoof::class.java,
      Plugins::class.java,
      PointerESP::class.java,
      PortalMenu::class.java,
      PotionSaver::class.java,
      Projectiles::class.java,
      ProphuntESP::class.java,
      Reach::class.java,
      Regen::class.java,
      ResetVL::class.java,
      ReverseStep::class.java,
      Rotations::class.java,
      SafeWalk::class.java,
      Scaffold::class.java,
      ServerCrasher::class.java,
      Skeletal::class.java,
      SlimeJump::class.java,
      Sneak::class.java,
      Spammer::class.java,
      Speed::class.java,
      SpeedMine::class.java,
      SpinBot::class.java,
      Sprint::class.java,
      Step::class.java,
      StorageESP::class.java,
      Strafe::class.java,
      SuperKnockback::class.java,
      SuperheroFX::class.java,
      TNTBlock::class.java,
      TNTESP::class.java,
      TargetMark::class.java,
      TargetStrafe::class.java,
      Teams::class.java,
      Teleport::class.java,
      TeleportAura::class.java,
      TestValues::class.java,
      Timer::class.java,
      Tracers::class.java,
      TrueSight::class.java,
      VanillaFly::class.java,
      Velocity::class.java,
      WallClimb::class.java,
      WaterSpeed::class.java,
      XCarry::class.java,
      XRay::class.java,
      Zoot::class.java,
    )

    registerModule(Animations)
    registerModule(IRC)
    registerModule(ClickGuiModule)
    registerModule(Fucker)
    registerModule(ChestAura)
    registerModule(ConfigSettings)
    registerModule(ClientSettings)

    ClientUtils.getLogger().info("[ModuleManager] Successfully loaded ${modules.size} modules.")
  }

  /**
   * Register [module]
   */
  fun registerModule(module: Module) {
    modules += module
    moduleClassMap[module.javaClass] = module

    module.onInitialize()
    generateCommand(module)
    LiquidBounce.eventManager.registerListener(module)
  }

  /**
   * Register [moduleClass]
   */
  private fun registerModule(moduleClass: Class<out Module>) {
    try {
      registerModule(moduleClass.newInstance())
    } catch (e: Throwable) {
      ClientUtils.getLogger().error("Failed to load module: ${moduleClass.name} (${e.javaClass.name}: ${e.message})")
    }
  }

  /**
   * Register a list of modules
   */
  @SafeVarargs
  fun registerModules(vararg modules: Class<out Module>) {
    modules.forEach(this::registerModule)
  }

  /**
   * Unregister module
   */
  fun unregisterModule(module: Module) {
    modules.remove(module)
    moduleClassMap.remove(module::class.java)
    LiquidBounce.eventManager.unregisterListener(module)
  }

  /**
   * Generate command for [module]
   */
  internal fun generateCommand(module: Module) {
    val values = module.values

    if (values.isEmpty()) return

    LiquidBounce.commandManager.registerCommand(ModuleCommand(module, values))
  }

  /**
   * Legacy stuff
   *
   * TODO: Remove later when everything is translated to Kotlin
   */

  /**
   * Get module by [moduleClass]
   */
  fun <T : Module> getModule(moduleClass: Class<T>): T = moduleClassMap[moduleClass] as T

  operator fun <T : Module> get(clazz: Class<T>) = getModule(clazz)

  /**
   * Get module by [moduleName]
   */
  fun getModule(moduleName: String?) = modules.find { it.name.equals(moduleName, ignoreCase = true) }

  /**
   * Module related events
   */

  /**
   * Handle incoming key presses
   */
  @EventTarget
  private fun onKey(event: KeyEvent) = modules.filter { it.keyBind == event.key }.forEach { it.toggle() }

  override fun handleEvents() = true
}
