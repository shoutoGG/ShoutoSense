/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.combat

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EntityDamageEvent
import net.ccbluex.liquidbounce.event.EntityKilledEvent
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.script.api.global.Chat
import net.ccbluex.liquidbounce.value.BoolValue
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.EnumChatFormatting
import kotlin.math.roundToInt

@ModuleInfo(name = "HpLog", description = "log le hp", category = ModuleCategory.COMBAT, array = false)
class HpLog : Module() {

  private val combat = BoolValue("Combat", true)
  private val death = BoolValue("Death", true)
  private val blockWarn = BoolValue("BlockWarn", true)
  private val moreInfo = BoolValue("MoreInfo", false)
  private var msg = ""
  private var oldmsg = ""
  private val ka: KillAura
    get() = LiquidBounce.moduleManager.getModule(KillAura::class.java)

  override fun onDisable() {
    oldmsg = ""
    msg = ""
  }

  private fun selfHealth(): Float {
    val h = mc.thePlayer.health + mc.thePlayer.absorptionAmount
    return (h * 100).roundToInt() / 100f
  }

  private fun targetHealth(): Float {
    val h = ka.target!!.health + ka.target!!.absorptionAmount
    return (h * 100).roundToInt() / 100f
  }

  private fun format(): String {
    var sign = '='
    var color = EnumChatFormatting.YELLOW

    val selfHealth = selfHealth()
    val targetHealth = targetHealth()

    if (selfHealth > targetHealth) {
      sign = '>'
      color = EnumChatFormatting.GREEN
    } else if (selfHealth < targetHealth) {
      sign = '<'
      color = EnumChatFormatting.RED
    }

    return "$color[HpLog] self: $selfHealth $sign target(${ka.target!!.name}): $targetHealth"
  }

  private fun moreInfo(e: EntityLivingBase): String {
    return "${e.name} ground=${if (e.onGround) 1 else 0} health=${e.health.roundToInt()}/${e.maxHealth.roundToInt()} hurt=${e.hurtTime}/${e.maxHurtTime} hurtR=${e.hurtResistantTime}/${e.maxHurtResistantTime}"
  }

  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    oldmsg = msg
    if (ka.target == null) return
    msg = format()
    if (msg == oldmsg) return
    if (combat.get()) Chat.print(msg)
  }

  @EventTarget
  fun onEntityKilled(event: EntityKilledEvent) {
    val entity = event.targetEntity
    if (entity != mc.thePlayer && death.get()) print("[HpLog] Won with ${selfHealth()} HP left")
  }

  @EventTarget
  fun onEntityDamaged(event: EntityDamageEvent) {
    if (blockWarn.get() && event.damagedEntity == mc.thePlayer && !ka.blockingStatus && !ka.autoBlockModeValue.eq("off")) {
      chat("§ctook damage but §lnot blocking§r§c even though autoblock is on")
    }

    if (ka.target == null || event.damagedEntity !is EntityLivingBase) return
    if (moreInfo.get() && (event.damagedEntity == mc.thePlayer || event.damagedEntity == ka.target)) {
      print("[HpLog] ${moreInfo(mc.thePlayer)}")
      print("[HpLog] ${moreInfo(event.damagedEntity)}")
    }
  }
}
