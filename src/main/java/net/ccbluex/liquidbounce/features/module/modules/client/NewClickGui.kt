package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.ui.client.clickgui.astolfo.AstolfoClickGui
import net.ccbluex.liquidbounce.value.FloatValue

@ModuleInfo(name = "NewClickGui", description = "Opens the Astolfo's ClickGuiModule.", category = ModuleCategory.CLIENT, forceNoSound = true, onlyEnable = true)
class NewClickGui : Module() {
  val scale = FloatValue("scale", 1f, 0f, 10f)
  val scroll = FloatValue("Scroll", 20f, 0f, 200f)
  private var instance: AstolfoClickGui? = null

  override fun onEnable() {
    val newInstance = AstolfoClickGui()
    if (instance == null) instance = newInstance
    mc.displayGuiScreen(instance)
  }
}