package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C03PacketPlayer

class BadPacketA : Check("more than 20 noGround C03 packets in a row") {
  private var streak = 0

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C03PacketPlayer) {
      if (packet.onGround || mc.thePlayer.capabilities.allowFlying) streak = 0
      else if (++streak > 20) {
        flag()
        streak = 0
      }
    }
  }
}