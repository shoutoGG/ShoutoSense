/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.player

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.ccbluex.liquidbounce.value.*
import net.minecraft.network.play.client.C03PacketPlayer
import java.security.SecureRandom

@ModuleInfo(name = "Damage", spacedName = "Damage", description = "change shit when u get damaged", category = ModuleCategory.PLAYER)
class Damage : Module() {
  private val event = MultiBoolValue("Event", linkedMapOf("Update" to true, "Render3D" to false, "MotionPre" to false, "MotionPost" to false))
  private val timer = BoolValue("Timer", true)
  private val timerMS = IntegerValue("TimerMS", 150, 0, 1000) { timer.get() }
  private val timerSpeed = FloatValue("TimerSpeed", 1f, 0f, 4f) { timer.get() }

  private val strafe = BoolValue("Strafe", true)
  private val strafeMode = ListValue("StrafeMode", arrayOf("Custom", "Current", "Add", "Multiply"), "Current") { strafe.get() }
  private val strafeAdd = FloatValue("StrafeAdd", 0.1f, 0f, 1f) { strafe.get() && strafeMode.eq("add") }
  private val strafeMultiply = FloatValue("StrafeMultiply", 1.1f, 0f, 2f) { strafe.get() && strafeMode.eq("multiply") }
  private val strafeCutom = FloatValue("StrafeCustom", 0.3f, 0f, 2f) { strafe.get() && strafeMode.eq("custom") }
  private val strafeGroundMultiplier = FloatValue("StrafeGround", 0.5f, 0f, 1f) { strafe.get() }
  private val strafeMS = IntegerValue("StrafeMS", 100, 0, 1000) { strafe.get() }

  private val swing = BoolValue("Swing", false)
  private val regen = BoolValue("Regen", false)
  private val minRegenChance: IntegerValue = object : IntegerValue("MinRegenChance", 50, 0, 100, "%", { regen.get() }) {
    override fun onChange(oldValue: Int, newValue: Int) {
      if (newValue > maxRegenChance.get()) set(maxRegenChance.get())
    }
  }

  private val maxRegenChance: IntegerValue = object : IntegerValue("MaxRegenChance", 50, 0, 100, "%", { regen.get() }) {
    override fun onChange(oldValue: Int, newValue: Int) {
      if (newValue < minRegenChance.get()) set(minRegenChance.get())
    }
  }

  private val regenPacketCount = IntegerValue("RegenPacketCount", 1, 0, 20) { regen.get() }

  private val blink = BoolValue("Blink", false)
  private val blinkMS = IntegerValue("BlinkMS", 150, 0, 500) { blink.get() }

  private val random = SecureRandom()
  private val shouldRegen: Boolean
    get() = random.nextFloat() <= (minRegenChance.get() + (maxRegenChance.get() - minRegenChance.get()) * random.nextFloat())
  private val blinkModule: Blink
    get() = LiquidBounce.moduleManager.getModule(Blink::class.java)

  private var timerActivated = false
  private var strafeActivated = false
  private var blinkActivated = false
  private val setTimerTimer = MSTimer()
  private val strafeTimer = MSTimer()
  private val blinkTimer = MSTimer()

  override fun onDisable() {
    mc.timer.timerSpeed = 1F
  }

  fun ground(v: Float) = v * if (mc.thePlayer.onGround) strafeGroundMultiplier.get() else 1f

  @EventTarget
  fun onEntityDamage(event: EntityDamageEvent) {
    val entity = event.damagedEntity
    if (entity != mc.thePlayer) return

    if (blink.get()) {
      blinkTimer.reset()
      blinkModule.state = true
      blinkActivated = true
      debug("damaged, starte blink")
    }
    if (timer.get()) {
      setTimerTimer.reset()
      mc.timer.timerSpeed = timerSpeed.get()
      timerActivated = true
      debug("damaged, timer set to ${timerSpeed.get()}")
    }

    if (strafe.get()) {
      strafeTimer.reset()
      strafeActivated = true
      debug("damaged, strafing")
    }

    if (shouldRegen) {
      repeat(regenPacketCount.get()) {
        mc.netHandler.addToSendQueue(C03PacketPlayer(mc.thePlayer.onGround))
      }
    }

    if (swing.get()) mc.thePlayer.swingItem()
  }

  private fun work() {
    if (setTimerTimer.hasTimePassed(timerMS.get()) && timerActivated) {
      timerActivated = false
      setTimerTimer.reset()
      if (mc.timer.timerSpeed != 1f) debug("reset timer")
      mc.timer.timerSpeed = 1f
    } else if (timerActivated) {
      debug("damaged, timer set to ${timerSpeed.get()}")
      mc.timer.timerSpeed = timerSpeed.get()
    }

    if (strafeTimer.hasTimePassed(strafeMS.get()) && strafeActivated) {
      strafeActivated = false
      strafeTimer.reset()
    } else if (strafeActivated) {
      debug("strafe")
      when (strafeMode.get().lowercase()) {
        "current" -> MovementUtils.strafe()
        "add" -> MovementUtils.strafe(ground(MovementUtils.speed + strafeAdd.get()))
        "multiply" -> MovementUtils.strafe(ground(MovementUtils.speed * strafeMultiply.get()))
        "custom" -> MovementUtils.strafe(ground(strafeCutom.get()))
      }
    }

    if (blinkTimer.hasTimePassed(blinkMS.get()) && blinkActivated) {
      blinkModule.state = false
      blinkActivated = false
      blinkTimer.reset()
    }
  }

  @EventTarget
  fun onUpdate(ignored: UpdateEvent) {
    if (event.get("Update")) work()
  }

  @EventTarget
  fun onRender3D(e: Render3DEvent) {
    if (event.get("Render3D")) work()
  }

  @EventTarget
  fun onMotion(e: MotionEvent) {
    if (event.get("MotionPre") && e.eventState == EventState.PRE) work()
    if (event.get("MotionPost") && e.eventState == EventState.POST) work()
  }


  override val tag: String
    get() = values.filter { it is BoolValue && it.get() }.joinToString(",") { it.name }
}
