package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.watchdog

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventState
import net.ccbluex.liquidbounce.event.MotionEvent
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.Speed
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.ccbluex.liquidbounce.utils.timer.TickTimer
import net.minecraft.potion.Potion

class WatchdogNew : SpeedMode("WatchdogNew") {
  private val tickTimer = TickTimer()
  private var groundTick = 0
  override fun onMotion(eventMotion: MotionEvent) {
    val speed = LiquidBounce.moduleManager.getModule(Speed::class.java)
    if (speed == null || eventMotion.eventState !== EventState.PRE) return
    if (!mc.thePlayer.onGround || !MovementUtils.isMoving) {
      mc.timer.timerSpeed = 1f
    }
    if (MovementUtils.isMoving) {
      if (mc.thePlayer.onGround) {
        if (groundTick >= 0) {
          mc.timer.timerSpeed = 1.02f
          if (speed.sendJumpValue.get()) {
            mc.thePlayer.jump()
          } else {
            mc.thePlayer.motionY = 0.42
          }
          if (!mc.thePlayer.isPotionActive(Potion.moveSpeed)) {
            MovementUtils.strafe(0.43f)
          }
          if (mc.thePlayer.isPotionActive(Potion.moveSpeed)) {
            MovementUtils.strafe(0.63f)
          }
        }
        groundTick++
      } else {
        groundTick = 0
        mc.thePlayer.motionY += -0.03 * 0.03
      }
    }
  }

  override fun onEnable() {
    val speed = LiquidBounce.moduleManager.getModule(Speed::class.java)
    super.onEnable()
  }

  override fun onDisable() {
    mc.timer.timerSpeed = 1f
    super.onDisable()
  }

  override fun onUpdate() {}
  override fun onMotion() {}
  override fun onMove(event: MoveEvent) {}
}