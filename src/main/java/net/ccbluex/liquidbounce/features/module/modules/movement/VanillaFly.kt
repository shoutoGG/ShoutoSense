package net.ccbluex.liquidbounce.features.module.modules.movement

import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.minecraft.network.play.client.C00PacketKeepAlive
import net.minecraft.network.play.client.C03PacketPlayer

@ModuleInfo(name = "VanillaFly", spacedName = "Vanilla Fly", description = "funny longjump", category = ModuleCategory.MOVEMENT)
class VanillaFly : Module() {
  private val speedValue = FloatValue("Speed", 2f, 0f, 5f)
  private val vspeedHValue = FloatValue("VerticalMove", 2f, 0f, 5f)
  private val vspeedValue = FloatValue("Vertical", 1f, 0f, 3f)
  private val kickBypassValue = BoolValue("KickBypass", false)
  private val keepAliveValue = BoolValue("KeepAlive", false) // old KeepAlive fly combined
  private val noClipValue = BoolValue("NoClip", false)
  private val spoofValue = BoolValue("SpoofGround", false)

  private var packets = 0

  val vertical: Float
    get() = if (mc.gameSettings.keyBindForward.pressed || mc.gameSettings.keyBindBack.pressed || mc.gameSettings.keyBindLeft.pressed || mc.gameSettings.keyBindRight.pressed) vspeedHValue.get() else vspeedValue.get()

  override fun onEnable() {
    packets = 0
  }

  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    if (keepAliveValue.get()) {
      mc.netHandler.addToSendQueue(C00PacketKeepAlive())
    }
    if (noClipValue.get()) {
      mc.thePlayer.noClip = true
    }

    mc.thePlayer.capabilities.isFlying = false
    MovementUtils.resetMotion(true)
    if (mc.gameSettings.keyBindJump.isKeyDown) {
      mc.thePlayer.motionY += vertical
      debug("went up")
    }

    if (mc.gameSettings.keyBindSneak.isKeyDown) {
      mc.thePlayer.motionY -= vertical
      debug("went down")
    }

    MovementUtils.strafe(speedValue.get())
    debug("he flew he flew")
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {
    val packet = event.packet

    if (packet is C03PacketPlayer) {
      if (spoofValue.get()) packet.onGround = true
      if (packets++ >= 40 && kickBypassValue.get()) {
        packets = 0
        MovementUtils.handleVanillaKickBypass()
        debug("trying to bypass kick")
      }
    }
  }
}