package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.value.*

@ModuleInfo(name = "TestValues", description = "every value type there is", category = ModuleCategory.CLIENT, canEnable = false, forceNoSound = true, array = false)
class TestValues : Module() {
  val bv = BoolValue("BoolValue", false)
  val iv = IntegerValue("IntegerValue", 0, -100, 100)
  val fv = FloatValue("FloatValue", 4f, 0f, 8f)
  val tv = TextValue("TextValue", "username")
  val lv = ListValue("ListValue", arrayOf(
    "ListValue1",
    "ListValue2",
    "ListValue3",
    "ListValue4",
    "ListValue5",
    "ListValue6",
    "ListValue7",
    "ListValue8",
    "ListValue9",
    "ListValue10",
    "ListValue11",
    "ListValue12",
  ), "ListValue1")

  val blv = BlockValue("BlockValue", 26)
  val mbv1 = MultiBoolValue("MultiBoolValue1", arrayOf(
    "MBValue1",
    "MBValue2",
    "MBValue3",
    "MBValue4",
    "MBValue5",
    "MBValue6",
    "MBValue7",
    "MBValue8",
    "MBValue9",
    "MBValue10",
    "MBValue11",
  ))

  val mbv2 = MultiBoolValue("MultiBoolValue2", linkedMapOf(
    "value1" to false,
    "value2" to true,
    "value3" to false,
    "value4" to true,
    "value5" to false,
    "value6" to true,
    "value7" to false,
    "value8" to true,
    "value9" to false,
    "value10" to true,
    "value11" to false,
    "value12" to true,
  ))
}