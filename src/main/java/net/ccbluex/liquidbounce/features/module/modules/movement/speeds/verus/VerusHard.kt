/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.verus

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.Speed
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils

class VerusHard : SpeedMode("VerusHard") {
  override fun onDisable() {
    mc.timer.timerSpeed = 1f
    super.onDisable()
  }

  override fun onMotion() {
    val speed = LiquidBounce.moduleManager.getModule(Speed::class.java)
    if (!mc.gameSettings.keyBindForward.isKeyDown && !mc.gameSettings.keyBindLeft.isKeyDown && !mc.gameSettings.keyBindRight.isKeyDown && !mc.gameSettings.keyBindBack.isKeyDown) {
      return
    }
    mc.timer.timerSpeed = speed.verusTimer.get()
    if (mc.thePlayer.onGround) {
      mc.thePlayer.jump()
      if (mc.thePlayer.isSprinting) {
        MovementUtils.strafe(MovementUtils.speed + 0.2f)
      }
    }
    MovementUtils.strafe(Math.max(MovementUtils.baseMoveSpeed.toFloat(), MovementUtils.speed)) // no sprint = faster - verus, since 2018
  }

  override fun onUpdate() {}
  override fun onMove(event: MoveEvent) {}
}