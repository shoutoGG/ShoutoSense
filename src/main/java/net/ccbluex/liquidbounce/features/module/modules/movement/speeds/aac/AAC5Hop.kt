/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils.isMoving

class AAC5Hop : SpeedMode("AAC5Hop") {
  override fun onMotion() {

  }

  override fun onUpdate() {
    if (!isMoving) {
      return
    }
    if (mc.thePlayer.onGround) {
      mc.thePlayer.jump()
      mc.thePlayer.speedInAir = 0.0201F
      mc.timer.timerSpeed = 0.94F
    }
    if (mc.thePlayer.fallDistance > 0.7 && mc.thePlayer.fallDistance < 1.3) {
      mc.thePlayer.speedInAir = 0.02F
      mc.timer.timerSpeed = 1.8F
    }
  }

  override fun onMove(event: MoveEvent?) {
  }

  override fun onDisable() {
    mc.thePlayer!!.speedInAir = 0.02f
    mc.timer.timerSpeed = 1f
  }
}
