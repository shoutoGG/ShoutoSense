/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac.*
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix.MatrixDynamic
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix.MatrixMultiply
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix.MatrixSemiStrafe
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix.MatrixTimerBalance
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.ncp.*
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.other.*
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.spartan.SpartanYPort
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.spectre.SpectreBHop
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.spectre.SpectreLowHop
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.spectre.SpectreOnGround
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.verus.VerusHard
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.verus.VerusHop
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.verus.VerusLowHop
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.watchdog.WatchdogBoost
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.watchdog.WatchdogCustom
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.watchdog.WatchdogStable
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.ListValue
import net.minecraft.client.gui.GuiChat
import net.minecraft.client.gui.GuiIngameMenu
import net.minecraft.client.settings.GameSettings

@ModuleInfo(name = "Speed", description = "Allows you to move faster.", category = ModuleCategory.MOVEMENT)
class Speed : Module() {
  private val speedModes = arrayOf(
    // NCP
    NCPBHop(),
    NCPFHop(),
    SNCPBHop(),
    NCPHop(),
    NCPYPort(), // AAC
    AAC4FastHop(),
    AAC4LongHop(),
    AAC4Hop(),
    AAC4SlowHop(),
    AACv4BHop(),
    AACBHop(),
    AAC2BHop(),
    AAC3BHop(),
    AAC4BHop(),
    AAC4Hop2(),
    AAC5Hop(),
    AAC5BHop(),
    AAC6BHop(),
    AAC7BHop(),
    OldAACBHop(),
    AACPort(),
    AACLowHop(),
    AACLowHop2(),
    AACLowHop3(),
    AACGround(),
    AACGround2(),
    AACHop350(),
    AACHop3313(),
    AACHop438(),
    AACYPort(),
    AACYPort2(),
    AACYPort3(), // Hypixel
    WatchdogBoost(),
    WatchdogStable(),
    WatchdogCustom(), // Spartan
    SpartanYPort(), // Spectre
    SpectreBHop(),
    SpectreLowHop(),
    SpectreOnGround(), // Verus
    VerusHop(),
    VerusLowHop(),
    VerusHard(), // Matrix
    MatrixSemiStrafe(),
    MatrixTimerBalance(),
    MatrixMultiply(),
    MatrixDynamic(), // Other
    SlowHop(),
    StrafeHop(),
    CustomSpeed(),
    Jump(),
    Legit(),
    AEMine(),
    GWEN(),
    Boost(),
    Frame(),
    MiJump(),
    OnGround(),
    YPort(),
    YPort2(),
    HiveHop(),
    MineplexGround(),
    TeleportCubeCraft(),
  )
  private val modifySprint = BoolValue("ModifySprinting", true)
  private val tagDisplay = ListValue("Tag", arrayOf("Type", "FullName", "All"), "Type")

  val typeValue: ListValue = object : ListValue("Type", arrayOf("NCP", "AAC", "Spartan", "Spectre", "Watchdog", "Verus", "Matrix", "Custom", "Other"), "NCP") {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }

  private val ncpModeValue: ListValue = object : ListValue("NCP-Mode", arrayOf("BHop", "FHop", "SBHop", "Hop", "YPort"), "BHop", { typeValue.eq("ncp") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  private val aacModeValue: ListValue = object : ListValue("AAC-Mode", arrayOf("4FastHop", "4LongHop", "4Hop", "4Hop2", "4SlowHop", "v4BHop", "BHop", "2BHop", "3BHop", "4BHop", "5Hop", "5BHop", "6BHop", "7BHop", "OldBHop", "Port", "LowHop", "LowHop2", "LowHop3", "Ground", "Ground2", "Hop3.5.0", "Hop3.3.13", "Hop4.3.8", "YPort", "YPort2"), "4Hop", { typeValue.eq("aac") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  private val watchdogModeValue: ListValue = object : ListValue("Watchdog-Mode", arrayOf("Boost", "Stable", "Custom"), "Stable", { typeValue.eq("watchdog") }) {
    // the worst hypixel bypass ever existed
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  private val spectreModeValue: ListValue = object : ListValue("Spectre-Mode", arrayOf("BHop", "LowHop", "OnGround"), "BHop", { typeValue.eq("spectre") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  val otherModeValue: ListValue = object : ListValue("Other-Mode", arrayOf("YPort", "YPort2", "Boost", "Frame", "MiJump", "OnGround", "SlowHop", "StrafeHop", "Jump", "Legit", "AEMine", "GWEN", "HiveHop", "MineplexGround", "TeleportCubeCraft"), "Boost", { typeValue.eq("other") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  private val verusModeValue: ListValue = object : ListValue("Verus-Mode", arrayOf("Hop", "LowHop", "Hard"), "Hop", { typeValue.eq("verus") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }
  private val matrixModeValue: ListValue = object : ListValue("Matrix-Mode", arrayOf("MatrixSemiStrafe", "MatrixTimerBalance", "MatrixMultiply", "MatrixDynamic"), "MatrixSemiStrafe", { typeValue.eq("matrix") }) {
    override fun onChange(oldValue: String, newValue: String) {
      if (state) {
        onDisable()
      }
    }

    override fun onChanged(oldValue: String, newValue: String) {
      if (state) {
        onEnable()
      }
    }
  }

  //region value for modes
  val verusTimer = FloatValue("Verus-Timer", 1f, 0.1f, 10f) { typeValue.eq("verus") && verusModeValue.eq("verushard") }

  val speedValue = FloatValue("CustomSpeed", 1.6f, 0.2f, 2f) { typeValue.eq("custom") }
  val launchSpeedValue = FloatValue("CustomLaunchSpeed", 1.6f, 0.2f, 2f) { typeValue.eq("custom") }
  val addYMotionValue = FloatValue("CustomAddYMotion", 0f, 0f, 2f) { typeValue.eq("custom") }
  val yValue = FloatValue("CustomY", 0f, 0f, 4f) { typeValue.eq("custom") }
  val upTimerValue = FloatValue("CustomUpTimer", 1f, 0.1f, 2f) { typeValue.eq("custom") }
  val downTimerValue = FloatValue("CustomDownTimer", 1f, 0.1f, 2f) { typeValue.eq("custom") }
  val strafeValue = ListValue("CustomStrafe", arrayOf("Strafe", "Boost", "Plus", "PlusOnlyUp", "Non-Strafe"), "Boost") { typeValue.eq("custom") }
  val groundStay = IntegerValue("CustomGroundStay", 0, 0, 10) { typeValue.eq("custom") }
  val groundResetXZValue = BoolValue("CustomGroundResetXZ", false) { typeValue.eq("custom") }
  val resetXZValue = BoolValue("CustomResetXZ", false) { typeValue.eq("custom") }
  val resetYValue = BoolValue("CustomResetY", false) { typeValue.eq("custom") }
  val doLaunchSpeedValue = BoolValue("CustomDoLaunchSpeed", true) { typeValue.eq("custom") }

  val jumpStrafe = BoolValue("JumpStrafe", false) { typeValue.eq("other") && otherModeValue.eq("Jump") }

  val timerValue = BoolValue("UseTimer", true) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogcustom") }
  val smoothStrafe = BoolValue("SmoothStrafe", true) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogcustom") }
  val customSpeedValue = FloatValue("StrSpeed", 0.42f, 0.2f, 2f) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogcustom") }
  val motionYValue = FloatValue("MotionY", 0.42f, 0f, 2f) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogcustom") }
  val sendJumpValue = BoolValue("SendJump", true) { typeValue.eq("watchdog") && !watchdogModeValue.eq("watchdogcustom") }
  val recalcValue = BoolValue("ReCalculate", true) { (typeValue.eq("watchdog") && sendJumpValue.get() && !watchdogModeValue.eq("watchdogcustom")) }
  val glideStrengthValue = FloatValue("GlideStrength", 0.03f, 0f, 0.05f) { typeValue.eq("watchdog") && !watchdogModeValue.eq("watchdogcustom") }
  val moveSpeedValue = FloatValue("MoveSpeed", 1.47f, 1f, 1.7f) { typeValue.eq("watchdog") && !watchdogModeValue.eq("watchdogcustom") }
  val jumpYValue = FloatValue("JumpY", 0.42f, 0f, 1f) { typeValue.eq("watchdog") && !watchdogModeValue.eq("watchdogcustom") }
  val baseStrengthValue = FloatValue("BaseMultiplier", 1f, 0.5f, 1f) { typeValue.eq("watchdog") && !watchdogModeValue.eq("watchdogcustom") }
  val baseTimerValue = FloatValue("BaseTimer", 1.5f, 1f, 3f) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogboost") }
  val baseMTimerValue = FloatValue("BaseMultiplierTimer", 1f, 0f, 3f) { typeValue.eq("watchdog") && watchdogModeValue.eq("watchdogboost") }

  val portMax = FloatValue("AAC-PortLength", 1f, 1f, 20f) { typeValue.eq("aac") && aacModeValue.get().contains("port", true) }
  val aacGroundTimerValue = FloatValue("AACGround-Timer", 3f, 1.1f, 10f) { typeValue.eq("aac") && aacModeValue.get().contains("ground", true) }

  val aaccUpSpeedInAir = FloatValue("AACCustomUpSpeedInAir", 0.21f, 0.2f, 1f) { typeValue.eq("aac") && aacModeValue.eq("aaccustom") }
  val aaccUpTimer = FloatValue("AACCustomUpTimer", 1.1f, 1f, 4f) { typeValue.eq("aac") && aacModeValue.eq("aaccustom") }
  val aaccFallDistance = FloatValue("AACCustomFallDistance", 0.5f, 0f, 5f) { typeValue.eq("aac") && aacModeValue.eq("aaccustom") }
  val aaccDownSpeedInAir = FloatValue("AACCustomDownSpeedInAir", 0.25f, 0.2f, 1f) { typeValue.eq("aac") && aacModeValue.eq("aaccustom") }
  val aaccDownTimer = FloatValue("AACCustomDownTimer", 1.7f, 1f, 4f) { typeValue.eq("aac") && aacModeValue.eq("aaccustom") }

  val cubecraftPortLengthValue = FloatValue("CubeCraft-PortLength", 1f, 0.1f, 2f) { typeValue.eq("other") && otherModeValue.eq("teleportcubecraft") }
  val mineplexGroundSpeedValue = FloatValue("MineplexGround-Speed", 0.5f, 0.1f, 1f) { typeValue.eq("other") && otherModeValue.eq("mineplexground") }

  //endregion

  @EventTarget
  fun onUpdate(event: UpdateEvent?) {
    if (mc.thePlayer.isSneaking) {
      return
    }
    if (MovementUtils.isMoving && modifySprint.get()) {
      mc.thePlayer.isSprinting = !modeName.equals("verushard", true)
    }
    val speedMode = mode
    speedMode?.onUpdate()
  }

  @EventTarget
  fun onMotion(event: MotionEvent) {
    if (mc.thePlayer.isSneaking || event.eventState !== EventState.PRE) {
      return
    }
    if (MovementUtils.isMoving && modifySprint.get()) {
      mc.thePlayer.isSprinting = !modeName.equals("verushard", true)
    }
    val speedMode = mode
    if (speedMode != null) {
      speedMode.onMotion(event)
      speedMode.onMotion()
    }
  }

  @EventTarget
  fun onMove(event: MoveEvent?) {
    if (mc.thePlayer.isSneaking) {
      return
    }
    val speedMode = mode
    speedMode?.onMove(event)
  }

  @EventTarget
  fun onTick(event: TickEvent?) {
    if (mc.thePlayer.isSneaking) {
      return
    }
    val speedMode = mode
    speedMode?.onTick()
  }

  @EventTarget
  fun onJump(event: JumpEvent?) {
    val speedMode = mode
    speedMode?.onJump(event)
  }

  override fun onEnable() {
    if (mc.thePlayer == null) {
      return
    }
    mc.timer.timerSpeed = 1f
    val speedMode = mode
    speedMode?.onEnable()
  }

  override fun onDisable() {
    if (mc.thePlayer == null) {
      return
    }
    mc.timer.timerSpeed = 1f
    mc.gameSettings.keyBindJump.pressed = (mc.thePlayer != null && (mc.inGameHasFocus || LiquidBounce.moduleManager.getModule(InvMove::class.java).state) && !(mc.currentScreen is GuiIngameMenu || mc.currentScreen is GuiChat) && GameSettings.isKeyDown(mc.gameSettings.keyBindJump))
    val speedMode = mode
    speedMode?.onDisable()
  }

  override val tag: String
    get() {
      if (tagDisplay.eq("type")) {
        return typeValue.get()
      }
      if (tagDisplay.eq("fullname")) {
        return modeName
      }
      return if (typeValue.get() === "Other") otherModeValue.get() else if (typeValue.get() === "Custom") "Custom" else typeValue.get() + ", " + onlySingleName
    }
  private val onlySingleName: String
    get() {
      var mode = ""
      when (typeValue.get().lowercase()) {
        "ncp" -> mode = ncpModeValue.get()
        "aac" -> mode = aacModeValue.get()
        "spartan" -> mode = "Spartan"
        "spectre" -> mode = spectreModeValue.get()
        "watchdog" -> mode = watchdogModeValue.get()
        "verus" -> mode = verusModeValue.get()
        "matrix" -> mode = matrixModeValue.get()
      }
      return mode
    }
  val modeName: String
    get() {
      var mode = ""
      when (typeValue.get()) {
        "NCP" -> mode = if (ncpModeValue.eq("SBHop")) {
          "SNCPBHop"
        } else {
          "NCP" + ncpModeValue.get()
        }

        "AAC" -> mode = if (aacModeValue.eq("oldbhop")) {
          "OldAACBHop"
        } else {
          "AAC" + aacModeValue.get()
        }

        "Spartan" -> mode = "SpartanYPort"
        "Spectre" -> mode = "Spectre" + spectreModeValue.get()
        "Watchdog" -> mode = "Watchdog" + watchdogModeValue.get()
        "Verus" -> mode = "Verus" + verusModeValue.get()
        "Custom" -> mode = "Custom"
        "Other" -> mode = otherModeValue.get()
        "Matrix" -> mode = matrixModeValue.get()
      }
      return mode
    }
  val mode: SpeedMode?
    get() {
      for (speedMode in speedModes) {
        if (speedMode.modeName.equals(modeName, ignoreCase = true)) {
          return speedMode
        }
      }
      return null
    }
}