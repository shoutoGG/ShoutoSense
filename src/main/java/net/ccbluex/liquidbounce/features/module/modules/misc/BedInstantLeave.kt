package net.ccbluex.liquidbounce.features.module.modules.misc

import net.ccbluex.liquidbounce.event.ClickEvent
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.utils.MouseButton
import net.ccbluex.liquidbounce.utils.PacketUtils
import net.ccbluex.liquidbounce.value.TextValue
import net.minecraft.item.ItemBed
import net.minecraft.network.play.client.C01PacketChatMessage

@ModuleInfo(name = "BedInstantLeave", spacedName = "Bed Instant Leave", description = "plz wait 3s plz pls", category = ModuleCategory.MISC)
class BedInstantLeave : Module() {
  private val command = TextValue("Command", "/bw leave")

  @EventTarget
  fun onClick(event: ClickEvent) {
    if (event.button == MouseButton.RIGHT) {
      val slot9 = mc.thePlayer.inventoryContainer.getSlot(44)
      val heldItem = mc.thePlayer.heldItem
      if (slot9 != null && slot9.stack.item is ItemBed && heldItem != null && heldItem.item != null && heldItem.item is ItemBed) {
        event.cancelEvent()
        PacketUtils.sendPacketNoEvent(C01PacketChatMessage(command.get()))
      }
    }
  }
}