/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils

class AAC4LongHop : SpeedMode("AAC4LongHop") {
  override fun onMotion() {}
  override fun onUpdate() {
    if (mc.thePlayer.isInWater) {
      return
    }
    if (!MovementUtils.isMoving) {
      return
    }
    if (mc.thePlayer.onGround) {
      mc.gameSettings.keyBindJump.pressed = false
      mc.thePlayer.jump()
    }
    if (!mc.thePlayer.onGround && mc.thePlayer.fallDistance <= 0.1) {
      mc.thePlayer.speedInAir = 0.02f
      mc.timer.timerSpeed = 1.5f
    }
    if (mc.thePlayer.fallDistance > 0.1 && mc.thePlayer.fallDistance < 1.3) {
      mc.timer.timerSpeed = 0.7f
    }
    if (mc.thePlayer.fallDistance >= 1.3) {
      mc.timer.timerSpeed = 1.0f
      mc.thePlayer.speedInAir = 0.02f
    }
  }

  override fun onMove(event: MoveEvent) {}
}