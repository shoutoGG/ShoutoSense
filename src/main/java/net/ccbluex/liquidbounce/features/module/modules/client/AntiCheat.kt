package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.ccbluex.liquidbounce.utils.ClassUtils

@ModuleInfo(name = "Anticheat", description = "run client side checks (from leaked ACs)", category = ModuleCategory.CLIENT)
class AntiCheat : Module() {
  private val checks = ClassUtils.resolvePackage("${this.javaClass.`package`.name}.anticheat", Check::class.java).map { it.newInstance() as Check }.sortedBy { it.javaClass.simpleName }

  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    for (check in checks) check.onUpdate()
  }

  @EventTarget
  fun onMotion(event: MotionEvent) {
    for (check in checks) check.onMotion(event)
  }

  @EventTarget
  fun onMove(event: MoveEvent) {
    for (check in checks) check.onMove(event)
  }

  @EventTarget
  fun onTick(event: TickEvent) {
    for (check in checks) check.onTick()
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {
    for (check in checks) check.onPacket(event, event.packet)
  }

  @EventTarget
  fun onJump(event: JumpEvent) {
    for (check in checks) check.onJump(event)
  }

  override fun onEnable() {
    for (check in checks) check.onEnable()
  }

  override fun onDisable() {
    for (check in checks) check.onDisable()
  }
}