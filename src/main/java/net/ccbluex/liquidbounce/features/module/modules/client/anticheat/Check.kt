package net.ccbluex.liquidbounce.features.module.modules.client.anticheat

import net.ccbluex.liquidbounce.event.JumpEvent
import net.ccbluex.liquidbounce.event.MotionEvent
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.utils.ClientUtils
import net.ccbluex.liquidbounce.utils.MinecraftInstance
import net.minecraft.network.Packet

@SuppressWarnings("unused")
abstract class Check(private val desc: String) : MinecraftInstance() {

  open fun onEnable() {}
  open fun onDisable() {}
  open fun onMotion(event: MotionEvent) {}
  open fun onUpdate() {}
  open fun onMove(event: MoveEvent) {}
  open fun onJump(event: JumpEvent) {}
  open fun onPacket(event: PacketEvent, packet: Packet<*>) {}
  open fun onTick() {}

  fun flag(info: String = "") {
    ClientUtils.displayChatMessage("§7[AC] §cfailed §b§l${this.javaClass.simpleName}§r: $desc${if (info.isNotEmpty()) ": $info" else ""}")
  }
}