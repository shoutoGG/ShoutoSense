package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C02PacketUseEntity
import net.minecraft.network.play.client.C02PacketUseEntity.Action.INTERACT
import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement

class BadPacketF : Check("invalid order between interact and place") {
  private var sent = false
  private var interact = false

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C03PacketPlayer) {
      sent = false
      interact = false
    } else if (packet is C08PacketPlayerBlockPlacement) {
      sent = true
    } else if (packet is C02PacketUseEntity && packet.action == INTERACT && sent && !interact) {
      flag()
    }
  }
}