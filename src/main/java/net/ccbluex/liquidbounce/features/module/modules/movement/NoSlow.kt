package net.ccbluex.liquidbounce.features.module.modules.movement

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.ccbluex.liquidbounce.utils.PacketUtils
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.ListValue
import net.minecraft.item.*
import net.minecraft.network.Packet
import net.minecraft.network.play.INetHandlerPlayServer
import net.minecraft.network.play.client.*
import net.minecraft.network.play.client.C03PacketPlayer.*
import net.minecraft.network.play.server.S08PacketPlayerPosLook
import net.minecraft.network.play.server.S09PacketHeldItemChange
import net.minecraft.network.play.server.S30PacketWindowItems
import net.minecraft.util.BlockPos
import net.minecraft.util.EnumFacing
import java.util.*
import kotlin.math.sqrt

@ModuleInfo(name = "NoSlow", spacedName = "No Slow", category = ModuleCategory.MOVEMENT, description = "Prevent you from getting slowed down by items (swords, foods, etc.) and liquids.")
class NoSlow : Module() {
  private val msTimer = MSTimer()
  private val modeValue = ListValue("PacketMode", arrayOf("Vanilla", "Watchdog", "OldWatchdog", "OldHypixel", "Blink", "Experimental", "NCP", "AAC", "AAC5", "Vulcan", "Matrix", "Medusa", "Custom"), "Vanilla")
  private val block = BoolValue("Block", true)
  private val blockForwardMultiplier = FloatValue("BlockForwardMultiplier", 1.0F, 0.2F, 1.0F, "x") { block.get() }
  private val blockStrafeMultiplier = FloatValue("BlockStrafeMultiplier", 1.0F, 0.2F, 1.0F, "x") { block.get() }
  private val consume = BoolValue("Consume", true)
  private val consumeForwardMultiplier = FloatValue("ConsumeForwardMultiplier", 1.0F, 0.2F, 1.0F, "x") { consume.get() }
  private val consumeStrafeMultiplier = FloatValue("ConsumeStrafeMultiplier", 1.0F, 0.2F, 1.0F, "x") { consume.get() }
  private val bow = BoolValue("Bow", true)
  private val bowForwardMultiplier = FloatValue("BowForwardMultiplier", 1.0F, 0.2F, 1.0F, "x") { bow.get() }
  private val bowStrafeMultiplier = FloatValue("BowStrafeMultiplier", 1.0F, 0.2F, 1.0F, "x") { bow.get() }
  val sneak = BoolValue("Sneak", false)
  val sneakForwardMultiplier = FloatValue("SneakForwardMultiplier", 1.0F, 0.3F, 1.0F, "x") { sneak.get() }
  val sneakStrafeMultiplier = FloatValue("SneakStrafeMultiplier", 1.0F, 0.3F, 1.0F, "x") { sneak.get() }
  private val cancelSlotReset = BoolValue("CancelSlotReset", false)
  private val customRelease = BoolValue("CustomReleasePacket", false) { modeValue.eq("custom") }
  private val customPlace = BoolValue("CustomPlacePacket", false) { modeValue.eq("custom") }
  private val customOnGround = BoolValue("CustomOnGround", false) { modeValue.eq("custom") }
  private val customDelayValue = IntegerValue("CustomDelay", 60, 0, 1000, "ms") { modeValue.eq("custom") }
  private val testValue = BoolValue("SendPacket", false) { modeValue.eq("watchdog") }
  private val packetTriggerValue = ListValue("PacketTrigger", arrayOf("PreRelease", "PostRelease"), "PostRelease") { modeValue.eq("blink") }

  val liquidPushValue = BoolValue("NoLiquidPush", true)

  private val teleportValue = BoolValue("Teleport", false)
  private val teleportModeValue = ListValue("TeleportMode", arrayOf("Vanilla", "VanillaNoSetback", "Custom", "Decrease"), "Vanilla") { teleportValue.get() }
  private val teleportNoApplyValue = BoolValue("TeleportNoApply", false) { teleportValue.get() }
  private val teleportCustomSpeedValue = FloatValue("Teleport-CustomSpeed", 0.13f, 0f, 1f) { teleportValue.get() && teleportModeValue.equals("Custom") }
  private val teleportCustomYValue = BoolValue("Teleport-CustomY", false) { teleportValue.get() && teleportModeValue.equals("Custom") }
  private val teleportDecreasePercentValue = FloatValue("Teleport-DecreasePercent", 0.13f, 0f, 1f) { teleportValue.get() && teleportModeValue.equals("Decrease") }

  private val blinkPackets = mutableListOf<Packet<*>>()
  private var lastX = 0.0
  private var lastY = 0.0
  private var lastZ = 0.0
  private var lastMotionX = 0.0
  private var lastMotionY = 0.0
  private var lastMotionZ = 0.0
  private var lastOnGround = false

  private var fasterDelay = false
  private var placeDelay = 0L
  private val timer = MSTimer()
  private val isBlocking: Boolean
    get() = (mc.thePlayer.isUsingItem || LiquidBounce.moduleManager[KillAura::class.java].blockingStatus) && mc.thePlayer.heldItem != null && mc.thePlayer.heldItem.item is ItemSword

  // skid
  private var nextTemp = false
  private var pendingFlagApplyPacket = false
  private val vmPackets = LinkedList<Packet<INetHandlerPlayServer>>()
  private var lastBlockingStat = false
  private var waitC03 = false
  private var sentPacket = false

  override fun onEnable() {
    blinkPackets.clear()
    msTimer.reset()
  }

  override fun onDisable() {
    blinkPackets.forEach {
      mc.netHandler.addToSendQueue(it)
    }
    blinkPackets.clear()
    vmPackets.clear()
    nextTemp = false
    waitC03 = false
  }

  override val tag: String
    get() = modeValue.get()

  private fun sendPacket(event: MotionEvent, sendC07: Boolean, sendC08: Boolean, delay: Boolean, delayValue: Long, onGround: Boolean, watchDog: Boolean = false) {
    val digging = C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos(-1, -1, -1), EnumFacing.DOWN)
    val blockPlace = C08PacketPlayerBlockPlacement(mc.thePlayer.inventory.getCurrentItem())
    val blockMent = C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, mc.thePlayer.inventory.getCurrentItem(), 0f, 0f, 0f)
    if (onGround && !mc.thePlayer.onGround) {
      return
    }
    if (sendC07 && event.eventState == EventState.PRE) {
      if (delay && msTimer.hasTimePassed(delayValue)) {
        mc.netHandler.addToSendQueue(digging)
      } else if (!delay) {
        mc.netHandler.addToSendQueue(digging)
      }
    }
    if (sendC08 && event.eventState == EventState.POST) {
      if (delay && msTimer.hasTimePassed(delayValue) && !watchDog) {
        mc.netHandler.addToSendQueue(blockPlace)
        msTimer.reset()
      } else if (!delay && !watchDog) {
        mc.netHandler.addToSendQueue(blockPlace)
      } else if (watchDog) {
        mc.netHandler.addToSendQueue(blockMent)
      }
    }
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {
    val packet = event.packet
    val killAura = LiquidBounce.moduleManager[KillAura::class.java]

    if (cancelSlotReset.get() && packet is S09PacketHeldItemChange) {
      chat("cancelling S09")
      event.cancelEvent()
      mc.netHandler.addToSendQueue(C09PacketHeldItemChange(mc.thePlayer.inventory.currentItem))
    }

    if (modeValue.eq("watchdog") && packet is S30PacketWindowItems && (mc.thePlayer.isUsingItem || mc.thePlayer.isBlocking)) {
      event.cancelEvent()
      debug("detected reset item packet")
    }

    if (modeValue.equals("Medusa")) {
      if ((mc.thePlayer.isUsingItem || mc.thePlayer.isBlocking) && sentPacket) {
        PacketUtils.sendPacketNoEvent(C0BPacketEntityAction(mc.thePlayer, C0BPacketEntityAction.Action.STOP_SPRINTING))
        sentPacket = false
      }
      if (!mc.thePlayer.isUsingItem || !mc.thePlayer.isBlocking) {
        sentPacket = true
      }
    }

    if ((modeValue.eq("Matrix") || modeValue.eq("Vulcan")) && nextTemp) {
      if ((packet is C07PacketPlayerDigging || packet is C08PacketPlayerBlockPlacement) && isBlocking) {
        event.cancelEvent()
      } else if (packet is C03PacketPlayer || packet is C0APacketAnimation || packet is C0BPacketEntityAction || packet is C02PacketUseEntity || packet is C07PacketPlayerDigging || packet is C08PacketPlayerBlockPlacement) {
        if (modeValue.eq("Vulcan") && waitC03 && packet is C03PacketPlayer) {
          waitC03 = false
          return
        }
        vmPackets.add(packet as Packet<INetHandlerPlayServer>)
        event.cancelEvent()
      }
    }

    if (teleportValue.get() && packet is S08PacketPlayerPosLook) {
      pendingFlagApplyPacket = true
      lastMotionX = mc.thePlayer.motionX
      lastMotionY = mc.thePlayer.motionY
      lastMotionZ = mc.thePlayer.motionZ
      when (teleportModeValue.get().lowercase()) {
        "vanillanosetback" -> {
          val x = packet.x - mc.thePlayer.posX
          val y = packet.y - mc.thePlayer.posY
          val z = packet.z - mc.thePlayer.posZ
          val diff = sqrt(x * x + y * y + z * z)
          if (diff <= 8) {
            event.cancelEvent()
            pendingFlagApplyPacket = false
            PacketUtils.sendPacketNoEvent(C06PacketPlayerPosLook(packet.x, packet.y, packet.z, packet.getYaw(), packet.getPitch(), mc.thePlayer.onGround))
          }
        }
      }
    } else if (pendingFlagApplyPacket && packet is C06PacketPlayerPosLook) {
      pendingFlagApplyPacket = false
      if (teleportNoApplyValue.get()) {
        event.cancelEvent()
      }
      when (teleportModeValue.get().lowercase()) {
        "vanilla", "vanillanosetback" -> {
          mc.thePlayer.motionX = lastMotionX
          mc.thePlayer.motionY = lastMotionY
          mc.thePlayer.motionZ = lastMotionZ
        }

        "custom" -> {
          if (MovementUtils.isMoving) {
            MovementUtils.strafe(teleportCustomSpeedValue.get())
          }

          if (teleportCustomYValue.get()) {
            if (lastMotionY > 0) {
              mc.thePlayer.motionY = teleportCustomSpeedValue.get().toDouble()
            } else {
              mc.thePlayer.motionY = -teleportCustomSpeedValue.get().toDouble()
            }
          }
        }

        "decrease" -> {
          mc.thePlayer.motionX = lastMotionX * teleportDecreasePercentValue.get()
          mc.thePlayer.motionY = lastMotionY * teleportDecreasePercentValue.get()
          mc.thePlayer.motionZ = lastMotionZ * teleportDecreasePercentValue.get()
        }
      }
    }

    if (!modeValue.eq("blink")) return
    if (!isBlocking) return    // shouldn't support consumeables for now
    // if (mc.thePlayer.itemInUse == null || mc.thePlayer.itemInUse.item == null) return

    when (packet) {
      is C04PacketPlayerPosition, is C06PacketPlayerPosLook -> {
        if (mc.thePlayer.positionUpdateTicks >= 20 && packetTriggerValue.eq("postrelease")) {
          (packet as C03PacketPlayer).x = lastX
          packet.y = lastY
          packet.z = lastZ
          packet.onGround = lastOnGround
          debug("pos update reached 20")
        } else {
          event.cancelEvent()
          if (packetTriggerValue.eq("postrelease")) PacketUtils.sendPacketNoEvent(C03PacketPlayer(lastOnGround))
          blinkPackets.add(packet)
          debug("packet player (movement) added at ${blinkPackets.size - 1}")
        }
      }

      is C05PacketPlayerLook -> {
        event.cancelEvent()
        if (packetTriggerValue.eq("postrelease")) PacketUtils.sendPacketNoEvent(C03PacketPlayer(lastOnGround))
        blinkPackets.add(packet)
        debug("packet player (rotation) added at ${blinkPackets.size - 1}")
      }

      is C03PacketPlayer -> {
        if (packetTriggerValue.eq("prerelease") || packet.onGround != lastOnGround) {
          event.cancelEvent()
          blinkPackets.add(packet)
          debug("packet player (idle) added at ${blinkPackets.size - 1}")
        }
      }

      is C0BPacketEntityAction -> {
        event.cancelEvent()
        blinkPackets.add(packet)
        debug("packet action added at ${blinkPackets.size - 1}")
      }

      is C07PacketPlayerDigging -> {
        if (packetTriggerValue.eq("prerelease")) {
          if (blinkPackets.size > 0) {
            blinkPackets.forEach {
              mc.netHandler.addToSendQueue(it)
            }
            debug("sent ${blinkPackets.size} packets.")
            blinkPackets.clear()
          }
        }
      }
    }
  }

  @EventTarget
  fun onMotion(event: MotionEvent) {
    if (!MovementUtils.isMoving && !modeValue.eq("blink")) return

    val heldItem = mc.thePlayer.heldItem
    val killAura = LiquidBounce.moduleManager[KillAura::class.java]

    when (modeValue.get().lowercase(Locale.getDefault())) {
      "aac5" -> if (event.eventState == EventState.POST && isBlocking) {
        mc.netHandler.addToSendQueue(C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, mc.thePlayer.inventory.getCurrentItem(), 0f, 0f, 0f))
      }

      "watchdog" -> if (testValue.get() && (isBlocking) && event.eventState == EventState.PRE && mc.thePlayer.itemInUse != null && mc.thePlayer.itemInUse.item != null) {
        val item = mc.thePlayer.itemInUse.item
        if (mc.thePlayer.isUsingItem && (item is ItemFood || item is ItemBucketMilk || item is ItemPotion) && mc.thePlayer.getItemInUseCount() >= 1) PacketUtils.sendPacketNoEvent(C09PacketHeldItemChange(mc.thePlayer.inventory.currentItem))
      }

      "blink" -> {
        if (event.eventState == EventState.PRE && !mc.thePlayer.isUsingItem && !mc.thePlayer.isBlocking) {
          lastX = event.x
          lastY = event.y
          lastZ = event.z
          lastOnGround = event.onGround
          if (blinkPackets.size > 0 && packetTriggerValue.eq("postrelease")) {
            blinkPackets.forEach {
              mc.netHandler.addToSendQueue(it)
            }
            debug("sent ${blinkPackets.size} packets.")
            blinkPackets.clear()
          }
        }
      }

      "experimental" -> {
        if ((mc.thePlayer.isUsingItem || mc.thePlayer.isBlocking) && timer.hasTimePassed(placeDelay)) {
          mc.playerController.syncCurrentPlayItem()
          mc.netHandler.addToSendQueue(C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos.ORIGIN, EnumFacing.DOWN))
          if (event.eventState == EventState.POST) {
            placeDelay = 200L
            if (fasterDelay) {
              placeDelay = 100L
              fasterDelay = false
            } else fasterDelay = true
            timer.reset()
          }
        }
      }

      else -> {
        if (!isBlocking) return
        when (modeValue.get().lowercase(Locale.getDefault())) {
          "aac" -> {
            if (mc.thePlayer.ticksExisted % 3 == 0) sendPacket(event, sendC07 = true, sendC08 = false, delay = false, delayValue = 0, onGround = false)
            else sendPacket(event, sendC07 = false, sendC08 = true, delay = false, delayValue = 0, onGround = false)
          }

          "ncp" -> sendPacket(event, sendC07 = true, sendC08 = true, delay = false, delayValue = 0, onGround = false)
          "oldwatchdog" -> {
            if (mc.thePlayer.ticksExisted % 2 == 0) sendPacket(event, sendC07 = true, sendC08 = false, delay = false, delayValue = 50, onGround = true)
            else sendPacket(event, sendC07 = false, sendC08 = true, delay = false, delayValue = 0, onGround = true, watchDog = true)
          }

          "oldhypixel" -> {
            if (event.eventState == EventState.PRE) mc.netHandler.addToSendQueue(C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos(-1, -1, -1), EnumFacing.DOWN))
            else mc.netHandler.addToSendQueue(C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, null, 0.0f, 0.0f, 0.0f))
          }

          "custom" -> sendPacket(event, customRelease.get(), customPlace.get(), customDelayValue.get() > 0, customDelayValue.get().toLong(), customOnGround.get())
        }
      }
    }
  }

  @EventTarget
  fun onSlowDown(event: SlowDownEvent) {
    val heldItem = mc.thePlayer.heldItem?.item

    if (block.get() && heldItem is ItemSword) {
      event.forward = getMultiplier(heldItem, true)
      event.strafe = getMultiplier(heldItem, false)
    }
    if (consume.get() && (heldItem is ItemFood || heldItem is ItemPotion || heldItem is ItemBucketMilk)) {
      event.forward = getMultiplier(heldItem, true)
      event.strafe = getMultiplier(heldItem, false)
    }
    if (bow.get() && heldItem is ItemBow) {
      event.forward = getMultiplier(heldItem, true)
      event.strafe = getMultiplier(heldItem, false)
    }
  }

  private fun getMultiplier(item: Item?, isForward: Boolean) = when (item) {
    is ItemFood, is ItemPotion, is ItemBucketMilk -> {
      if (isForward) this.consumeForwardMultiplier.get() else this.consumeStrafeMultiplier.get()
    }

    is ItemSword -> {
      if (isForward) this.blockForwardMultiplier.get() else this.blockStrafeMultiplier.get()
    }

    is ItemBow -> {
      if (isForward) this.bowForwardMultiplier.get() else this.bowStrafeMultiplier.get()
    }

    else -> 0.2F
  }


  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    if (mc.thePlayer == null || mc.theWorld == null) return
    if ((modeValue.eq("Matrix") || modeValue.eq("Vulcan")) && (lastBlockingStat || isBlocking)) {
      if (msTimer.hasTimePassed(230) && nextTemp) {
        nextTemp = false
        PacketUtils.sendPacketNoEvent(C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, BlockPos(-1, -1, -1), EnumFacing.DOWN))
        if (vmPackets.isNotEmpty()) {
          var canAttack = false
          for (packet in vmPackets) {
            if (packet is C03PacketPlayer) {
              canAttack = true
            }
            if (!((packet is C02PacketUseEntity || packet is C0APacketAnimation) && !canAttack)) {
              PacketUtils.sendPacketNoEvent(packet)
            }
          }
          vmPackets.clear()
        }
      }
      if (!nextTemp) {
        lastBlockingStat = isBlocking
        if (!isBlocking) {
          return
        }
        PacketUtils.sendPacketNoEvent(C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, mc.thePlayer.inventory.getCurrentItem(), 0f, 0f, 0f))
        nextTemp = true
        waitC03 = modeValue.eq("Vulcan")
        msTimer.reset()
      }
    }
  }
}
