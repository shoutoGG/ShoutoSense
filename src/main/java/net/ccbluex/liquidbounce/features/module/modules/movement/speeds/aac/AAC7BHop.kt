/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils

class AAC7BHop : SpeedMode("AAC7BHop") {
  override fun onMotion() {}
  override fun onUpdate() {
    if (!MovementUtils.isMoving || mc.thePlayer.ridingEntity != null || mc.thePlayer.hurtTime > 0) {
      return
    }
    if (mc.thePlayer.onGround) {
      mc.thePlayer.jump()
      mc.thePlayer.motionY = 0.405
      mc.thePlayer.motionX *= 1.004
      mc.thePlayer.motionZ *= 1.004
      return
    }
    val speed = MovementUtils.speed * 1.0072
    val yaw = Math.toRadians(mc.thePlayer.rotationYaw.toDouble())
    mc.thePlayer.motionX = -Math.sin(yaw) * speed
    mc.thePlayer.motionZ = Math.cos(yaw) * speed
  }

  override fun onMove(event: MoveEvent) {}
}