@file:Suppress("unused")

package net.ccbluex.liquidbounce.features.module.modules.combat

import de.enzaxd.viaforge.ViaForge
import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.*
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.exploit.Disabler
import net.ccbluex.liquidbounce.features.module.modules.misc.AntiBot
import net.ccbluex.liquidbounce.features.module.modules.misc.Teams
import net.ccbluex.liquidbounce.features.module.modules.movement.TargetStrafe
import net.ccbluex.liquidbounce.features.module.modules.player.Blink
import net.ccbluex.liquidbounce.features.module.modules.render.FreeCam
import net.ccbluex.liquidbounce.features.module.modules.world.Scaffold
import net.ccbluex.liquidbounce.utils.*
import net.ccbluex.liquidbounce.utils.extensions.getDistanceToEntityBox
import net.ccbluex.liquidbounce.utils.extensions.getNearestPointBB
import net.ccbluex.liquidbounce.utils.misc.RandomUtils
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.ccbluex.liquidbounce.utils.timer.TimeUtils
import net.ccbluex.liquidbounce.value.*
import net.minecraft.client.gui.inventory.GuiContainer
import net.minecraft.client.gui.inventory.GuiInventory
import net.minecraft.enchantment.EnchantmentHelper
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityArmorStand
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.item.ItemSword
import net.minecraft.network.play.client.*
import net.minecraft.potion.Potion
import net.minecraft.util.*
import net.minecraft.world.WorldSettings
import org.lwjgl.input.Keyboard
import java.security.SecureRandom
import java.util.*
import kotlin.math.cos
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sin

@ModuleInfo(name = "KillAura", spacedName = "Kill Aura", description = "Automatically attacks targets around you.", category = ModuleCategory.COMBAT, keyBind = Keyboard.KEY_R)
class KillAura : Module() {
  val attackNote = NoteValue("Attack")
  private val maxCPS: IntegerValue = object : IntegerValue("MaxCPS", 8, 1, 20) {
    override fun onChanged(oldValue: Int, newValue: Int) {
      val i = minCPS.get()
      if (i > newValue) set(i)

      attackDelay = TimeUtils.randomClickDelay(minCPS.get(), this.get())
    }
  }
  private val minCPS: IntegerValue = object : IntegerValue("MinCPS", 5, 1, 20) {
    override fun onChanged(oldValue: Int, newValue: Int) {
      val i = maxCPS.get()
      if (i < newValue) set(i)

      attackDelay = TimeUtils.randomClickDelay(this.get(), maxCPS.get())
    }
  }
  private val priorityValue = ListValue("Priority", arrayOf("Health", "Distance", "Direction", "LivingTime", "Armor", "HurtResistance", "HurtTime", "HealthAbsorption", "RegenAmplifier"), "Distance")
  val targetModeValue = ListValue("TargetMode", arrayOf("Single", "Switch", "Multi"), "Switch")
  private val switchDelayValue = IntegerValue("SwitchDelay", 1000, 1, 2000, "ms") { targetModeValue.eq("switch") }
  private val limitedMultiTargetsValue = IntegerValue("LimitedMultiTargets", 0, 0, 50) { targetModeValue.eq("multi") }
  private val hurtTimeValue = IntegerValue("HurtTime", 10, 0, 10)
  private val swingValue = ListValue("Swing", arrayOf("Normal", "Packet", "Off"), "Normal")

  val rangeNote = NoteValue("Range")
  val rangeValue = FloatValue("Range", 3.7f, 1f, 8f, "m")
  private val throughWallsRangeValue = FloatValue("ThroughWallsRange", 3f, 0f, 8f, "m")
  private val rangeSprintReducementValue = FloatValue("RangeSprintReducement", 0f, 0f, 0.4f, "m")

  val rotationNode = NoteValue("Rotation")
  private val rotations = ListValue("RotationMode", arrayOf("Vanilla", "Smart", "BackTrack", "Off"), "BackTrack")
  private val maxTurnSpeed: FloatValue = object : FloatValue("MaxTurnSpeed", 360f, 0f, 360f, "°", { !rotations.eq("off") }) {
    override fun onChanged(oldValue: Float, newValue: Float) {
      val v = minTurnSpeed.get()
      if (v > newValue) set(v)
    }
  }
  private val minTurnSpeed: FloatValue = object : FloatValue("MinTurnSpeed", 360f, 0f, 360f, "°", { !rotations.eq("off") }) {
    override fun onChanged(oldValue: Float, newValue: Float) {
      val v = maxTurnSpeed.get()
      if (v < newValue) set(v)
    }
  }
  private val noHitCheck = BoolValue("NoHitCheck", false) { !rotations.eq("off") }
  private val silentRotationValue = BoolValue("SilentRotation", true) { !rotations.eq("off") }
  private val rotationStrafeValue = ListValue("Strafe", arrayOf("Off", "Strict", "Silent"), "Off")
  private val fovValue = FloatValue("FOV", 360f, 0f, 360f)


  val abNote = NoteValue("Autoblock")
  val autoBlockModeValue = ListValue("AutoBlock", arrayOf("Off", "Packet", "AfterTick", "NCP", "OldHypixel"), "Off")
  private val blockTimingValue = MultiBoolValue("BlockTiming", linkedMapOf("AfterAttack" to true, "MotionPre" to false, "MotionPost" to false)) { !autoBlockModeValue.eq("Off") }
  private val noDuplicateBlock = BoolValue("NoDuplicateBlock", false) { !autoBlockModeValue.eq("Off") }
  private val interactAutoBlockValue = BoolValue("InteractAutoBlock", true) { !autoBlockModeValue.eq("Off") }
  private val abThruWallValue = BoolValue("AutoBlockThroughWalls", false) { !autoBlockModeValue.eq("Off") }
  private val afterTickPatchValue = BoolValue("AfterTickPatch", true) { autoBlockModeValue.eq("AfterTick") }
  private val abPressUseKey = BoolValue("AutoblockPressUseKey", false) { !autoBlockModeValue.eq("Off") }
  private val abMethod = ListValue("AutoblockPacketMethod", arrayOf("Packet", "Controller"), "Packet") { !autoBlockModeValue.eq("off") }
  private val blockRate = IntegerValue("BlockRate", 100, 1, 100, "%") { !autoBlockModeValue.eq("Off") }

  val predictNote = NoteValue("Predict")
  private val predictValue = BoolValue("Predict", true)
  private val predictSize = IntegerValue("PredictTicks", 1, 0, 10) { predictValue.get() }

  val rcNote = NoteValue("Random Center")
  private val randomCenterValue = BoolValue("RandomCenter", false) { !rotations.get().equals("none", true) }
  private val randomCenterNewValue = BoolValue("NewCalc", true) { !rotations.get().equals("none", true) && randomCenterValue.get() }
  private val minRand: FloatValue = object : FloatValue("MinMultiply", 0.8f, 0f, 2f, "x", { !rotations.get().equals("none", true) && randomCenterValue.get() }) {
    override fun onChanged(oldValue: Float, newValue: Float) {
      val v = maxRand.get()
      if (v < newValue) set(v)
    }
  }
  private val maxRand: FloatValue = object : FloatValue("MaxMultiply", 0.8f, 0f, 2f, "x", { !rotations.get().equals("none", true) && randomCenterValue.get() }) {
    override fun onChanged(oldValue: Float, newValue: Float) {
      val v = minRand.get()
      if (v > newValue) set(v)
    }
  }
  private val outborderValue = BoolValue("Outborder", false)

  val bpNote = NoteValue("Bypass")
  private val failRateValue = FloatValue("FailRate", 0f, 0f, 100f)
  private val fakeSwingValue = BoolValue("FakeSwing", false)
  private val noInventoryAttackValue = BoolValue("NoInvAttack", false)
  private val noInventoryDelayValue = IntegerValue("NoInvDelay", 200, 0, 500, "ms") { noInventoryAttackValue.get() }
  private val raycastValue = BoolValue("RayCast", false)
  private val raycastIgnoredValue = BoolValue("RayCastIgnored", false) { raycastValue.get() }
  private val livingRaycastValue = BoolValue("LivingRayCast", false) { raycastValue.get() }
  private val aacValue = BoolValue("AAC", false)
  private val keepSprintValue = BoolValue("KeepSprint", true)
  private val noScaffValue = BoolValue("NoScaffold", true)
  private val blinkCheck = BoolValue("NoBlink", true)
  private val noBadPackets = BoolValue("NoBadPackets", true)

  // Target
  var target: EntityLivingBase? = null
  var currentTarget: EntityLivingBase? = null
  var hitable = false
  private val prevTargetEntities = mutableListOf<Int>()

  private val targetRotation = TargetRotation()
  private val swing = Swing()
  private val ab = Autoblock()

  // Attack delay
  private val attackTimer = MSTimer()
  private var attackDelay = 0L
  private var clicks = 0

  // Container Delay
  private var containerOpen = -1L

  // Fake block status
  val blockingStatus: Boolean
    get() = ab.blocking
  var fakeBlock = false

  // by me
  var canSendBlockPacket = false


  override fun onEnable() {
    mc.thePlayer ?: return
    mc.theWorld ?: return

    updateTarget()
  }


  override fun onDisable() {
    target = null
    currentTarget = null
    hitable = false
    prevTargetEntities.clear()
    attackTimer.reset()
    clicks = 0

    ab.forceUnblock()
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {
    if (event.packet is C03PacketPlayer) {
      canSendBlockPacket = true
      debug("can send now")
    }
  }

  @EventTarget
  fun onMotion(event: MotionEvent) {
    if (rotationStrafeValue.eq("Off")) update()

    val state = event.eventState

    currentTarget ?: return
    updateHitable()

    if (state == EventState.PRE && blockTimingValue.get("MotionPre")) {
      debug("block on pre motion ${debugMsg(currentTarget!!)}")
      ab.block(currentTarget!!)
    }

    if (state == EventState.POST && blockTimingValue.get("MotionPost")) {
      debug("block on post motion ${debugMsg(currentTarget!!)}")
      ab.block(currentTarget!!)
    }
  }


  @EventTarget
  fun onStrafe(event: StrafeEvent) {
    val targetStrafe = LiquidBounce.moduleManager.getModule(TargetStrafe::class.java)
    if (rotationStrafeValue.eq("Off") && !targetStrafe.state) return

    update()

    if (currentTarget != null && RotationUtils.targetRotation != null) {
      if (targetStrafe.canStrafe) {
        val strafingData = targetStrafe.getData()
        MovementUtils.strafeCustom(MovementUtils.speed, strafingData[0], strafingData[1], strafingData[2])
        event.cancelEvent()
      } else when (rotationStrafeValue.get().lowercase(Locale.getDefault())) {
        "strict" -> {
          val (yaw) = RotationUtils.targetRotation ?: return
          var strafe = event.strafe
          var forward = event.forward
          val friction = event.friction

          var f = strafe * strafe + forward * forward

          if (f >= 1.0E-4F) {
            f = MathHelper.sqrt_float(f)

            if (f < 1.0F) f = 1.0F

            f = friction / f
            strafe *= f
            forward *= f

            val yawSin = MathHelper.sin((yaw * Math.PI / 180F).toFloat())
            val yawCos = MathHelper.cos((yaw * Math.PI / 180F).toFloat())

            mc.thePlayer.motionX += strafe * yawCos - forward * yawSin
            mc.thePlayer.motionZ += forward * yawCos + strafe * yawSin
          }
          event.cancelEvent()
        }

        "silent" -> {
          update()

          RotationUtils.targetRotation.applyStrafeToPlayer(event)
          event.cancelEvent()
        }
      }
    }
  }

  fun update() {
    if (cancelRun || (noInventoryAttackValue.get() && (mc.currentScreen is GuiContainer || System.currentTimeMillis() - containerOpen < noInventoryDelayValue.get()))) return

    // Update target
    updateTarget()

    if (target == null) {
      ab.unblock()
      return
    }

    // Target
    currentTarget = target

    if (!targetModeValue.eq("Switch") && isEnemy(currentTarget)) target = currentTarget
  }


  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    attack()
  }

  private fun attack() {
    if (cancelRun) {
      target = null
      currentTarget = null
      hitable = false
      ab.unblock()
      return
    }

    if (noInventoryAttackValue.get() && (mc.currentScreen is GuiContainer || System.currentTimeMillis() - containerOpen < noInventoryDelayValue.get())) {
      target = null
      currentTarget = null
      hitable = false
      if (mc.currentScreen is GuiContainer) containerOpen = System.currentTimeMillis()
      return
    }

    if (target != null && currentTarget != null) {
      while (clicks > 0) {
        runAttack()
        clicks--
      }
    }
  }


  @EventTarget
  fun onRender3D(event: Render3DEvent) {
    if (cancelRun) {
      target = null
      currentTarget = null
      hitable = false
      ab.unblock()
      return
    }

    if (noInventoryAttackValue.get() && (mc.currentScreen is GuiContainer || System.currentTimeMillis() - containerOpen < noInventoryDelayValue.get())) {
      target = null
      currentTarget = null
      hitable = false
      if (mc.currentScreen is GuiContainer) containerOpen = System.currentTimeMillis()
      return
    }

    target ?: return

    if (currentTarget != null && attackTimer.hasTimePassed(attackDelay) && currentTarget!!.hurtTime <= hurtTimeValue.get()) {
      clicks++
      attackTimer.reset()
      attackDelay = TimeUtils.randomClickDelay(minCPS.get(), maxCPS.get())
    }
  }


  @EventTarget
  fun onEntityMove(event: EntityMovementEvent) {
    val movedEntity = event.movedEntity

    if (target == null || movedEntity != currentTarget) return

    updateHitable()
  }


  private fun runAttack() {
    target ?: return
    currentTarget ?: return

    // Settings
    val failRate = failRateValue.get()
    val swingB = !swingValue.eq("off")
    val multi = targetModeValue.eq("Multi")
    val openInventory = aacValue.get() && mc.currentScreen is GuiInventory
    val failHit = failRate > 0 && Random().nextInt(100) <= failRate

    // Close inventory when open
    if (openInventory) mc.netHandler.addToSendQueue(C0DPacketCloseWindow())

    // Check is not hitable or check failrate
    if (!hitable || failHit) {
      if (swingB && (fakeSwingValue.get() || failHit)) swing.swing()
    } else { // Attack
      if (!multi) {
        attackEntity(currentTarget!!)
      } else {
        var targets = 0

        for (entity in mc.theWorld.loadedEntityList) {
          val distance = mc.thePlayer.getDistanceToEntityBox(entity)

          if (entity is EntityLivingBase && isEnemy(entity) && distance <= getRange(entity)) {
            attackEntity(entity)

            targets += 1

            if (limitedMultiTargetsValue.get() != 0 && limitedMultiTargetsValue.get() <= targets) break
          }
        }
      }

      prevTargetEntities.add(if (aacValue.get()) target!!.entityId else currentTarget!!.entityId)

      if (target == currentTarget) target = null
    }

    if (targetModeValue.eq("Switch") && attackTimer.hasTimePassed((switchDelayValue.get()).toLong())) {
      if (switchDelayValue.get() != 0) {
        prevTargetEntities.add(if (aacValue.get()) target!!.entityId else currentTarget!!.entityId)
        attackTimer.reset()
      }
    }

    // Open inventory
    if (openInventory) mc.netHandler.addToSendQueue(C16PacketClientStatus(C16PacketClientStatus.EnumState.OPEN_INVENTORY_ACHIEVEMENT))
  }


  private fun updateTarget() { // Reset fixed target to null
    var searchTarget = null

    // Settings
    val hurtTime = hurtTimeValue.get()
    val fov = fovValue.get()
    val switchMode = targetModeValue.eq("Switch")

    // Find possible targets
    val targets = mutableListOf<EntityLivingBase>()

    for (entity in mc.theWorld.loadedEntityList) {
      if (entity !is EntityLivingBase || !isEnemy(entity) || (switchMode && prevTargetEntities.contains(entity.entityId))) continue

      val distance = mc.thePlayer.getDistanceToEntityBox(entity)
      val entityFov = RotationUtils.getRotationDifference(entity)

      if (distance <= maxRange && (fov == 180F || entityFov <= fov) && entity.hurtTime <= hurtTime) targets.add(entity)
    }

    // Sort targets by priority
    when (priorityValue.get().lowercase(Locale.getDefault())) {
      "distance" -> targets.sortBy { mc.thePlayer.getDistanceToEntityBox(it) } // Sort by distance
      "health" -> targets.sortBy { it.health } // Sort by health
      "direction" -> targets.sortBy { RotationUtils.getRotationDifference(it) } // Sort by FOV
      "livingtime" -> targets.sortBy { -it.ticksExisted } // Sort by existence
      "hurtresistance" -> targets.sortBy { it.hurtResistantTime } // Sort by armor hurt time
      "hurttime" -> targets.sortBy { it.hurtTime } // Sort by hurt time
      "healthabsorption" -> targets.sortBy { it.health + it.absorptionAmount } // Sort by full health with absorption effect
      "regenamplifier" -> targets.sortBy { if (it.isPotionActive(Potion.regeneration)) it.getActivePotionEffect(Potion.regeneration).amplifier else -1 }
    }

    var found = false

    // Find best target
    for (entity in targets) { // Update rotations to current target
      if (!updateRotations(entity)) // when failed then try another target
      {
        continue
      }

      // Set target to current entity
      target = entity
      found = true
      break
    }

    if (found) {
      return
    }

    if (searchTarget != null) {
      if (target != searchTarget) target = searchTarget
      return
    } else {
      target = null
    }

    // Cleanup last targets when no target found and try again
    if (prevTargetEntities.isNotEmpty()) {
      prevTargetEntities.clear()
      updateTarget()
    }
  }


  private fun isEnemy(entity: Entity?): Boolean {
    if (entity is EntityLivingBase && (EntityUtils.targetDead || isAlive(entity)) && entity != mc.thePlayer) {
      if (!EntityUtils.targetInvisible && entity.isInvisible()) return false

      if (EntityUtils.targetPlayer && entity is EntityPlayer) {
        if (entity.isSpectator || AntiBot.isBot(entity)) return false

        if (EntityUtils.isFriend(entity) && !LiquidBounce.moduleManager[NoFriends::class.java].state) return false

        val teams = LiquidBounce.moduleManager[Teams::class.java]

        return !teams.state || !teams.isInYourTeam(entity)
      }

      return EntityUtils.targetMobs && EntityUtils.isMob(entity) || EntityUtils.targetAnimals && EntityUtils.isAnimal(entity)
    }

    return false
  }


  private fun attackEntity(entity: EntityLivingBase): Boolean {
    if (mc.thePlayer.isBlocking || blockingStatus) {
      debug("unblock before attack ${debugMsg(entity)}")
      ab.unblock()
    }

    LiquidBounce.eventManager.callEvent(AttackEvent(entity))
    if (swing.swingBefore()) swing.swing()
    mc.netHandler.addToSendQueue(C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK))
    if (swing.swingAfter()) swing.swing()
    if (keepSprintValue.get()) { // Critical Effect
      if (mc.thePlayer.fallDistance > 0F && !mc.thePlayer.onGround && !mc.thePlayer.isOnLadder && !mc.thePlayer.isInWater && !mc.thePlayer.isPotionActive(Potion.blindness) && !mc.thePlayer.isRiding) mc.thePlayer.onCriticalHit(entity)
      if (EnchantmentHelper.getModifierForCreature(mc.thePlayer.heldItem, entity.creatureAttribute) > 0F) mc.thePlayer.onEnchantmentCritical(entity)
    } else {
      if (mc.playerController.currentGameType != WorldSettings.GameType.SPECTATOR) mc.thePlayer.attackTargetEntityWithCurrentItem(entity)
    }

    // Start blocking after attack
    if ((!afterTickPatchValue.get() || !autoBlockModeValue.eq("AfterTick")) && (mc.thePlayer.isBlocking || canBlock)) if (blockTimingValue.get("AfterAttack")) {
      debug("block after attack ${debugMsg(entity)}")
      ab.block(entity, interactAutoBlockValue.get())
    }
    return true
  }


  private fun updateRotations(entity: Entity): Boolean {
    if (rotations.eq("off")) return true

    val disabler = LiquidBounce.moduleManager.getModule(Disabler::class.java)
    val modify = disabler.canModifyRotation

    if (modify) return true // just ignore then

    val defRotation = targetRotation.rotateTo(entity as EntityLivingBase) ?: return false

    if (silentRotationValue.get()) {
      RotationUtils.setTargetRotation(defRotation, 2)
    } else {
      defRotation.toPlayer(mc.thePlayer!!)
    }

    return true
  }


  private fun updateHitable() {
    if (rotations.eq("off")) {
      hitable = true
      return
    }

    val disabler = LiquidBounce.moduleManager.getModule(Disabler::class.java)

    // Completely disable rotation check if turn speed equals to 0 or NoHitCheck is enabled
    if (maxTurnSpeed.get() <= 0F || noHitCheck.get() || disabler.canModifyRotation) {
      hitable = true
      return
    }

    val reach = min(maxRange.toDouble(), mc.thePlayer.getDistanceToEntityBox(target!!)) + 1

    if (raycastValue.get()) {
      val raycastedEntity = RaycastUtils.raycastEntity(reach) {
        (!livingRaycastValue.get() || it is EntityLivingBase && it !is EntityArmorStand) && (isEnemy(it) || raycastIgnoredValue.get() || aacValue.get() && mc.theWorld.getEntitiesWithinAABBExcludingEntity(it, it.entityBoundingBox).isNotEmpty())
      }

      if (raycastValue.get() && raycastedEntity is EntityLivingBase && (LiquidBounce.moduleManager[NoFriends::class.java].state || !EntityUtils.isFriend(raycastedEntity))) currentTarget = raycastedEntity

      hitable = if (maxTurnSpeed.get() > 0F) currentTarget == raycastedEntity else true
    } else {
      hitable = if (currentTarget == null) false
      else RotationUtils.isFaced(currentTarget, reach)
    }
  }

  inner class Autoblock {
    var blocking = false

    fun block(target: EntityLivingBase, interact: Boolean = interactAutoBlockValue.get()) {
      if (autoBlockModeValue.eq("off") || !(blockRate.get() > 0 && Random().nextInt(100) <= blockRate.get())) return
      if (!canBlock) return
      if (noDuplicateBlock.get() && blocking) return
      if (noBadPackets.get() && !canSendBlockPacket) return

      if (!abThruWallValue.get() && !target.canEntityBeSeen(mc.thePlayer)) {
        fakeBlock = true
        return
      }

      if (autoBlockModeValue.eq("ncp")) {
        mc.netHandler.addToSendQueue(C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, null, 0.0f, 0.0f, 0.0f))
        blocking = true
        return
      }

      if (autoBlockModeValue.eq("oldhypixel")) {
        mc.netHandler.addToSendQueue(C08PacketPlayerBlockPlacement(BlockPos(-1, -1, -1), 255, mc.thePlayer.inventory.getCurrentItem(), 0.0f, 0.0f, 0.0f))
        blocking = true
        return
      }

      if (interact) {
        val positionEye = mc.renderViewEntity?.getPositionEyes(1F)

        val expandSize = target.collisionBorderSize.toDouble()
        val boundingBox = target.entityBoundingBox.expand(expandSize, expandSize, expandSize)

        val (yaw, pitch) = RotationUtils.targetRotation ?: Rotation(mc.thePlayer!!.rotationYaw, mc.thePlayer!!.rotationPitch)
        val yawCos = cos(-yaw * 0.017453292F - Math.PI.toFloat())
        val yawSin = sin(-yaw * 0.017453292F - Math.PI.toFloat())
        val pitchCos = -cos(-pitch * 0.017453292F)
        val pitchSin = sin(-pitch * 0.017453292F)
        val range = min(maxRange.toDouble(), mc.thePlayer!!.getDistanceToEntityBox(target)) + 1
        val lookAt = positionEye!!.addVector(yawSin * pitchCos * range, pitchSin * range, yawCos * pitchCos * range)

        val movingObject = boundingBox.calculateIntercept(positionEye, lookAt) ?: return
        val hitVec = movingObject.hitVec

        mc.netHandler.addToSendQueue(C02PacketUseEntity(target, Vec3(hitVec.xCoord - target.posX, hitVec.yCoord - target.posY, hitVec.zCoord - target.posZ)))
        mc.netHandler.addToSendQueue(C02PacketUseEntity(target, C02PacketUseEntity.Action.INTERACT))
      }

      if (abPressUseKey.get() && !mc.thePlayer.isUsingItem) mc.gameSettings.keyBindUseItem.pressed = true

      when (abMethod.get().lowercase()) {
        "packet" -> mc.netHandler.addToSendQueue(C08PacketPlayerBlockPlacement(mc.thePlayer.inventory.getCurrentItem()))
        "controller" -> mc.playerController.sendUseItem(mc.thePlayer, mc.theWorld, mc.thePlayer.inventory.getCurrentItem())
      }

      canSendBlockPacket = false
      blocking = true
    }

    fun unblock() {
      fakeBlock = false

      if (noBadPackets.get() && !canSendBlockPacket) return

      forceUnblock() // checked
    }

    fun forceUnblock() {
      if (blocking) {
        if (abPressUseKey.get() && mc.thePlayer.isUsingItem) mc.gameSettings.keyBindUseItem.pressed = false

        when (abMethod.get().lowercase()) {
          "packet" -> mc.netHandler.addToSendQueue(C07PacketPlayerDigging(C07PacketPlayerDigging.Action.RELEASE_USE_ITEM, if (autoBlockModeValue.eq("oldhypixel")) BlockPos(1.0, 1.0, 1.0) else BlockPos.ORIGIN, EnumFacing.DOWN))

          "controller" -> mc.playerController.onStoppedUsingItem(mc.thePlayer)
        }
        blocking = false
      }
    }
  }

  inner class TargetRotation {
    private val random = SecureRandom()
    private fun getBB(target: EntityLivingBase): AxisAlignedBB {
      var bb = target.entityBoundingBox
      if (predictValue.get()) bb = bb.offset(target.motionX * predictSize.get(), target.motionY * predictSize.get(), target.motionZ * predictSize.get())
      return bb
    }

    fun rotateTo(entity: EntityLivingBase): Rotation? {
      val boundingBox = getBB(entity)
      if (maxTurnSpeed.get() <= 0F) return RotationUtils.serverRotation
      var rotation = RotationUtils.serverRotation
      when (rotations.get().lowercase()) {
        "vanilla" -> {
          val (_, ro) = RotationUtils.searchCenter(boundingBox, outborderValue.get() && !attackTimer.hasTimePassed(attackDelay / 2), randomCenterValue.get(), predictValue.get(), mc.thePlayer!!.getDistanceToEntityBox(entity) < throughWallsRangeValue.get(), maxRange, RandomUtils.nextFloat(minRand.get(), maxRand.get()), randomCenterNewValue.get()) ?: return null
          rotation = ro
        }

        "smart" -> {
          rotation = RotationUtils.calculate(getNearestPointBB(mc.thePlayer.getPositionEyes(1F), boundingBox))
        }

        "backtrack" -> {
          rotation = RotationUtils.OtherRotation(boundingBox, RotationUtils.getCenter(entity.entityBoundingBox), predictValue.get(), mc.thePlayer!!.getDistanceToEntityBox(entity) < throughWallsRangeValue.get(), maxRange)
        }
      }

      if (minTurnSpeed.get() != 360f && maxTurnSpeed.get() != 360f) rotation = RotationUtils.limitAngleChange(RotationUtils.serverRotation, rotation, minTurnSpeed.get() + random.nextFloat() * (maxTurnSpeed.get() - minTurnSpeed.get()))
      return rotation
    }
  }

  inner class Swing {
    fun swingBefore(): Boolean {
      return ViaForge.getInstance().version <= 47
    }

    fun swingAfter(): Boolean {
      return !swingBefore()
    }

    fun swing() {
      when (swingValue.get().lowercase()) {
        "normal" -> mc.thePlayer.swingItem()
        "packet" -> mc.netHandler.addToSendQueue(C0APacketAnimation())
      }
    }
  }

  private fun debugMsg(target: EntityLivingBase): String {
    val s = mc.thePlayer
    val t = target

    return "name=${s.name}/${
      t.name
    } health=${s.health}/${s.maxHealth}:${
      t.health
    }/${
      t.maxHealth
    } te=${s.ticksExisted}:${
      t.ticksExisted
    } ground=${s.onGround}:${
      t.onGround
    } ht=${s.hurtTime}/${s.maxHurtTime}:${t.hurtTime}/${
      t.maxHurtTime
    } hrt=${s.hurtResistantTime}/${s.maxHurtResistantTime}:${
      t.hurtResistantTime
    }/${
      t.maxHurtResistantTime
    }"
  }

  //region idk

  private val cancelRun: Boolean
    get() {
      return mc.thePlayer.isSpectator || !isAlive(mc.thePlayer) || (blinkCheck.get() && LiquidBounce.moduleManager[Blink::class.java].state) || LiquidBounce.moduleManager[FreeCam::class.java].state || (noScaffValue.get() && LiquidBounce.moduleManager[Scaffold::class.java].state)
    }


  private fun isAlive(entity: EntityLivingBase) = entity.isEntityAlive && entity.health > 0 || aacValue.get() && entity.hurtTime > 5


  private val canBlock: Boolean
    get() = mc.thePlayer.heldItem != null && mc.thePlayer.heldItem.item is ItemSword


  private val maxRange: Float
    get() = max(rangeValue.get(), throughWallsRangeValue.get())

  private fun getRange(entity: Entity) = (if (mc.thePlayer.getDistanceToEntityBox(entity) >= throughWallsRangeValue.get()) rangeValue.get() else throughWallsRangeValue.get()) - if (mc.thePlayer.isSprinting) rangeSprintReducementValue.get() else 0F


  override val tag: String
    get() = targetModeValue.get()  //endregion
}