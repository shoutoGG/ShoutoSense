package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C03PacketPlayer

class BadPacketB : Check("invalid pitch") {
  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C03PacketPlayer && (packet.pitch < -90 || packet.pitch > 90)) {
      flag()
    }
  }
}