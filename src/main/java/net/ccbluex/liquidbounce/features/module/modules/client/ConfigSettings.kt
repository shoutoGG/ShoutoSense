package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.value.BoolValue

@ModuleInfo(name = "ConfigSettings", spacedName = "Config Settings", description = "Cusomizable config load/save", category = ModuleCategory.CLIENT, canEnable = false, forceNoSound = false)
object ConfigSettings : Module() {
  val saveBind = BoolValue("saveBind", false)
  val saveState = BoolValue("saveState", false)
  val saveCombatModules = BoolValue("saveCombatModules", true)
  val savePlayerModules = BoolValue("savePlayerModules", true)
  val saveMovementModules = BoolValue("saveMovementModules", true)
  val saveRenderModules = BoolValue("saveRenderModules", true)
  val saveClientModules = BoolValue("saveClientModules", true)
  val saveWorldModules = BoolValue("saveWorldModules", true)
  val saveMiscModules = BoolValue("saveMiscModules", true)
  val saveExploitModules = BoolValue("saveExploitModules", true)
  val saveGhostModules = BoolValue("saveGhostModules", true)
  val saveColorModules = BoolValue("saveColorModules", true)

  val loadBind = BoolValue("loadBind", false)
  val loadState = BoolValue("loadState", false)
  val loadCombatModules = BoolValue("loadCombatModules", true)
  val loadPlayerModules = BoolValue("loadPlayerModules", true)
  val loadMovementModules = BoolValue("loadMovementModules", true)
  val loadRenderModules = BoolValue("loadRenderModules", true)
  val loadClientModules = BoolValue("loadClientModules", true)
  val loadWorldModules = BoolValue("loadWorldModules", true)
  val loadMiscModules = BoolValue("loadMiscModules", true)
  val loadExploitModules = BoolValue("loadExploitModules", true)
  val loadGhostModules = BoolValue("loadGhostModules", true)
  val loadColorModules = BoolValue("loadColorModules", true)
}
