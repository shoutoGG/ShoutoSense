/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix

import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils.isMoving

class MatrixMultiply : SpeedMode("MatrixMultiply") {
  override fun onEnable() {
    mc.thePlayer.jumpMovementFactor = 0.02f
    mc.timer.timerSpeed = 1.0f
  }

  override fun onDisable() {
    mc.thePlayer.jumpMovementFactor = 0.02f
    mc.timer.timerSpeed = 1.0f
  }

  override fun onMotion() {
    if (!isMoving) {
      return
    }
    if (mc.thePlayer.onGround) {
      mc.timer.timerSpeed = 1.0f
      mc.thePlayer.jump()
    }
    if (mc.thePlayer.motionY > 0.003) {
      mc.thePlayer.motionX *= 1.01
      mc.thePlayer.motionZ *= 1.01
      mc.timer.timerSpeed = 1.05f
    }
  }

  override fun onUpdate() {}
  override fun onMove(event: MoveEvent) {}
}