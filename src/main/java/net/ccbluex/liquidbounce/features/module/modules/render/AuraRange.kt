/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.render

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.Render3DEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.minecraft.util.MathHelper.cos
import net.minecraft.util.MathHelper.sin
import org.lwjgl.opengl.GL11

@ModuleInfo(name = "AuraRange", spacedName = "Aura Range", description = "the circle thingy", category = ModuleCategory.RENDER)
class AuraRange : Module() {
  private val circleValue = BoolValue("Circle", true)
  private val red = IntegerValue("Red", 255, 0, 255) { circleValue.get() }
  private val green = IntegerValue("Green", 255, 0, 255) { circleValue.get() }
  private val blue = IntegerValue("Blue", 255, 0, 255) { circleValue.get() }
  private val alpha = IntegerValue("Alpha", 255, 0, 255) { circleValue.get() }
  private val accuracyValue = IntegerValue("Accuracy", 59, 0, 59) { circleValue.get() }
  private val thickness = FloatValue("Thickness", 0.7F, 0F, 4F) { circleValue.get() }

  private val range: Float
    get() = LiquidBounce.moduleManager.getModule(KillAura::class.java).rangeValue.get()

  @EventTarget
  fun onRender3D(event: Render3DEvent) {
    if (circleValue.get() && LiquidBounce.moduleManager.getModule(KillAura::class.java).state) {
      GL11.glPushMatrix()
      GL11.glTranslated(mc.thePlayer.lastTickPosX + (mc.thePlayer.posX - mc.thePlayer.lastTickPosX) * mc.timer.renderPartialTicks - mc.renderManager.renderPosX, mc.thePlayer.lastTickPosY + (mc.thePlayer.posY - mc.thePlayer.lastTickPosY) * mc.timer.renderPartialTicks - mc.renderManager.renderPosY, mc.thePlayer.lastTickPosZ + (mc.thePlayer.posZ - mc.thePlayer.lastTickPosZ) * mc.timer.renderPartialTicks - mc.renderManager.renderPosZ)
      GL11.glEnable(GL11.GL_BLEND)
      GL11.glEnable(GL11.GL_LINE_SMOOTH)
      GL11.glDisable(GL11.GL_TEXTURE_2D)
      GL11.glDisable(GL11.GL_DEPTH_TEST)
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)

      GL11.glLineWidth(thickness.get())
      GL11.glColor4f(red.get().toFloat() / 255.0F, green.get().toFloat() / 255.0F, blue.get().toFloat() / 255.0F, alpha.get().toFloat() / 255.0F)
      GL11.glRotatef(90F, 1F, 0F, 0F)
      GL11.glBegin(GL11.GL_LINE_STRIP)

      for (i in 0..360 step 60 - accuracyValue.get()) {
        GL11.glVertex2f(cos(i * 3.1415f / 180f) * range, sin(i * 3.1415f / 180f) * range)
      }

      GL11.glEnd()

      GL11.glDisable(GL11.GL_BLEND)
      GL11.glEnable(GL11.GL_TEXTURE_2D)
      GL11.glEnable(GL11.GL_DEPTH_TEST)
      GL11.glDisable(GL11.GL_LINE_SMOOTH)

      GL11.glPopMatrix()
    }

  }

}
