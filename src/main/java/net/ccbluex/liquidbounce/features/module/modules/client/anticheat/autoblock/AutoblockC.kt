package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.autoblock

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C07PacketPlayerDigging
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement

class AutoblockC : Check("released use item even though not using") {
  private var placing = true

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C07PacketPlayerDigging && packet.status == C07PacketPlayerDigging.Action.RELEASE_USE_ITEM && !placing) {
      flag()
      placing = false
    } else if (packet is C08PacketPlayerBlockPlacement) placing = true
  }
}