package net.ccbluex.liquidbounce.features.module.modules.player

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventState
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.MotionEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.utils.MovementUtils.isMoving
import net.ccbluex.liquidbounce.utils.PacketUtils.sendPacketNoEvent
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.ListValue
import net.ccbluex.liquidbounce.value.MultiBoolValue
import net.minecraft.init.Items
import net.minecraft.item.ItemStack
import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement
import net.minecraft.network.play.client.C09PacketHeldItemChange
import kotlin.random.Random

@ModuleInfo(name = "CustomGapple", spacedName = "Custom Gapple", description = "", category = ModuleCategory.PLAYER)
class CustomGapple : Module() {
  private val health = FloatValue("Health", 10f, 0f, 20f)
  private val conditions = MultiBoolValue("Conditions", linkedMapOf(
    "Blocking" to false,
    "Moving" to false,
    "Ground" to false,
  ))
  private val minDelay: IntegerValue = object : IntegerValue("MinDelay", 100, 0, 1000) {
    override fun onChanged(oldValue: Int, newValue: Int) {
      if (newValue > maxDelay.get()) set(maxDelay.get())
    }
  }
  private val maxDelay: IntegerValue = object : IntegerValue("MaxDelay", 200, 0, 1000) {
    override fun onChanged(oldValue: Int, newValue: Int) {
      if (newValue < minDelay.get()) set(minDelay.get())
    }
  }
  private val c03Count = IntegerValue("C03Count", 5, 0, 100)
  private val c03Mode = ListValue("C03Mode", arrayOf("Empty", "True", "False", "Real", "Invert"), "Empty") { c03Count.get() > 0 }
  private val timer = MSTimer()

  private val delay: Int
    get() = (minDelay.get() + Random.nextFloat() * (maxDelay.get() - minDelay.get())).toInt()

  private val blocking: Boolean
    get() = LiquidBounce.moduleManager[KillAura::class.java].blockingStatus


  private val gAppleSlot: Pair<Int, ItemStack>?
    get() {
      for (i in 0..8) {
        val stack = mc.thePlayer.inventory.getStackInSlot(i)
        if (stack != null && stack.item === Items.golden_apple) {
          return i to stack
        }
      }
      return null
    }

  private fun sendC03s() {
    repeat(c03Count.get()) {
      mc.netHandler.addToSendQueue(when (c03Mode.get().lowercase()) {
        "true" -> C03PacketPlayer(true)
        "false" -> C03PacketPlayer(false)
        "real" -> C03PacketPlayer(mc.thePlayer.onGround)
        "invert" -> C03PacketPlayer(!mc.thePlayer.onGround)
        else -> C03PacketPlayer()
      })
    }
  }

  @EventTarget
  fun onMotion(event: MotionEvent) {
    if (event.eventState != EventState.PRE) return
    if (conditions.get("Blocking") && !blocking) return
    if (conditions.get("Ground") && !mc.thePlayer.onGround) return
    if (conditions.get("Moving") && !isMoving) return
    if (mc.thePlayer.health > health.get()) return
    if (gAppleSlot == null) return
    if (!timer.hasTimePassed(delay)) return

    timer.reset()
    val oldSlot = mc.thePlayer.inventory.currentItem
    val pair = gAppleSlot!!
    val newSlot = pair.first
    val stack = pair.second

    sendPacketNoEvent(C09PacketHeldItemChange(newSlot))
    sendPacketNoEvent(C08PacketPlayerBlockPlacement(stack))
    sendC03s()
    sendPacketNoEvent(C09PacketHeldItemChange(oldSlot))
    chat("ate a gapple")
  }
}