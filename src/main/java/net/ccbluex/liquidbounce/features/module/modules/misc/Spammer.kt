/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.misc

import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.utils.misc.RandomUtils
import net.ccbluex.liquidbounce.utils.timer.MSTimer
import net.ccbluex.liquidbounce.utils.timer.TimeUtils.randomDelay
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.TextValue
import java.util.*

@ModuleInfo(name = "Spammer", description = "Spams the chat with a given message.", category = ModuleCategory.MISC)
class Spammer : Module() {
  private val maxDelayValue: IntegerValue = object : IntegerValue("MaxDelay", 1000, 0, 5000, "ms") {
    override fun onChanged(oldValue: Int, newValue: Int) {
      val minDelayValueObject = minDelayValue.get()
      if (minDelayValueObject > newValue) set(minDelayValueObject)
      delay = randomDelay(minDelayValue.get(), this.get())
    }
  }
  private val minDelayValue: IntegerValue = object : IntegerValue("MinDelay", 500, 0, 5000, "ms") {
    override fun onChanged(oldValue: Int, newValue: Int) {
      val maxDelayValueObject = maxDelayValue.get()
      if (maxDelayValueObject < newValue) set(maxDelayValueObject)
      delay = randomDelay(this.get(), maxDelayValue.get())
    }
  }
  private val messageValue = TextValue("Message", "Example text")
  private val customValue = BoolValue("Custom", false)
  private val blankText = TextValue("Placeholder guide", "") { customValue.get() }
  private val guideFloat = TextValue("%f", "Random float") { customValue.get() }
  private val guideInt = TextValue("%i", "Random integer (max length 10000)") { customValue.get() }
  private val guideString = TextValue("%s", "Random string (max length 9)") { customValue.get() }
  private val guideShortString = TextValue("%ss", "Random short string (max length 5)") { customValue.get() }
  private val guideLongString = TextValue("%ls", "Random long string (max length 16)") { customValue.get() }
  private val msTimer = MSTimer()
  private var delay = randomDelay(minDelayValue.get(), maxDelayValue.get())

  @EventTarget
  fun onUpdate(event: UpdateEvent?) {
    if (msTimer.hasTimePassed(delay)) {
      mc.thePlayer.sendChatMessage(if (customValue.get()) replace(messageValue.get()) else messageValue.get() + " >" + RandomUtils.randomString(5 + Random().nextInt(5)) + "<")
      msTimer.reset()
      delay = randomDelay(minDelayValue.get(), maxDelayValue.get())
    }
  }

  private fun replace(strIn: String): String {
    var s = strIn
    val r = Random()
    while (s.contains("%f")) s = s.substring(0, s.indexOf("%f")) + r.nextFloat() + s.substring(s.indexOf("%f") + "%f".length)
    while (s.contains("%i")) s = s.substring(0, s.indexOf("%i")) + r.nextInt(10000) + s.substring(s.indexOf("%i") + "%i".length)
    while (s.contains("%s")) s = s.substring(0, s.indexOf("%s")) + RandomUtils.randomString(r.nextInt(8) + 1) + s.substring(s.indexOf("%s") + "%s".length)
    while (s.contains("%ss")) s = s.substring(0, s.indexOf("%ss")) + RandomUtils.randomString(r.nextInt(4) + 1) + s.substring(s.indexOf("%ss") + "%ss".length)
    while (s.contains("%ls")) s = s.substring(0, s.indexOf("%ls")) + RandomUtils.randomString(r.nextInt(15) + 1) + s.substring(s.indexOf("%ls") + "%ls".length)
    return s
  }
}
