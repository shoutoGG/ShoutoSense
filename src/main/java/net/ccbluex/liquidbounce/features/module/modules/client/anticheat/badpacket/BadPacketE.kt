package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C09PacketHeldItemChange

class BadPacketE : Check("duplicated C09 packet") {
  private var lastSlot = -1

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C09PacketHeldItemChange) {
      val slot = packet.slotId
      if (slot == lastSlot) flag()
      lastSlot = slot
    }
  }
}