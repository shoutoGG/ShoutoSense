package net.ccbluex.liquidbounce.features.module.modules.player

import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.minecraft.network.play.client.C0DPacketCloseWindow

@ModuleInfo(name = "XCarry", description = "Allows you to store items in your crafting slots.", category = ModuleCategory.PLAYER)
class XCarry : Module() {

  @EventTarget
  fun onPacket(event: PacketEvent) {
    if (event.packet is C0DPacketCloseWindow) event.cancelEvent()
  }

}
