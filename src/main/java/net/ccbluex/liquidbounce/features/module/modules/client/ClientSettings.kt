package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.IntegerValue
import net.ccbluex.liquidbounce.value.ListValue

@ModuleInfo(name = "ClientSettings", spacedName = "Client Settings", description = "I have no better place to put values", category = ModuleCategory.CLIENT, canEnable = false, forceNoSound = true, array = false)
object ClientSettings : Module() {
  val showToggleNotification = BoolValue("ToggleNotification", true)
  val playToggleSound = ListValue("ToggleSound", arrayOf("None", "Default", "Custom"), "Default")
  val toggleVolume = IntegerValue("ToggleVolume", 100, 0, 100) { playToggleSound.get().equals("custom", true) }
  val chatLimit = IntegerValue("ChatLimit", 1000, 1, 32767)

  val hvhMode = BoolValue("HvHMode", false)
}