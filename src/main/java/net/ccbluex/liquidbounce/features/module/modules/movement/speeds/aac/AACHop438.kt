/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils

class AACHop438 : SpeedMode("AACHop4.3.8") {
  override fun onMotion() {}
  override fun onUpdate() {
    val self = mc.thePlayer ?: return

    mc.timer.timerSpeed = 1.0f

    if (!MovementUtils.isMoving || self.isInWater || self.isInLava || self.isOnLadder || self.isRiding) return

    if (self.onGround) { //      strafe(0.28)
      self.jump()
    } else { //      if (self.fallDistance <= 0.1) mc.timer.timerSpeed = 1.4f
      //      else if (self.fallDistance < 1.3) mc.timer.timerSpeed = 0.7f
      //      else mc.timer.timerSpeed = 1f
      mc.timer.timerSpeed = when {
        self.motionY > 0 -> 2f
        self.motionY < 0 -> 0.7f
        else -> 1f
      }
    }
  }

  override fun onMove(event: MoveEvent) {}
  override fun onDisable() {}
}
