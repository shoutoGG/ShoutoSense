/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.MotionEvent
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.features.module.modules.world.Scaffold
import net.ccbluex.liquidbounce.utils.MovementUtils.isMoving
import net.ccbluex.liquidbounce.utils.MovementUtils.speed
import net.ccbluex.liquidbounce.utils.MovementUtils.strafe
import net.minecraft.client.settings.GameSettings

class AACYPort3 : SpeedMode("AACYPort3") {
  private var wasTimer = false
  private var ticks = 0

  override fun onUpdate() {
    ticks++
    if (wasTimer) {
      mc.timer.timerSpeed = 1.00f
      wasTimer = false
    }
    mc.thePlayer.jumpMovementFactor = 0.0245f
    if (!mc.thePlayer.onGround && ticks > 3 && mc.thePlayer.motionY > 0) {
      mc.thePlayer.motionY = -0.27
    }

    mc.gameSettings.keyBindJump.pressed = GameSettings.isKeyDown(mc.gameSettings.keyBindJump)
    if (speed < 0.215f && !mc.thePlayer.onGround) {
      strafe(0.215f)
    }
    if (mc.thePlayer.onGround && isMoving) {
      ticks = 0
      mc.gameSettings.keyBindJump.pressed = false
      mc.thePlayer.jump()
      if (!mc.thePlayer.isAirBorne) {
        return
      }
      mc.timer.timerSpeed = 1.2f
      wasTimer = true
      if (speed < 0.48f) {
        strafe(0.48f)
      } else {
        strafe((speed * 0.985).toFloat())
      }
    } else if (!isMoving) {
      mc.timer.timerSpeed = 1.00f
      mc.thePlayer.motionX = 0.0
      mc.thePlayer.motionZ = 0.0
    }
  }

  override fun onMotion() {}

  override fun onMotion(event: MotionEvent) {}

  override fun onDisable() {
    val scaffoldModule = LiquidBounce.moduleManager.getModule(Scaffold::class.java)

    if (!mc.thePlayer.isSneaking && !scaffoldModule.state) strafe(0.3f)
  }

  override fun onMove(event: MoveEvent) {
  }

}