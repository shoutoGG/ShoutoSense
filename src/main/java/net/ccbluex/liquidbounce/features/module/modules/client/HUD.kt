/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.client

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.KeyEvent
import net.ccbluex.liquidbounce.event.Render2DEvent
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.ui.client.hud.designer.GuiHudDesigner
import net.ccbluex.liquidbounce.utils.AnimationUtils
import net.ccbluex.liquidbounce.utils.render.RenderUtils
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue
import net.ccbluex.liquidbounce.value.ListValue
import net.ccbluex.liquidbounce.value.TextValue

@ModuleInfo(name = "HUD", description = "Toggles visibility of the HUD.", category = ModuleCategory.CLIENT, array = false)
class HUD : Module() {
  val tabHead = BoolValue("Tab-HeadOverlay", true)
  val animHotbarValue = BoolValue("AnimatedHotbar", true)
  private val animHotbatSpeedValue = FloatValue("AnimatedHotbarSpeed", 0.2f, 0f, 0.5f) { animHotbarValue.get() }
  val blackHotbarValue = BoolValue("BlackHotbar", true)
  val inventoryParticle = BoolValue("InventoryParticle", false)
  val cmdBorderValue = BoolValue("CommandChatBorder", true)

  val guiButtonStyle = ListValue("ButtonStyle", arrayOf("Minecraft", "LiquidBounce", "Rounded", "LiquidBounce+"), "Minecraft")

  val containerBackground = BoolValue("ContainerBackground", false)
  val containerButton = ListValue("ContainerButton", arrayOf("TopLeft", "TopRight", "Off"), "TopLeft")
  val invEffectOffset = BoolValue("InvEffectOffset", false)
  val domainValue = TextValue("ScoreboardDomain", ".hud scoreboard-domain <your domain here>")

  private var hotBarX = 0F

  @EventTarget
  fun onRender2D(event: Render2DEvent) {
    if (mc.currentScreen is GuiHudDesigner) return
    LiquidBounce.hud.render(false)
  }

  @EventTarget
  fun onUpdate(event: UpdateEvent?) {
    LiquidBounce.hud.update()
  }

  @EventTarget
  fun onKey(event: KeyEvent) {
    LiquidBounce.hud.handleKey('a', event.key)
  }

  fun getAnimPos(pos: Float): Float {
    hotBarX = if (state && animHotbarValue.get()) AnimationUtils.animate(pos, hotBarX, animHotbatSpeedValue.get() * RenderUtils.deltaTime.toFloat())
    else pos

    return hotBarX
  }

  init {
    state = true
  }
}