package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.aac

import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils
import net.minecraft.client.settings.GameSettings

class AAC4Hop2 : SpeedMode("AAC4Hop2") {
  private var wasTimer = false

  override fun onDisable() {
    wasTimer = false
    mc.timer.timerSpeed = 1.0f
  }

  override fun onUpdate() {
    if (wasTimer) {
      wasTimer = false
      mc.timer.timerSpeed = 1.0f
    }
    mc.thePlayer.motionY -= 0.00348
    mc.thePlayer.jumpMovementFactor = 0.026f
    mc.gameSettings.keyBindJump.pressed = GameSettings.isKeyDown(mc.gameSettings.keyBindJump)
    if (MovementUtils.isMoving && mc.thePlayer.onGround) {
      mc.gameSettings.keyBindJump.pressed = false
      mc.timer.timerSpeed = 1.25f
      wasTimer = true
      mc.thePlayer.jump()
      MovementUtils.strafe()
    } else if (MovementUtils.speed < 0.215) {
      MovementUtils.strafe(0.215f)
    }
  }
}
