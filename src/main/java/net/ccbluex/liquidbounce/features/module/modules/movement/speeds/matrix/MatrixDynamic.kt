/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.matrix

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils.isMoving
import net.ccbluex.liquidbounce.utils.MovementUtils.strafe

class MatrixDynamic : SpeedMode("MatrixDynamic") {
  private val ka: KillAura
    get() = LiquidBounce.moduleManager[KillAura::class.java]

  override fun onEnable() {
    mc.thePlayer.jumpMovementFactor = 0.02f
    mc.timer.timerSpeed = 1.0f
  }

  override fun onDisable() {
    mc.thePlayer.jumpMovementFactor = 0.02f
    mc.timer.timerSpeed = 1.0f
  }

  override fun onMotion() {
    if (isMoving && mc.thePlayer.onGround) {
      mc.thePlayer.jump()
      strafe(0.28f)
    }
    if (ka.hitable) strafe(0.2f + if (mc.thePlayer.hurtTime % 2 == 0) 0.05f else -0.05f)
  }

  override fun onUpdate() {}
  override fun onMove(event: MoveEvent) {}
}