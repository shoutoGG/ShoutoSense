package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C02PacketUseEntity
import net.minecraft.network.play.client.C03PacketPlayer

class BadPacketD : Check("invalid order between attack and interact") {
  private var attack = false
  private var interactAt = false
  private var interact = false

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C02PacketUseEntity) {
      if (packet.action == C02PacketUseEntity.Action.ATTACK && !attack && (interact || interactAt)) flag("attacked AFTER interact")
      if (packet.action == C02PacketUseEntity.Action.INTERACT) interact = true
      if (packet.action == C02PacketUseEntity.Action.INTERACT_AT && !interactAt && interact) {
        flag("interactAt AFTER interact")
        interactAt = true
      }
    } else if (packet is C03PacketPlayer) {
      attack = false
      interact = false
      interactAt = false
    }
  }
}