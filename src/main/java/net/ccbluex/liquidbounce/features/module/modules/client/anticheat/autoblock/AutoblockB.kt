package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.autoblock

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.item.ItemSword
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement
import net.minecraft.network.play.client.C0BPacketEntityAction

class AutoblockB : Check("invalid sword block?") {
  private var sent = false
  fun onPacketaa(event: PacketEvent, packet: Packet<*>) {
    if (packet is C03PacketPlayer) {
      if (sent) flag()
      sent = false
    } else if (packet is C08PacketPlayerBlockPlacement) {
      if (packet.placedBlockDirection != 255) return
      if (packet.stack == null) return
      if (packet.stack.item is ItemSword && mc.thePlayer.isSprinting) sent = true
    } else if (packet is C0BPacketEntityAction && packet.action == C0BPacketEntityAction.Action.STOP_SPRINTING) sent = false
  }
}