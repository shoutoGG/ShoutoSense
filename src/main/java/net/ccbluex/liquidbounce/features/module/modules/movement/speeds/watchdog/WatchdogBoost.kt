/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement.speeds.watchdog

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.MoveEvent
import net.ccbluex.liquidbounce.features.module.modules.movement.Speed
import net.ccbluex.liquidbounce.features.module.modules.movement.TargetStrafe
import net.ccbluex.liquidbounce.features.module.modules.movement.speeds.SpeedMode
import net.ccbluex.liquidbounce.utils.MovementUtils

class WatchdogBoost : SpeedMode("WatchdogBoost") {
  override fun onMotion() {}
  override fun onUpdate() {}
  override fun onMove(event: MoveEvent) {
    val speed = LiquidBounce.moduleManager.getModule(Speed::class.java)
    val targetStrafe = LiquidBounce.moduleManager.getModule(TargetStrafe::class.java)
    mc.timer.timerSpeed = 1f
    if (MovementUtils.isMoving && !(mc.thePlayer.isInWater || mc.thePlayer.isInLava) && !mc.gameSettings.keyBindJump.isKeyDown) {
      var moveSpeed = Math.max(MovementUtils.baseMoveSpeed * speed.baseStrengthValue.get(), MovementUtils.speed.toDouble())
      if (mc.thePlayer.onGround) {
        if (speed.sendJumpValue.get()) {
          mc.thePlayer.jump()
        }
        if (speed.recalcValue.get()) {
          moveSpeed = Math.max(MovementUtils.baseMoveSpeed * speed.baseStrengthValue.get(), MovementUtils.speed.toDouble())
        }
        if (mc.thePlayer.isCollidedHorizontally) mc.thePlayer.motionY = MovementUtils.getJumpBoostModifier(0.42)
        else mc.thePlayer.motionY = MovementUtils.getJumpBoostModifier(speed.jumpYValue.get().toDouble())

        event.y = mc.thePlayer.motionY
        moveSpeed *= speed.moveSpeedValue.get().toDouble()
      } else if (speed.glideStrengthValue.get() > 0 && event.y < 0) {
        mc.thePlayer.motionY += speed.glideStrengthValue.get().toDouble()
        event.y = mc.thePlayer.motionY
      }
      mc.timer.timerSpeed = Math.max(speed.baseTimerValue.get() + Math.abs(mc.thePlayer.motionY.toFloat()) * speed.baseMTimerValue.get(), 1f)
      if (targetStrafe.canStrafe) {
        targetStrafe.strafe(event, moveSpeed)
      } else {
        MovementUtils.setSpeed(event, moveSpeed)
      }
    }
  }
}