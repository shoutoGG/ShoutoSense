package net.ccbluex.liquidbounce.features.module.modules.client.anticheat.badpacket

import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.features.module.modules.client.anticheat.Check
import net.minecraft.network.Packet
import net.minecraft.network.play.client.C03PacketPlayer
import net.minecraft.network.play.client.C0BPacketEntityAction
import net.minecraft.network.play.client.C0BPacketEntityAction.Action

class BadPacketC : Check("invalid sprint packet") {
  private var sent = false

  override fun onPacket(event: PacketEvent, packet: Packet<*>) {
    if (packet is C0BPacketEntityAction) {
      if (packet.action in setOf(Action.START_SPRINTING, Action.STOP_SPRINTING)) {
        if (sent) flag()
        else sent = true
      }
    } else if (packet is C03PacketPlayer) sent = false
  }
}