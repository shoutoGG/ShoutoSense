/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.movement

import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.UpdateEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.value.BoolValue
import net.ccbluex.liquidbounce.value.FloatValue

@ModuleInfo(name = "AutoJump", spacedName = "Auto Jump", description = "Automatically makes you jump.", category = ModuleCategory.MOVEMENT)
class AutoJump : Module() {
  private val legit = BoolValue("LegitJump", true)
  private val height = FloatValue("Height", 0.42f, 0f, 1f)

  @EventTarget
  fun onUpdate(event: UpdateEvent) {
    if (!mc.thePlayer.onGround) return

    if (legit.get()) mc.thePlayer.jump()
    else mc.thePlayer.motionY += height.get()
  }
}
