/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.module.modules.render

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.event.EventTarget
import net.ccbluex.liquidbounce.event.PacketEvent
import net.ccbluex.liquidbounce.event.Render3DEvent
import net.ccbluex.liquidbounce.features.module.Module
import net.ccbluex.liquidbounce.features.module.ModuleCategory
import net.ccbluex.liquidbounce.features.module.ModuleInfo
import net.ccbluex.liquidbounce.features.module.modules.combat.KillAura
import net.ccbluex.liquidbounce.features.module.modules.exploit.Disabler
import net.ccbluex.liquidbounce.features.module.modules.ghost.BowAimbot
import net.ccbluex.liquidbounce.features.module.modules.movement.Fly
import net.ccbluex.liquidbounce.features.module.modules.movement.Sprint
import net.ccbluex.liquidbounce.features.module.modules.world.ChestAura
import net.ccbluex.liquidbounce.features.module.modules.world.Fucker
import net.ccbluex.liquidbounce.features.module.modules.world.Scaffold
import net.ccbluex.liquidbounce.utils.RotationUtils
import net.ccbluex.liquidbounce.value.ListValue
import net.minecraft.network.play.client.C03PacketPlayer

@ModuleInfo(name = "Rotations", description = "Allows you to see server-sided head and body rotations.", category = ModuleCategory.RENDER)
class Rotations : Module() {

  val modeValue = ListValue("Mode", arrayOf("Head", "Body"), "Body")

  private var playerYaw: Float? = null

  @EventTarget
  fun onRender3D(event: Render3DEvent) {
    if (modeValue.get().equals("head", true) && RotationUtils.serverRotation != null) mc.thePlayer.rotationYawHead = RotationUtils.serverRotation.yaw
  }

  @EventTarget
  fun onPacket(event: PacketEvent) {
    if (modeValue.get().equals("head", true) || !shouldRotate() || mc.thePlayer == null) {
      playerYaw = null
      return
    }

    val packet = event.packet
    if (packet is C03PacketPlayer && packet.rotating) {
      playerYaw = packet.yaw
      mc.thePlayer.renderYawOffset = packet.getYaw()
      mc.thePlayer.rotationYawHead = packet.getYaw()
    } else {
      if (playerYaw != null) mc.thePlayer.renderYawOffset = this.playerYaw!!
      mc.thePlayer.rotationYawHead = mc.thePlayer.renderYawOffset
    }
  }

  private fun getState(module: Class<out Module>) = LiquidBounce.moduleManager[module].state

  fun shouldRotate(): Boolean {
    val killAura = LiquidBounce.moduleManager.getModule(KillAura::class.java)
    val disabler = LiquidBounce.moduleManager.getModule(Disabler::class.java)
    val sprint = LiquidBounce.moduleManager.getModule(Sprint::class.java)
    return getState(Scaffold::class.java) || (getState(Sprint::class.java) && sprint.allDirectionsValue.get() && sprint.moveDirPatchValue.get()) || (getState(KillAura::class.java) && killAura.target != null) || (getState(Disabler::class.java) && disabler.canRenderInto3D) || getState(BowAimbot::class.java) || getState(Fucker::class.java) || getState(ChestAura::class.java) || getState(Fly::class.java)
  }
}
