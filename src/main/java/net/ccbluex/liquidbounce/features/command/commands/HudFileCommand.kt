/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.utils.ClientUtils

class HudFileCommand : Command("hudfile", arrayOf("hudf")) {

  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    when (args.size) {
      1 -> ClientUtils.displayChatMessage(LiquidBounce.fileManager.hudConfig.file.absolutePath)
      2 -> {
        when (args[1].lowercase()) {
          "load", "l" -> LiquidBounce.fileManager.loadConfig(LiquidBounce.fileManager.hudConfig)
          "save", "s" -> LiquidBounce.fileManager.saveConfig(LiquidBounce.fileManager.hudConfig)
        }
      }
    }

  }

  override fun tabComplete(args: Array<String>): List<String> {
    return emptyList()
  }

}
