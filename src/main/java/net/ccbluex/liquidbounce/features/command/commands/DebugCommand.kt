/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command

class DebugCommand : Command("debug", emptyArray()) {
  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    when (args.size) {
      1 -> {
        LiquidBounce.debug = !LiquidBounce.debug
        chat("global debug toggled ${if (LiquidBounce.debug) "on" else "off"} ")
      }

      2 -> {
        val module = LiquidBounce.moduleManager.getModule(args[1])
        if (module == null) {
          chat("no such module")
          return
        }

        module.debug = !module.debug
        chat("${module.name}'s debug status toggled ${if (module.debug) "on" else "off"} ")
      }
    }
  }


  override fun tabComplete(args: Array<String>): List<String> {
    if (args.isEmpty()) return emptyList()

    val moduleName = args[0]

    return when (args.size) {
      1 -> LiquidBounce.moduleManager.modules.map { it.name }.filter { it.startsWith(moduleName, true) }.toList()

      else -> emptyList()
    }
  }
}
