/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.value.NoteValue

class ResetModuleCommand : Command("resetmodule", arrayOf("resetmod")) {
  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    when (args.size) {
      2 -> {
        val module = LiquidBounce.moduleManager.getModule(args[1])
        if (module == null) {
          print("no such module")
          return
        }
        for (value in module.values.filter { it !is NoteValue }) {
          val old = value.get()
          value.reset()
          val new = value.get()
          chat("§8reseted §7${module.name}.§f§l${value.name}§r: §c${old}§r §b-> §a§l${new}")
        }
      }
    }
  }

  override fun tabComplete(args: Array<String>): List<String> {
    if (args.isEmpty()) return emptyList()

    val moduleName = args[0]

    return when (args.size) {
      1 -> LiquidBounce.moduleManager.modules.map { it.name }.filter { it.startsWith(moduleName, true) }.toList()

      else -> emptyList()
    }
  }
}
