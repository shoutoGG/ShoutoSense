/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.utils.ClientUtils

class FovCommand : Command("fov", emptyArray()) {
  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    when (args.size) {
      1 -> ClientUtils.displayChatMessage("${mc.gameSettings.fovSetting}")
      2 -> mc.gameSettings.fovSetting = args[1].toFloat()
    }

    chatSyntax(arrayOf("<FOV>"))
  }

  override fun tabComplete(args: Array<String>): List<String> {
    return emptyList()
  }
}
