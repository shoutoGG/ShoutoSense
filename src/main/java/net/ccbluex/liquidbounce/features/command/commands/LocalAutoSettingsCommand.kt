/*
 * LiquidBounce++ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/PlusPlusMC/LiquidBouncePlusPlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.utils.SettingsUtils
import java.io.File

class LocalAutoSettingsCommand : Command("config", arrayOf("c", "localsetting", "locateautosettings", "locateautosetting", "localsettings", "localconfig")) {
  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    if (args.isEmpty()) return
    when (args[1].lowercase()) {
      "load", "l" -> {
        if (args.size < 3) {
          chatSyntax("config load NAME")
          return
        }
        val settingsFile = File(LiquidBounce.fileManager.settingsDir, args[2])
        if (!settingsFile.exists()) return
        val settings = settingsFile.readText()
        SettingsUtils.applySettings(settings)
        chat("§aloaded§f §c§l\"${args[2]}\"")
      }

      "save", "s" -> {
        if (args.size < 3) {
          chatSyntax("config save NAME")
          return
        }
        val settingsFile = File(LiquidBounce.fileManager.settingsDir, args[2])
        if (settingsFile.exists()) settingsFile.delete()
        settingsFile.writeText(SettingsUtils.dumpSettings())
        chat("§asaved§f §c§l\"${args[2]}\"")
      }

      "copy", "c" -> {
        if (args.size < 4) {
          chatSyntax("config copy OLD NEW")
          return
        }
        val oldFile = File(LiquidBounce.fileManager.settingsDir, args[2])
        val newFile = File(LiquidBounce.fileManager.settingsDir, args[3])
        if (!oldFile.exists()) return
        oldFile.copyTo(newFile)
        chat("§acopied§f §c§l\"${args[2]}\"§r -> §b§l\"${args[3]}\"")
      }

      "move", "m" -> {
        if (args.size < 4) {
          chatSyntax("config move OLD NEW")
          return
        }
        val oldFile = File(LiquidBounce.fileManager.settingsDir, args[2])
        val newFile = File(LiquidBounce.fileManager.settingsDir, args[3])
        if (!oldFile.exists()) return
        oldFile.renameTo(newFile)
        chat("§amoved§f §c§l\"${args[2]}\"§r -> §b§l\"${args[3]}\"")
      }

      "delete", "d" -> {
        if (args.size < 3) {
          chatSyntax("config delete NAME")
          return
        }
        val settingsFile = File(LiquidBounce.fileManager.settingsDir, args[2])
        if (settingsFile.exists()) settingsFile.delete()
        chat("§cdeleted§f §c§l\"${args[2]}\"")
      }

      "list" -> {
        chat("§bYour config${if (getLocalSettings()!!.size > 1) "s" else ""}:")
        for (file in getLocalSettings()!!) {
          chat("§7-§f§l ${file.name}")
        }
      }

      else -> chatSyntax("config <load/save/delete/move/copy/list>")
    }
  }

  override fun tabComplete(args: Array<String>): List<String> {
    if (args.isEmpty()) return emptyList()

    return when (args.size) {
      1 -> listOf("delete", "list", "load", "save", "copy", "move").filter { it.startsWith(args[0], true) }
      2 -> {
        when (args[0].lowercase()) {
          "delete", "load" -> {
            val settings = this.getLocalSettings() ?: return emptyList()

            return settings.map { it.name }.filter { it.startsWith(args[1], true) }
          }
        }
        return emptyList()
      }

      else -> emptyList()
    }
  }

  private fun getLocalSettings(): Array<File>? = LiquidBounce.fileManager.settingsDir.listFiles()
}
