package net.ccbluex.liquidbounce.features.command.commands

import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.features.module.modules.client.IRC
import net.ccbluex.liquidbounce.utils.misc.StringUtils

class LiquidChatCommand : Command("chat", arrayOf("lc", "irc")) {

  private val lChat = LiquidBounce.moduleManager.getModule(IRC::class.java)

  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    if (args.size > 1) {
      if (!lChat.state) {
        chat("§cError: §7LiquidChat is disabled!")
        return
      }

      if (!lChat.client.isConnected()) {
        chat("§cError: §IRC is currently not connected to the server!")
        return
      }

      val message = StringUtils.toCompleteString(args, 1)

      lChat.client.sendMessage(message)
    } else chatSyntax("chat <message>")
  }
}
