/*
 * LiquidBounce+ Hacked Client
 * A free open source mixin-based injection hacked client for Minecraft using Minecraft Forge.
 * https://github.com/WYSI-Foundation/LiquidBouncePlus/
 */
package net.ccbluex.liquidbounce.features.command.commands

import jdk.internal.dynalink.beans.StaticClass
import jdk.nashorn.api.scripting.NashornScriptEngineFactory
import net.ccbluex.liquidbounce.LiquidBounce
import net.ccbluex.liquidbounce.features.command.Command
import net.ccbluex.liquidbounce.script.api.global.Chat
import net.ccbluex.liquidbounce.script.api.global.Item
import net.ccbluex.liquidbounce.script.api.global.Setting
import net.minecraft.client.Minecraft
import net.minecraft.util.EnumChatFormatting
import javax.script.ScriptEngine

class EvalCommand : Command("eval", arrayOf("eval", "js")) {
  // pasted from Script.kt
  private val scriptEngine: ScriptEngine = NashornScriptEngineFactory().getScriptEngine(
    emptyArray(),
    this.javaClass.classLoader,
  )

  init {
    scriptEngine.put("Chat", StaticClass.forClass(Chat::class.java))
    scriptEngine.put("Setting", StaticClass.forClass(Setting::class.java))
    scriptEngine.put("Item", StaticClass.forClass(Item::class.java))
    scriptEngine.put("mc", Minecraft.getMinecraft())
    scriptEngine.put("moduleManager", LiquidBounce.moduleManager)
    scriptEngine.put("commandManager", LiquidBounce.commandManager)
    scriptEngine.put("scriptManager", LiquidBounce.scriptManager)
    scriptEngine.put("LB", LiquidBounce) //scriptEngine.put("registerScript", RegisterScript())
  }

  /**
   * Execute commands with provided [args]
   */
  override fun execute(args: Array<String>) {
    var js = args.drop(1).joinToString(separator = " ")
    if (js.isEmpty()) return
    if (args[0].equals("${LiquidBounce.commandManager.prefix}eval", ignoreCase = true)) {
      js = "Chat.print($js)"
      Chat.print("[Eval] $js")
    }
    try {
      scriptEngine.eval(js)
    } catch (e: Exception) {
      e.printStackTrace()
      Chat.print("${EnumChatFormatting.RED}[Eval] ${e.message}")
      Chat.print("${EnumChatFormatting.RED}[Eval] exception occurred, look at the game log for more info")
    }
  }

}
